---
short-title: COMP1511 18s1 lec05
title: "Lecture 4: Functions and Loops"
...


<style>
pre {
    padding-top: 0em !important;
}
</style>

<main is="slide" class="title">
<h1><big><strong>Welcome!</strong></big></h1>

<h1>COMP1511 18s1<br />
    <small>Programming Fundamentals</small></h1>
</main>

<main is="slide" class="title">
<h1>COMP1511 18s1<br />
  <strong>— Lecture 4 —</strong><br />
  <small>More Functions + Loops</small></h1>

Andrew Bennett  
<small><tt>&lt;andrew.bennett@unsw.edu.au&gt;</tt></small>

even more functions  
`while` loops

</main>

<main is="slide">

<p>&nbsp;</p>

<big><big><big><big><big>
<strong>
Before we begin...
</strong>
</big></big></big></big></big>

<p>&nbsp;</p>
<big>
__introduce__ yourself to the person sitting next to you  

<p>&nbsp;</p>


__why__ did they decide to study __computing__?
</big>
</main>


<main is="slide">

Overview
=======

<big><strong>after this lecture, you should be able to...</strong></big>

handle __invalid input__ to your program

understand __why__ we use functions

write __simple functions__

understand the basics of  __while loops__

<small><small><small>
(__note__: you shouldn't be able to do all of these immediately after
watching this lecture. however, this lecture should (hopefully!) give
you the foundations you need to develop these skills. remember:
programming is like learning any other language or skill, it takes consistent and
regular practice.)
</small>
</small>
</small>
</main>


<main is="slide">

Admin
=====

<big><big><big><strong>Don't panic!</strong></big></big></big>

these slides are on WebCMS3 ("DRAFT")

lecture recordings are on WebCMS3

make sure you have __home computing__ set up

make sure you can send and receive __uni emails__

</main>

<main is="slide">

A challenge for you
===================

<big><strong>Guess the Number</strong></big>

computer is thinking of a number  
enter a guess  
program responds "higher" or "lower" or "correct!"

<small>
__hint__  
to start out with:  
have a fixed secret number  
(i.e. `int secret = 5`)  
`scanf`  their guess  
rerun the program to guess another number
</small>

</main>



<!--
<section name="review">
<main is="slide">

Review
======

</main>
</section>
-->

<section name="functions">


<main is="slide" class="title">

<big>
<big>remember __functions__?
</big>
</big>

</main>
<!--


goal for today:
  - show likw WHY functions are useful
  - break down some program that does things already into functions that do each
    of the things
  - look at handling scanf

second half:
  - loops



-->
<!-- week 3 lecture 2 from 17s2, probably don't go this far? -->
<main is="slide">

Functions
=================

building blocks in our programs

self-contained, reusable pieces of code

abstraction

</main>
<main is="slide">

Anatomy of a Function
=============================

__return type__  
(`void` if no return value)  
__function name__  
__parameters__  
(inside parens, comma separated;  
`void` if no parameters)  
__statements__  
__return statement__

<pre><code class="lang-clike">
int addNumbers (int num1, int num2) {
    int sum = num1 + num2;
    return sum;
}
</code></pre>

</main>
<main is="slide">

Functions as Building Blocks
============================

for example:  
a function that takes a number and multiplies it by 2

we can take our number, and put it into the function, and get it out doubled

<pre><code class="lang-clike">
int x = 5;
x = doubled (x);
</code></pre>

__key things__:  
input (parameters)  
output (return value)  
functions won't change values

</main>

<main is="slide">

Why Functions?
==========

Revisiting `license.c`

</main>

<!-- demo goes in here -->

<main is="slide">

Why Functions?
==========

__main__ function:  
want to know __what__ it's doing  
don't need to know __how__ it's doing it

</main>


<main is="slide">

Side Note: When `scanf` Goes Wrong
=================================

what do we do if somebody enters __invalid input__?

(e.g. enters a word, not a number)

<pre><code class="lang-clike">
int a;
int b;
// What happens if they didn't type in two numbers?
int num = scanf("%d %d", &a, &b);
</code>
</pre>



</main>



<main is="slide">

Side Note: When `scanf` Goes Wrong
=================================

`scanf` __returns__ the number of things successfully scanned in

e.g.
<pre><code class="lang-clike">
int a;
int b;
// num will be 2 if both a and b were scanned successfully
int num = scanf("%d %d", &a, &b);
</code>
</pre>

</main>

<main is="slide">

Side Note: When `scanf` Goes Wrong
=================================

we can wrap this in an `if` statement:

<pre><code class="lang-clike">
int a;
int b;
// num will be 2 if both a and b were scanned successfully
if (scanf("%d %d", &a, &b) != 2) {
    printf("Invalid input!\n");
}
</code>
</pre>

</main>



<main is="slide">

Features of Functions
=============================

a function can have zero or more parameter(s)

a function can __only__ return zero or one value(s)

`* * *`

a function stores a local copy of parameters passed to it

the original values of variables remain unaltered

<!--
parameters received by the function,  
and local variables created by the function,  
are all __discarded__ when the function returns
-->

</main>



<!--
<main is="slide">

Touching the `void`
===================

most functions return a value...  
but `void` functions don't return anything

functions may have "side effects"  
like changing the state of the system,  
by printing things out

</main>
-->

<!--
<main is="slide">

Using Functions
===============

we've seen how to call a function:  
_printf_, _scanf_

don't show the types,
just the name of it

</main>
-->
</section>

<section name="loops">


<main is="slide" class="title">

<big>
<big>before we get started: extending the challenge</big>
</big>

</main>


<main is="slide">

Extending the challenge
===================

<big><strong>Guess the Number (v2)</strong></big>

computer is thinking of a number  
enter a guess  
program responds "higher" or "lower" or "correct!"  
then asks again  
and again  
until you guess correctly

<small>
__hint__  
use a loop to run the code multiple times
(coming up next!)
</small>

</main>



<main is="slide" class="title">

<big>
<big>and now for something __new__...</big>
</big>

</main>


<main is="slide">

Remember `if` statements?
=========================

<pre><code class="lang-clike">
int main (void) {
    printf ("Enter a number: ");

    int num;
    scanf ("%d", &num);

    if (num < 10) {
        printf ("Hello!\n");
    }
    
    return 0;
}
</code></pre>

__if__ the condition is true,
__then__ do something,
__else__ do something else.

</main>
<main is="slide">

What if we wanted to do something more than once?

<pre><code class="lang-clike">
int main (void) {
    printf ("Enter a number: ");

    int num;
    scanf ("%d", &num);

    while (num < 10) {
        printf ("Hello!\n");
    }
    
    return 0;
}
</code></pre>

</main>
<main is="slide">

What if we wanted to do something more than once?


<pre><code class="lang-clike">
int main (void) {
    printf ("Enter a number: ");

    int num;
    scanf ("%d", &num);

    while (num < 10) {
        printf ("Hello!\n");
        num++;
    }
    
    return 0;
}
</code></pre>

</main>
<main is="slide">

Anatomy of a Loop
=================

__initialisation__  
__condition__  
__statements__  
__update__

<pre><code class="lang-clike">
int i = 0;
while (i < 10) {
    printf ("Hello (number %d)\n", i);
    i = i + 1;
}
</code></pre>

</main>


</section>

<section name="end">
<!--
<main is="slide">

Feedback?
=========


<strong><big>
<a href="http://bit.do/comp1511-feedback-week2" style="color:#f9b200">
bit.do/comp1511-feedback-week2</a>
</big></strong>

<hr>

<img
src="http://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/week02/comp1511-week2-feedback-qr.png" />

alternate link: <a
href="https://andrewb3.typeform.com/to/uxfME1">https://andrewb3.typeform.com/to/uxfME1</a>
</main>
-->

<main is="slide">
end
</main>

</section>

<section name="challenge">


<main is="slide">

Another challenge
===================

<big><strong>Guess the Number (v3)</strong></big>

__human__ is thinking of a number  
__computer__ guesses  
__human__ responds "higher" or "lower" or "correct!"  
__computer__ guesses again  
and again  
until it has guessed correctly

</main>




</section>


<!-- code highlighted
<pre><code class="lang-clike">
if (condition 1) {
    if (condition 2) {
        if (condition 3) {
            // Do stuff
        } else if (condition 4) {
            // Do something else
        }
    } else if (condition 5) {
        if (condition 6) {
            // Do something completely different
        }
    } else {
        // In all other cases, do this.
    }
}
</code></pre>
-->



