---
short-title: COMP1511 18s1 lec04
title: "Lecture 5: More Loops"
...


<style>
pre {
    padding-top: 0em !important;
}
</style>

<main is="slide" class="title">
<h1><big><strong>Welcome!</strong></big></h1>

<h1>COMP1511 18s1<br />
    <small>Programming Fundamentals</small></h1>
</main>

<main is="slide" class="title">
<h1>COMP1511 18s1<br />
  <strong>— Lecture 5 —</strong><br />
  <small>More Loops</small></h1>

Andrew Bennett  
<small><tt>&lt;andrew.bennett@unsw.edu.au&gt;</tt></small>

`while` loops  
loops inside loops  
stopping loops

</main>

<main is="slide">

<p>&nbsp;</p>

<big><big><big><big><big>
<strong>
Before we begin...
</strong>
</big></big></big></big></big>

<p>&nbsp;</p>
<big>
__introduce__ yourself to the person sitting next to you  

<p>&nbsp;</p>


__why__ did they decide to study __computing__?
</big>
</main>


<main is="slide">

Overview
=======

<big><strong>after this lecture, you should be able to...</strong></big>


understand the basics of __while loops__

understand the basics of __nested while loops__

write programs using __while loops__ to solve simple problems

know about the course __style guide__

<small><small><small>
(__note__: you shouldn't be able to do all of these immediately after
watching this lecture. however, this lecture should (hopefully!) give
you the foundations you need to develop these skills. remember:
programming is like learning any other language, it takes consistent and
regular practice.)
</small>
</small>
</small>
</main>


<main is="slide">

Admin
=====

<big><big><strong>Don't panic!</strong></big></big>

lecture recordings are on WebCMS3  
<small>Echo360 was sad last night :(</small>

weekly tests start this week  
<small>don't be scared!</small>

course __style guide__ published

</main>

<section name="loops-again">
<main is="slide">

Loops
=====

what if we want to do something multiple times?

**Use a loop!**

<i>keep doing this __while__ this condition is true</i>

</main>
<main is="slide">

Anatomy of a Loop
=================

&nbsp;  
__initialisation__  
.

__condition__  
.

__statements__  
.

__update__  
.

</main>
<main is="slide">

Anatomy of a Loop
=================

&nbsp;  
__initialisation__  
set up our variables

__condition__  
.

__statements__  
.

__update__  
.

</main>
<main is="slide">

Anatomy of a Loop
=================

&nbsp;  
__initialisation__  
set up our variables

__condition__  
while "something"...

__statements__  
.

__update__  
.

</main>
<main is="slide">

Anatomy of a Loop
=================

__initialisation__  
set up our variables

__condition__  
while "something"...

__statements__  
things we do inside our loop

__update__  
.

</main>
<main is="slide">

Anatomy of a Loop
=================

__initialisation__  
set up our variables

__condition__  
while "something"...

__statements__  
things we do inside our loop

__update__  
move along to the next iteration

</main>
<main is="slide">

Aside: Definitions
==================

__iterate__  
perform repeatedly

__iteration__  
the repetition of a process

</main>
<main is="slide">

A Counting Loop
===============

"Do this thing `n` different times"

sometimes, it's explicit:  
_e.g._ print out 'hello world!' 10 times

sometimes, it's not:  
_e.g._ print out the numbers from 1-10  
_e.g._ calculate the power of a number (e.g., $$2^3$$)

</main>
<main is="slide">

A Counting Loop
===============

"Do this thing `n` times"

use a __loop counter__  
... a variable that we use in our loop  
to count how many times we've done something

</main>
<main is="slide">

A Counting Loop
===============

do something until we've done it `n` times  
_e.g._ print out 'hello world!' 10 times

counter starts at 0  
print "hello world!"; increase counter to 1 (we've done it once)  
print "hello world!"; increase counter to 2 (we've done it twice)  
print "hello world!"; increase counter to 3 (we've done it three times)  
...  
print "hello world!"; increase counter to 9 (we've done it 9 times)  
print "hello world!"; increase counter to 10 (we've done it 10 times)

now stop, because we've done it 10 times.

</main>
<main is="slide">

A Counting Loop
===============

how would we code this?

start our counter at 0

print "hello world!"  
*while* counter is less than 10,  
increase our counter by 1

</main>
<main is="slide">

Anatomy of a Loop
=================

__initialisation__  
set up our variables

__condition__  
while "something"...

__statements__  
things we do inside our loop

__update__  
move along to the next iteration

</main>
<main is="slide">

&nbsp;  
   
   
   
 

<pre><code class="lang-clike">
????
while (?????) {
    ????
    ????
}
</code></pre>

</main>
<main is="slide">

&nbsp;  
__initialisation__  
   
   
 

<pre><code class="lang-clike">
// set up our loop counter, start at 0
while (?????) {
    ????
    ????
}
</code></pre>

</main>
<main is="slide">

&nbsp;  
initialisation  
__condition__  
   
 

<pre><code class="lang-clike">
// set up our loop counter, start at 0
while (<i>something</i>) {
    ????
    ????
}
</code></pre>

</main>
<main is="slide">

&nbsp;  
initialisation  
condition  
__statements__  
 

<pre><code class="lang-clike">
// set up our loop counter, start at 0
while (<i>something</i>) {
    // do something
    ????
}
</code></pre>

</main>
<main is="slide">

&nbsp;  
initialisation  
condition  
statements  
__update__

<pre><code class="lang-clike">
// set up our loop counter, start at 0
while (<i>something</i>) {
    // do something
    // move to the next iteration of the loop
}
</code></pre>

</main>
<main is="slide">

&nbsp;  
__initialisation__  
condition  
statements  
update

<pre><code class="lang-clike">
int i = 0;
while (<i>something</i>) {
    // do something
    // move to the next iteration of the loop
}
</code></pre>

</main>
<main is="slide">

&nbsp;  
initialisation  
__condition__  
statements  
update

<pre><code class="lang-clike">
int i = 0;
while (i < 10) {
    // do something
    // move to the next iteration of the loop
}
</code></pre>

</main>
<main is="slide">

&nbsp;  
initialisation  
condition  
__statements__  
update

<pre><code class="lang-clike">
int i = 0;
while (i < 10) {
    printf ("hello, world!\n");
    // move to the next iteration of the loop
}
</code></pre>

</main>
<main is="slide">

&nbsp;  
initialisation  
condition  
statements  
__update__

<pre><code class="lang-clike">
int i = 0;
while (i < 10) {
    printf ("hello, world!\n");
    i = i + 1;
}
</code></pre>

</main>
</section>

<section name="stopping">
<main is="slide" class="title">

<big><h1>
how do we know  
when to __stop__?
</h1></big>
</main>


<main is="slide">

Loop Counters
====

<pre><code class="lang-clike">
int i = 0;
while (i < 10) {
    printf ("hello, world!\n");
    i = i + 1;
}
</code></pre>


</main>


<main is="slide">

Loop Counters
====

<pre><code class="lang-clike">
// Print out "hello, world!" n times,
// where n is chosen by the user.

int num;
printf ("Enter a number: ");
scanf ("%d", &num);

int i = 0;
while (i < num) {
    printf ("hello, world!\n");
    i = i + 1;
}
</code></pre>


</main>



<main is="slide">

Sentinel Value (Flag)
====

<pre><code class="lang-clike">
int finished = 0;
while (!finished) {
    printf ("hello, world!\n");
    finished = 1;
}
</code></pre>


</main>

<main is="slide">

Sentinel Value (Flag)
====

<pre><code class="lang-clike">
// Print out the number that the user entered
// Stop when they type 0

int n = 1;
while (n != 0) {
    printf ("You entered: %d\n", n);
    scanf("%d", &n);
}
</code></pre>


</main>



<!--
<main is="slide">

title
====

content

</main>
-->
</section>



<section name="style-guide">


<main is="slide" class="title">

<big>
<big>
<h1>
what is a __style guide__?
</h1>
</big>
</big>

</main>

<main is="slide">

Style Guide
=====

<a href="https://cgi.cse.unsw.edu.au/~cs1511/resources/style_guide.html"
class="orange">
https://cgi.cse.unsw.edu.au/~cs1511/resources/style_guide.html
</a>

linked from WebCMS3


</main>
</section>


<section name="nested-loops">


<!--
like if statements, loops can go inside each other

loops can also go inside if statements / if statements can go inside
loops

example: print a square

-->

<main is="slide" class="title">

nested loops
======

<big>
loops __inside__ loops
</big>

</main>


<main is="slide">

nested loops
======

<pre><code class="lang-clike">
while (something) {
    while (somethingElse) {

    }
}
</code></pre>

</main>

<main is="slide">

Demo: Printing a Square
======

scan in a number: __width__  
print out a square of __width__ * __width__ stars.

e.g. for width = 4:
<pre style="text-align: center">
* * * *
* * * *
* * * *
* * * *
</pre>

<small>
<i>__challenge__: can you just print the outside?</i>
<pre style="text-align:center">
* * * *
*     *
*     *
* * * *
</pre>
</small>

</main>

<!--
<main is="slide">

Demo: Printing a Square
======

Scan in a number: __width__
Print out a square of __width__ * __width__ stars.

e.g. for width = 4:
<pre>
* * * *
* * * *
* * * *
* * * *
</pre>

</main>
-->


</section>

<section name="end">
<main is="slide">

Feedback?
=========


<strong><big>
<a href="http://bit.do/comp1511-feedback-week3" style="color:#f9b200">
bit.do/comp1511-feedback-week3</a>
</big></strong>

<hr>

<img
src="http://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/week03/comp1511-week3-feedback-qr.png" />

alternate link: <a href="https://andrewb3.typeform.com/to/KuVZP4"
style="color:#f9b200">
https://andrewb3.typeform.com/to/KuVZP4
</a>

<main is="slide">
end
</main>

</section>

<!-- code highlighted
<pre><code class="lang-clike">
if (condition 1) {
    if (condition 2) {
        if (condition 3) {
            // Do stuff
        } else if (condition 4) {
            // Do something else
        }
    } else if (condition 5) {
        if (condition 6) {
            // Do something completely different
        }
    } else {
        // In all other cases, do this.
    }
}
</code></pre>
-->


