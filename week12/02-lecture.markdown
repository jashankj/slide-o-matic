---
short-title: COMP1511 18s1 lec20
title: "Lecture 20: Illegal C + Memory"
...

define(CODE, {{<pre><code class="language-c">}})
define(NOCODE, {{</code></pre>}})

<main is="slide" class="title">
<h1><big><strong>Welcome!</strong></big></h1>

<h1>COMP1511 18s1<br />
    <small>Programming Fundamentals</small></h1>
</main>

<main is="slide" class="title">
<h1>COMP1511 18s1<br />
  <strong>— Lecture 20 —</strong><br />
  <small>Illegal C + Memory</small></h1>

Andrew Bennett  
<small><tt>&lt;andrew.bennett@unsw.edu.au&gt;</tt></small>

</main>

<!--
http://cgi.cse.unsw.edu.au/~cs1511ass/18s1/farnarkle_tournament/
-->

<!--
<main is="slide">

<p>&nbsp;</p>

<big><big><big><big><big>
<strong>
Before we begin...
</strong>
</big></big></big></big></big>

<p>&nbsp;</p>
<big>
__introduce__ yourself to the person sitting next to you  

<p>&nbsp;</p>


__why__ did they decide to study __computing__?
</big>
</main>
-->

<!--
<main is="slide">

Feedback
=======

upload __lecture code__  


upload/incorporate __diagrams__  
in lecture recordings

<i>more</i> __diagrams__

go through programs __as a whole__  
before running them

lecture __subtitles__

</main>

-->

<main is="slide">

Overview
=======

<big><strong>after this lecture, you should be able to...</strong></big>

understand the __memory layout__ of a C program

understand some of the __security implications__ of this

have more of an understanding about __illegal C__

__identify__ and __prevent__ basic vulnerabilities in C code

(... and more?)

<small><small><small>
(__note__: you shouldn't be able to do all of these immediately after
watching this lecture. however, this lecture should (hopefully!) give
you the foundations you need to develop these skills. remember:
programming is like learning any other language, it takes consistent and
regular practice.)
</small>
</small>
</small>
</main><main is="slide">

Admin
=====

<big>
<big><big><big><strong>Don't panic!</strong></big></big></big>
</big>

__assignment 3__ out now!  
<small>week 11's tute/lab help you get started</small>

<big>week 11</big>  
__lab__ due __tonight__  
__weekly test__ due __friday__

don't forget about __help sessions__!  
<small>see course website for details</small>

</main><main is="slide">

Questions?
===========

<big><big>
<a class="orange"
href="https://echo360.org.au/home">
https://echo360.org.au/
</a>
</big></big>

<small>
note: you may need to go via __Moodle__  
<a class="orange"
href="https://moodle.telt.unsw.edu.au">
https://moodle.telt.unsw.edu.au
</a>

<small><small>(let me know if you can/can't access it!)</small></small>

<img
src="https://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/misc/echo360.png">
</main>

<!--
<section name="more-arrays">

arrays as functions parameters
  constant length array
  array length as parameter
  special terminating elelemnt

uninitialized array elements - show them dcc dashdash valgrind

</section>
-->


<!--

memory layout in C

we've talked about this a bit already:

everything is in memory

(including your code!)



some terminology:

stack -> "function memory"

heap -> "where malloced memory goes"


stack frames

every function has its own memory -> "stack frame"

stores all of the local variables etc

but also stores other necessary information:

where the stack frame starts/ends

where to go in the code when this function returns


-->


<section name="memory">

<main is="slide" class="title">

<big><h1>let's talk about: <strong>memory in C</strong></h1></big>


</main><main is="slide">

Memory Layout in C
=====

we've talked about this a bit already

__everything__ is in memory

(including your code!)

<small><small>
(diagram: <a class="orange"
href="https://www.cse.unsw.edu.au/~andrewb/slides/week06/04-lecture/#21">week
6 slides</a>)
</small></small>

</main><main is="slide">

Some Terminology
=====

__stack__: function memory

__heap__: dynamic memory (e.g. from malloc)

</main><main is="slide">

Stack Frames
=====

every function has its own memory

we call this a __stack frame__

...

it stores all of the local variables, etc  
but also other necessary information:

where the stack frame __starts__ / __ends__

where to go __in the code__ when this function __returns__  
<small>(the __return address__)</small>

</main><main is="slide">

Implications
=====

what happens if these are incorrect?

</main><main is="slide">

Illegal Array Access
=====

<small>
(demo: _interactive array tool_)
</small>


</main><main is="slide">

But wait, it gets better...
=====

<small>
(demo: _popping a calc_)
</small>

</main><main is="slide">

Smashing The Stack for Fun And Profit
====

<a class="orange"
href="http://insecure.org/stf/smashstack.html">
                     Smashing The Stack For Fun And Profit
</a>

</main>

</section>

<section name="farnarkle">

<main is="slide" class="title">

<big><h1>remember <strong>farnarkling</strong>?</h1></big>


</main><main is="slide">

The Farnarkle AI Leaderboard
=====

<a class="orange"
href="http://cs1511.cse.unsw.edu/farnarkle_tournament/">link</a>

<a class="orange"
href="https://cgi.cse.unsw.edu.au/~cs1511ass/18s1/farnarkle_tournament/
">backup link</a>

</main><main is="slide">

But that's not possible...
=====

<img
src="https://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/misc/farnarkle1.png">

</main><main is="slide">

But that's not possible...
=====

<img
src="https://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/misc/farnarkle2.png">

</main><main is="slide">

But that's not possible...
=====

<img
src="https://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/misc/farnarkle3.png">

</main><main is="slide">

What??
=====

(demo: _andrewb's farnarkle AI_)

</main><main is="slide">

What??
=====

<small>
links:  
<a
href="https://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/misc/farnarkle_results.png">terminal
output</a>  
<a
href="https://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/misc/farnarkle_explanation.pdf">explanation</a>

(demo: _andrewb's farnarkle AI_)

</main>


</section>

<section name="questions">

<main is="slide" class="title">

<big><h1><strong>questions?</strong></h1></big>

</main>

</section>
<section name="end">

<!--
<main is="slide">

Feedback?
=========


<strong><big>
<a href="http://bit.do/comp1511-feedback-week4" style="color:#f9b200">
bit.do/comp1511-feedback-week4</a>
</big></strong>

<hr>

<img
src="http://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/week04/comp1511-week4-feedback-qr.png" />

alternate link: <a href="https://andrewb3.typeform.com/to/WrAhfV"
style="color:#f9b200">
https://andrewb3.typeform.com/to/WrAhfV
</a>
</main>

-->

<main is="slide">
end
</main>

</section>

<!-- code highlighted
<pre><code class="lang-clike">
if (condition 1) {
    if (condition 2) {
        if (condition 3) {
            // Do stuff
        } else if (condition 4) {
            // Do something else
        }
    } else if (condition 5) {
        if (condition 6) {
            // Do something completely different
        }
    } else {
        // In all other cases, do this.
    }
}
</code></pre>
-->


