---
short-title: COMP1511 18s1 lec19
title: "Lecture 19: Stacks + Queues + ADTs"
...

define(CODE, {{<pre><code class="language-c">}})
define(NOCODE, {{</code></pre>}})

define(TT, {{<tt><strong>$1</strong></tt>}})

<main is="slide" class="title">
<h1><big><strong>Welcome!</strong></big></h1>

<h1>COMP1511 18s1<br />
    <small>Programming Fundamentals</small></h1>
</main>

<main is="slide" class="title">
<h1>COMP1511 18s1<br />
  <strong>— Lecture 19 —</strong><br />
  <small>Stacks + Queues + ADTs</small></h1>

Andrew Bennett  
<small><tt>&lt;andrew.bennett@unsw.edu.au&gt;</tt></small>

</main>


<!--
<main is="slide">

<p>&nbsp;</p>

<big><big><big><big><big>
<strong>
Before we begin...
</strong>
</big></big></big></big></big>

<p>&nbsp;</p>
<big>
__introduce__ yourself to the person sitting next to you  

<p>&nbsp;</p>


__why__ did they decide to study __computing__?
</big>
</main>
-->

<!--
<main is="slide">

Feedback
=======

upload __lecture code__  


upload/incorporate __diagrams__  
in lecture recordings

<i>more</i> __diagrams__

go through programs __as a whole__  
before running them

lecture __subtitles__

</main>

-->

<main is="slide">

Overview
=======

<big><strong>after this lecture, you should be able to...</strong></big>

have a basic understanding of __stacks__ and __queues__

have a basic understanding of __ADTs__

know the difference between __concrete__ and __abstract__ types

<small><small><small>
(__note__: you shouldn't be able to do all of these immediately after
watching this lecture. however, this lecture should (hopefully!) give
you the foundations you need to develop these skills. remember:
programming is like learning any other language, it takes consistent and
regular practice.)
</small>
</small>
</small>
</main>

<main is="slide">

Admin
=====

<big>
<big><big><big><strong>Don't panic!</strong></big></big></big>
</big>

__assignment 3__ out now!  
<small>this week's tute/lab help you get started</small>

__week 10 weekly test__ due __thursday__

don't forget about __help sessions__!  
<small>see course website for details</small>

</main>

<!--
<section name="more-arrays">

arrays as functions parameters
  constant length array
  array length as parameter
  special terminating elelemnt

uninitialized array elements - show them dcc dashdash valgrind

</section>
-->


<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX-->
<!-- XXXXX    STACK CONCEPT    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX-->
<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX-->

<section name="stacks">

<main is="slide" class="title">

<big><h1>introducing: <strong>stacks</strong></h1></big>

</main><main is="slide">

# Stacks

__stacks__ are a type of __data structure__  
<small>(a way of __organising__ data)</small>

a __stack__ is a collection of items such that  
the __last__ item to enter is the __first__ one to exit

"<strong>l</strong>ast <strong>i</strong>n, <strong>f</strong>irst <strong>o</strong>ut" (LIFO)

based on the idea of a stack of books, or plates

CODE

|      |  <- next free space
|______|
|  30  |
|______|
|  20  |
|______|
|  10  |  <- first added
|______|


NOCODE

</main><main is="slide">
# Stack

a __stack__ is a collection of items such that  
the __last__ item to enter is the __first__ one to exit

"<strong>l</strong>ast <strong>i</strong>n, <strong>f</strong>irst <strong>o</strong>ut" (LIFO)

...

essential stack operations:

__push()__ -- add new item to stack  
__pop()__ -- remove top item from stack

additional stack operations:

__top()__ -- fetch top item (but don't remove it)  
__size()__ -- number of items  
__is_empty()__

</main><main is="slide">

# Stack Applications

page-visited history in a web browser

undo sequence in a text editor

checking for balanced brackets

HTML tag matching

postfix (RPN) calculator

chain of function calls in a program

</main><main is="slide">

# Implementing a Stack

there are several different ways we can __implement__ a stack  
<small>(aka actually write the C code to make a stack)</small>

using an __array__

using a __linked list__

(+ others)

</main>

<!--
<main is="slide">

# Stack - Abstract Data Type - C Interface

CODE
typedef struct stack_internals *stack;
stack stack_create(void);
void stack_free(stack stack);
void stack_push(stack stack, int item);
int stack_pop(stack stack);
int stack_is_empty(stack stack);
int stack_top(stack stack);
int stack_size(stack stack);
NOCODE

</main><main is="slide">

# Stack - Abstract Data Type - using C Interface

CODE
stack s;
s = stack_create();
stack_push(s, 10);
stack_push(s, 11);
stack_push(s, 12);
printf("%d\n", stack_size(s)); // prints 3
printf("%d\n", stackop(s)); // prints 12
printf("%d\n", stack_pop(s)); // prints 12
printf("%d\n", stack_pop(s)); // prints 11
printf("%d\n", stack_pop(s)); // prints 10
NOCODE

implementation of stack is **opaque** (hidden from user);  
user programs can not depend on how stack is implementated.

stack implementation can change  
_without_ risk of breaking user programs.

**information hiding** is crucial  
to managing complexity in large software systems.

-->

</section>

<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX-->
<!-- XXXXX    STACK CONCRETE arrays     XXXXXXXXXXXXXXXXXXXXXXXXXXX-->
<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX-->


<section name="array stack">
<main is="slide">

# Implementing a Stack with an Array

we can use an __array__ to store the stack  
by keeping track of where we're up to in the array

CODE
struct stack_internals {
    int array[MAX_SIZE]; // holds the values
    int upto; // the index of the next free slot
};
NOCODE

CODE
[ ][ ][ ][ ][ ][ ][ ] // (the array)
 ^
 upto
NOCODE

</main><main is="slide">

# Implementing a Stack with an Array

we can use an __array__ to store the stack  
by keeping track of where we're up to in the array

CODE
push(3)
.
.
.
NOCODE

CODE
[3][ ][ ][ ][ ][ ][ ]
    ^
   upto
NOCODE

</main><main is="slide">

# Implementing a Stack with an Array

we can use an __array__ to store the stack  
by keeping track of where we're up to in the array

CODE
push(3)
push(1)
.
.
NOCODE

CODE
[3][1][ ][ ][ ][ ][ ]
       ^
      upto
NOCODE

</main><main is="slide">

# Implementing a Stack with an Array

we can use an __array__ to store the stack  
by keeping track of where we're up to in the array

CODE
push(3)
push(1)
push(4)
.
NOCODE

CODE
[3][1][4][ ][ ][ ][ ]
          ^
         upto
NOCODE

</main><main is="slide">

# Implementing a Stack with an Array

we can use an __array__ to store the stack  
by keeping track of where we're up to in the array

CODE
push(3)
push(1)
push(4)
pop() // returns 4
NOCODE

CODE
[3][1][ ][ ][ ][ ][ ]
       ^
      upto
NOCODE

</main><main is="slide">

# Implementing a Stack with an Array

we can use an __array__ to store the stack  
by keeping track of where we're up to in the array

CODE
// making a stack
struct stack_internals s = {0}; // initialise to 0

// pushing "5" to the stack
s.array[s.upto] = 5;
s.upto++;

// popping from the stack
s.upto--;
int value = s.array[s.upto];
// value is 5
NOCODE
</main>

</section>


<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX-->
<!-- XXXXX    STACK CONCRETE    lists   XXXXXXXXXXXXXXXXXXXXXXXXXXX-->
<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX-->


<section name="list stack">

<main is="slide">

# Implementing a Stack with a Linked List

a stack can be implemented using a linked list,  
by adding and removing at the head

CODE
struct stack_internals {
    struct node *head;
};
NOCODE

CODE
push(3)
.
.
.
NOCODE

CODE
(3) -> X
 ^ head
NOCODE

</main><main is="slide">

# Implementing a Stack with a Linked List

a stack can be implemented using a linked list,  
by adding and removing at the head

CODE
struct stack_internals {
    struct node *head;
};
NOCODE

CODE
push(3)
push(1)
.
.
NOCODE

CODE
(1) -> (3) -> X
 ^ head
NOCODE

</main><main is="slide">

# Implementing a Stack with a Linked List

a stack can be implemented using a linked list,  
by adding and removing at the head

CODE
struct stack_internals {
    struct node *head;
};
NOCODE

CODE
push(3)
push(1)
push(4)
.
NOCODE

CODE
(4) -> (1) -> (3) -> X
 ^ head
NOCODE

</main><main is="slide">

# Implementing a Stack with a Linked List

a stack can be implemented using a linked list,  
by adding and removing at the head

CODE
struct stack_internals {
    struct node *head;
};
NOCODE

CODE
push(3)
push(1)
push(4)
pop() // returns 4
NOCODE

CODE
(1) -> (3) -> X
 ^ head
NOCODE

</main><main is="slide">

# Implementing a Stack with a Linked List

a stack can be implemented using a linked list,  
by adding and removing at the head

CODE
// making a stack
struct stack_internals s = {0}; // initialise to 0

// pushing "5" to the stack
struct node *node = new_node(5); // make a new node
node->next = s.head; // add before start of list
s.head = node; // update list to start here

// popping from the stack
int value = s->head->data;
struct node *tmp = s->head; // keep track so we can free it
s->head = s->head->next; // update list start
free(tmp);
NOCODE
</main>
</section>



<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX-->
<!-- XXXXX    concrete vs abstract   XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX-->
<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX-->


<section name="concrete vs abstract">

<main is="slide">

# Using a Stack

we can use either of these methods to implement a stack  
<small>(or another approach!)</small>

__I write code__ to implement a stack,  
you need to __use a stack__, so you use my code

</main>

<main is="slide" class="title">

<h1><big><big>
but what if the  
__implementation__ changes?
</big></big>
</h1>

</main>



<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX-->
<!-- XXXXX    ADTs                  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX-->
<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX-->

<!-- *************************************************************** -->

<main is="slide" class="title">

an aside: __USBs__
==================

works... anywhere!

</main>


<main is="slide">

Concrete vs Abstract
====================

<div style="font-size: 125%">
CODE
struct stack_internals {
    // ...
};
NOCODE
</div>

a type is...  
__concrete__  
if a user of that type has
knowledge of how it works

a type is...  
__abstract__  
if a user has no knowledge of how it works

</main>
<main is="slide">

Concrete vs Abstract
====================


CODE
struct stack_internals {
    // ...
};
NOCODE

a concrete type is "right here":  
if you can see the type, you can use it

</main>
<main is="slide">

Concrete vs Abstract
====================

you cannot __change the insides__ of the type  
without breaking current software

we couldn't, for example, easily __switch between__ stack implementations  
(array vs list)

</main><main is="slide">

Abstraction
===========

our old friend, abstraction

use functions to interact with the stack,  
__push__  
__pop__  
etc

doesn't really matter  
how the __implementation__ works...  
only that the __interface__ is correct.

</main>
<main is="slide">

Hiding Structures
=================

<div style="font-size: 125%">
CODE
typedef struct stack_internals *stack;
NOCODE
</div>

we can now refer to TT(stack),  
without knowing what's in  
TT(struct stack_internals)...

__we cannot dereference (stab) it__  
but it can move around the system  
as an opaque value.

</main>
</section>

<!-- *************************************************************** -->
<section name="adt">
<main is="slide" class="title">

<h1><big><strong>ADTs</strong><br />
    Abstract Data Types</big></h1>

separating the implementation from the interface

</main>
</section>
<!--
<main is="slide">

ADT Jargon
==========

__interface__  
the header file

__implementation__  
the .c file with functions defined

__consumer__, __user__  
other .c files that use the functions

__constructor__  
makes a new instance of the ADT

__destructor__  
destroys an instance of the ADT

__getter__, __setter__  
retrieve or change a value in an instance

</main>
</section>
-->


<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX-->
<!-- XXXXX    stac ADT              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX-->
<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX-->


<section name="stack ADT">

<main is="slide" class="title">

<h1>
implementing a  
<strong>stack ADT
</strong></h1>

</main><main is="slide">

# Why a Stack ADT?

if we implement our stack as an ADT

we can __change the implementation__

without affecting how to __use__ the stack

</main><main is="slide">

# Stack - Abstract Data Type - C Interface

CODE
// use `stack` to refer to a pointer to the stack struct
typedef struct stack_internals *stack;

// pass the pointer into the stack functions
// (rather than trying to modify the struct directly)
stack stack_create(void);
void stack_free(stack stack);
void stack_push(stack stack, int item);
int stack_pop(stack stack);
int stack_is_empty(stack stack);
int stack_top(stack stack);
int stack_size(stack stack);
NOCODE

</main><main is="slide">

# Stack - Abstract Data Type - using C Interface

we can only interact with the stack  
using its __interface__ functions

CODE
stack s;
s = stack_create();
stack_push(s, 10);
stack_push(s, 11);
stack_push(s, 12);
printf("%d\n", stack_size(s)); // prints 3
printf("%d\n", stack_top(s)); // prints 12
printf("%d\n", stack_pop(s)); // prints 12
printf("%d\n", stack_pop(s)); // prints 11
printf("%d\n", stack_pop(s)); // prints 10
NOCODE

</main><main is="slide">

# Stack - Abstract Data Type - using C Interface

we can only interact with the stack  
using its __interface__ functions

we can't __dereference__ the pointer or access the struct fields

CODE
stack s = stack_create();

// note: if we tried to do this,
// we would get a compile error

// we can't see inside the struct, how do we know
// if it has an `array` field?
s->array[0] = 10;

// how do we know if it has a `size` field?
printf("%d", s->size);
NOCODE

</main><main is="slide">

# Stack - Abstract Data Type - using C Interface

implementation of stack is **opaque** (hidden from user);  
user programs can not depend on how stack is implementated.

stack implementation can change  
_without_ risk of breaking user programs.

**information hiding** is crucial  
to managing complexity in large software systems.

</main><main is="slide">

# Stack - Abstract Data Type - switching implementations

we can easily change which __implementation__ we use

<span style="font-size:80%">
CODE
// inside stack_user.c
stack s = stack_create();
stack_push(s, 5);
stack_push(s, 10);
printf("%d, stack_pop(s));
printf("%d, stack_pop(s));
NOCODE

CODE
$ dcc -o stack stack_user.c stack_list.c
$ ./stack
10
5
NOCODE

CODE
$ dcc -o stack stack_user.c stack_array.c
$ ./stack
10
5
NOCODE
</span>
</section>


<!--

<section name="stack ADT">

<main is="slide" class="title">

<h1>
testing a <strong>stack ADT
</strong></h1>

</main>


</section>
-->


<!--

<section name="section">

<main is="slide" class="title">

<big><h1>introducing: <strong>something</strong></h1></big>


</main>


<main is="slide">

title
=====

contents

</main>

<main is="slide">

title
=====

contents

</main>

</section>

--> 
<section name="end">

<!--
<main is="slide">

Feedback?
=========


<strong><big>
<a href="http://bit.do/comp1511-feedback-week4" style="color:#f9b200">
bit.do/comp1511-feedback-week4</a>
</big></strong>

<hr>

<img
src="http://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/week04/comp1511-week4-feedback-qr.png" />

alternate link: <a href="https://andrewb3.typeform.com/to/WrAhfV"
style="color:#f9b200">
https://andrewb3.typeform.com/to/WrAhfV
</a>
</main>

-->

<main is="slide">
end
</main>

</section>

<!-- code highlighted
<pre><code class="lang-clike">
if (condition 1) {
    if (condition 2) {
        if (condition 3) {
            // Do stuff
        } else if (condition 4) {
            // Do something else
        }
    } else if (condition 5) {
        if (condition 6) {
            // Do something completely different
        }
    } else {
        // In all other cases, do this.
    }
}
</code></pre>
-->


