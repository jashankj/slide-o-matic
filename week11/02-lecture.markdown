---
short-title: COMP1511 18s1 lec18
title: "Lecture 18: Linked Lists + Fruit Bot"
...

define(CODE, {{<pre><code class="language-c">}})
define(NOCODE, {{</code></pre>}})

define(BOT, {{<img src="https://emojipedia-us.s3.amazonaws.com/thumbs/120/google/133/robot-face_1f916.png">}})

<main is="slide" class="title">
<h1><big><strong>Welcome!</strong></big></h1>

<h1>COMP1511 18s1<br />
    <small>Programming Fundamentals</small></h1>
</main>

<main is="slide" class="title">
<h1>COMP1511 18s1<br />
  <strong>— Lecture 18 —</strong><br />
  <small>Fruit Bot + More Linked Lists</small></h1>

Andrew Bennett  
<small><tt>&lt;andrew.bennett@unsw.edu.au&gt;</tt></small>

</main>

<!--
<main is="slide">

<p>&nbsp;</p>

<big><big><big><big><big>
<strong>
Before we begin...
</strong>
</big></big></big></big></big>

<p>&nbsp;</p>
<big>
__introduce__ yourself to the person sitting next to you  

<p>&nbsp;</p>


__why__ did they decide to study __computing__?
</big>
</main>
-->

<!--
<main is="slide">

Feedback
=======

upload __lecture code__  


upload/incorporate __diagrams__  
in lecture recordings

<i>more</i> __diagrams__

go through programs __as a whole__  
before running them

lecture __subtitles__

</main>

-->

<main is="slide">

Overview
=======

<big><strong>after this lecture, you should be able to...</strong></big>

start on the last assignment: __Fruit Bot__

work with __multi-file__ C programs

understand the purpose of __.h__ files

have a better understanding of __linked lists__

write code to __free__ a linked list

solve simple problems using __linked lists__

<small><small><small>
(__note__: you shouldn't be able to do all of these immediately after
watching this lecture. however, this lecture should (hopefully!) give
you the foundations you need to develop these skills. remember:
programming is like learning any other language, it takes consistent and
regular practice.)
</small>
</small>
</small>
</main>


<main is="slide">

Admin
=====

<big>
<big><big><big><strong>Don't panic!</strong></big></big></big>
</big>

__assignment 2__ due yesterday  
(if you haven't started yet.... <big><strong>☹</strong></big>)


__assignment 3__ out now!  
<small>this week's tute/lab help you get started</small>

__week 10 weekly test__ due __thursday__

don't forget about __help sessions__!  
<small>see course website for details</small>

</main>

<!--
tomorrow
1. fruit bot

2. intro multi-file compilation (needed for assignment)

3. back to linked lists
        I need to do free-ing list

1.
fruit_bot motivation/overview,  leave fine details of the structs to tutorials/lab.

Explain world is randomly generate so can not depend on any particular fruit
or place being there

Maybe write and run a  trviial bot.

Point out the last lab exercise is a bot.

2.
Main think for multifile compilation is 
explaining prototypes #defines in .h file
because file is unit of compilation

3.
linked lists.... yes, I still need to cover more list traversals (maybe
like see whether a certain value exists in the list) - we've done
things like sum/length, but haven't looked at any more interesting
conditions.

ordered insert could also be good (didn't get that far), and yes freeing
lists too.

-->

<section name="fruit bot">

<main is="slide" class="title">

<big><h1>introducing: <strong>Fruit Bot</strong></h1></big>

(assignment 3)

<style>
img {
    max-width:100px;
}
</style>

...

<img src="https://emojipedia-us.s3.amazonaws.com/thumbs/240/google/133/watermelon_1f349.png"> BOT  <img src="https://emojipedia-us.s3.amazonaws.com/thumbs/240/google/133/tangerine_1f34a.png"> BOT <img src="https://emojipedia-us.s3.amazonaws.com/thumbs/240/google/133/banana_1f34c.png"> BOT <img src="https://emojipedia-us.s3.amazonaws.com/thumbs/240/google/133/red-apple_1f34e.png"> BOT

</main>
<main is="slide">

Assignment Spec
=====

<a class="orange"
href="https://cgi.cse.unsw.edu.au/~cs1511/18s1/assignments/ass3/index.html">
https://cgi.cse.unsw.edu.au/~cs1511/18s1/assignments/ass3/index.html
</a>

</main>
<main is="slide">

Fruit Bot
=====

<big><big>note: __do not change the structs__</big></big>

__stateless__ bot  
<small>(similar to <i>Intensity</i>)</small>


world is __randomly generated__  
<small>(you can't assume any particular fruit is/isn't there)</small>


</main>
</section>
<section name="multi-file compilation">
<main is="slide">

An Aside: Multiple C Files
=====

until now we've only had __one__ .c file per program

but, we can make programs with __multiple__ .c files

</main>
<main is="slide">

Scope
=====

to call a function you need to know about its  
__return type__  
__input parameters__  

<small>(we call this the __API__)</small>
</main>
<main is="slide">

Using Functions From Another File
=====

__first.c__

CODE
void hello(void);
int square(int n);

int main(void) {
    hello();
    printf("%d", square(5));
}

void hello(void) {
    printf("Hello!\n");
}

int square(int n) {
    return n*n;
}
NOCODE


CODE
$ dcc -o first first.c
$ ./first
Hello!
25
NOCODE

</main>
<main is="slide">

Using Functions From Another File
=====

__second.c__

CODE
int main(void) {
    hello();
    printf("%d", square(5));
}
NOCODE

CODE
$ dcc -o second second.c
????
$ ./second
????
NOCODE

</main>

<main is="slide">

Using Functions From Another File
=====

__first.h__

CODE
#ifndef FIRST_H
#define FIRST_H

void hello(void);
int square(int n);

#endif
NOCODE

</main>


<main is="slide">

Using Functions From Another File
=====

__second.c__

CODE
#include "first.h"

int main(void) {
    hello();
    printf("%d", square(5));
}
NOCODE

CODE
$ dcc -o second second.c first.c
$ ./second
Hello
25
NOCODE

</main>

</section>
<section name="linked lists">

<main is="slide" class="title">

<big><h1>and now for some more <strong>Linked Lists</strong></h1></big>

</main>
<!-- review -->
<main is="slide" class="title">

<h1>
&lt;REVIEW&gt;
</h1>

</main>

</section>
<section name="node struct">
<!--
<main is="slide" class="title">

<big><h1>introducing: <strong>something</strong></h1></big>

</main>
-->
<main is="slide">

The node struct
=====

CODE
struct node {
    int data;
    struct node *next;
};
NOCODE

</main>
<main is="slide">

Interacting with a node struct
=====

CODE
struct node {
    int data;
    struct node *next;
};

// "struct node hello" (no *)
// "hello" is an actual node in the function's memory
struct node hello;
hello.data = 10;
hello.next = NULL;

// in the function's memory
//        ______
// hello |  10  |
//       |------|
//       | NULL |
//       |______|
NOCODE

</main>
<main is="slide">

Making a new node
=====

CODE
// Allocates memory for a new node; returns its address
struct node *make_node(int value) {
    struct node *new = malloc(1 * sizeof(struct node));
    new->data = value;
    new->next = NULL;
    return new;
}

// "struct node * hello"
// "hello" is a pointer to a node,
// it just stores the _address_
// (of the memory we get from malloc)
struct node *hello = make_node(10);

// in the heap (malloced memory)
//        ______
// hello |  10  |
//       |------|
//       | NULL |
//       |______|
NOCODE

</main>
<main is="slide">

Freeing a node
=====

CODE
// In accordance with Newton's 3rd Law of Memory Allocation
// "For every malloc, there is an equal and opposite free"
void free_node(struct node *node) {
    free(node);
}

struct node *hello = make_node(10);
free_node(hello);
NOCODE

</main>
</section>

<section name="node pointers">

<main is="slide">

Node pointers vs allocated nodes
===

**reference** to a node  
<small>arrow</small>

CODE
struct node *curr ...
NOCODE


vs

making (**allocating**) a new node  
<small>circle</small>

CODE
... = malloc(1 * sizeof(struct node));
NOCODE

</main>
<main is="slide">

Node pointers vs allocated nodes
===

**reference** to a node  
<small>arrow</small>

CODE
struct node *curr ...
NOCODE


vs

making (**allocating**) a new node  
<small>circle</small>

CODE
... = malloc(1 * sizeof(struct node));
NOCODE

<img
src="https://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/diagrams/linked_lists/struct_node_star_cyan_alpha.png">


</main>
<main is="slide">

Node pointers vs allocated nodes
===
**reference** to a node <small>(arrow)</small> vs  
making (**allocating**) a new node <small>(circle)</small>

<img
src="https://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/diagrams/linked_lists/struct_node_star_curr_equals_malloc_cyab_alpha.png" style="max-width:700px">

</main>

</section>
<section name="list traversal">
<main is="slide" class="title">

__array__/__list__ "__traversal__"  
=================

<small>(going through every element)</small>

</main>

<main is="slide">

Traversing... an Array
======================

CODE
void fillArray (int array[ARRAY_SIZE], int value) {
    int i = 0;
    while (i < ARRAY_SIZE) {
        array[i] = value;  // set the value
        i++;               // move to next element
    }
}
NOCODE

</main>
<main is="slide">

Traversing... a Linked List
===========================

CODE
void fillList (struct node *list, int value) {
    struct node *curr = list;
    while (curr != NULL) {
        curr->data = value; // set the value
        curr = curr->next;   // move to next node
    }
}
NOCODE

</main>
</section>
<section name="list loop">
<main is="slide">

The Standard List Loop
================

CODE
struct node *curr = list;

while (curr != NULL) {

    ?????

    curr = curr->next;
}
NOCODE

</main>
<main is="slide">

The Standard List Loop -- List Length
================

How can we calculate the length of a list?  
<small>i.e. how many nodes are in the list</small>

CODE
struct node *curr = list;

int num_nodes = 0;

while (curr != NULL) {

    num_nodes += 1;

    curr = curr->next;
}
NOCODE

</main>
<main is="slide">

The Standard List Loop -- List Sum
================

How can we sum all of the elements in a list?  
<small>i.e. add the values of all of the nodes together</small>

CODE
struct node *curr = list;

// int num_nodes = 0;
?????

while (curr != NULL) {

    // num_nodes += 1;
    ?????

    curr = curr->next;
}
NOCODE

</main>
<main is="slide" class="title">

<h1>
&lt;/REVIEW&gt;
</h1>

</main>
</section>

<section name="more list iteration">
<main is="slide">

More List Iteration
=====

does the list contain a certain __value__?

</main>
</section>
<section name="freeing a list">

<main is="slide">

Freeing a List
=====

_"For every malloc, there is an equal and opposite free."_

we need to free our entire list when we're done with it

</main>
</section>

<section name="end">

<!--
<main is="slide">

Feedback?
=========


<strong><big>
<a href="http://bit.do/comp1511-feedback-week4" style="color:#f9b200">
bit.do/comp1511-feedback-week4</a>
</big></strong>

<hr>

<img
src="http://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/week04/comp1511-week4-feedback-qr.png" />

alternate link: <a href="https://andrewb3.typeform.com/to/WrAhfV"
style="color:#f9b200">
https://andrewb3.typeform.com/to/WrAhfV
</a>
</main>

-->

<main is="slide">
end
</main>

</section>

<!-- code highlighted
<pre><code class="lang-clike">
if (condition 1) {
    if (condition 2) {
        if (condition 3) {
            // Do stuff
        } else if (condition 4) {
            // Do something else
        }
    } else if (condition 5) {
        if (condition 6) {
            // Do something completely different
        }
    } else {
        // In all other cases, do this.
    }
}
</code></pre>
-->


