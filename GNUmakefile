########################################################################
# <help>
# Greetings, adventurer, and welcome to this site's build system.
# This is a static HTML build system, using GNU Make, M4, and Pandoc.
#
# Targets
# -------
#
# site
#     .gen              forcibly rebuild everything
#     .build            incrementally rebuild everything
#     .clean            delete everything
#     .serve            serve locally (BINDHOST[=0.0.0.0], BINDPORT[=8000])
#
# page
#     .list             list all source pages
#     .add              add a new page (guesses name, or specify $N)
#     .check            syntax check page (all pages, or specify $N)
#     .edit             edit a page (specify $N)
#     .rm               remove a page (specify $N)
#
# remote
#     .list             list known remotes
#     .sync[.REMOTE]    sync all remotes, or a specified REMOTE
#
# </help>
#
# 2018-02-21	Jashank Jeremy <{jashankj,z5017851}@cse.unsw.edu.au>

########################################################################
### Configuration options.

# Paths
SITE_DIR        ?= _site
VENDOR_DIR      ?= ${S}/_vendor
TEMPLATE_FILE   ?= ${S}/_template.html
PAGE_FILE       ?= ${S}/_pages.list
STATICS_FILE    ?= ${S}/_statics.list

#SITE_URL        ?= /
SITE_URL        ?= /~andrewb/slides/

# Flags for tools.
PANDOC_FLAGS    ?= --standalone --template=${TEMPLATE_FILE}
PANDOC_FLAGS    += -M site-url=${SITE_URL}
PANDOC_FLAGS    += --mathjax
#PANDOC_FLAGS   += --self-contained -M site-url=${SITE_DIR}
SASS_FLAGS      ?= -r bootstrap

PARALLELISM     ?= 4

REMOTES		+= cse
REMOTE.rsync.cse = andrewb@williams.cse.unsw.edu.au:/web/andrewb/slides
REMOTE.http.cse	 = https://www.cse.unsw.edu.au/~andrewb/slides

########################################################################
### Output
ifdef V
    Q :=
else
    Q := @
endif

E  = @echo '	'"$(1)"
Ee = echo '    '"$(1)"
_VT_BOLD=[1m
_VT_NORMAL=[0m
_VT_RED=[1;31m
_VT_GREEN=[1;32m
_VT_ORANGE=[1;33m
_VT_BLUE=[1;34m
_VT_MAGENTA=[1;35m
_VT_CYAN=[1;36m
_VT_WHITE=[1;37m

S := $(shell pwd)

MKDIR	?= mkdir -p
M4	?= m4
PANDOC	?= pandoc
SASS	?= sass
SED	?= sed -E

ifeq ($(shell uname),FreeBSD)
  M4 = gm4
  SED = gsed -E
endif

# $(1) is the name of the doc <section> in Makefile.
# pick everything between tags | remove first line | remove last line
# | remove extra (?) line | strip leading `#` from lines
SHOW_DOCS = $(Q)awk '/<$(1)>/,/<\/$(1)>/' $(S)/GNUmakefile | ${SED} '1d' | ${SED} '$$d' | ${SED} 's/^\# \?//'
MAKEFILE_DOCS = help
.PHONY: ${MAKEFILE_DOCS}
${MAKEFILE_DOCS}:
	$(call SHOW_DOCS,$@)


################################################################################
### site

PAGES		:= $(shell cat ${PAGE_FILE} | cc -E - | ${SED} -e '/^\#/d; s/\#.*//g')
STATICS		:= $(shell cat ${STATICS_FILE} | cc -E - | ${SED} -e '/^\#/d; s/\#.*//g')

${SITE_DIR}::
	@mkdir -p ${SITE_DIR}

.PHONY: site.build
site.build::
	$(call E,* ${_VT_BOLD}building site...${_VT_NORMAL})
	@${MAKE} .do.site.build

.PHONY: .list.site.build
.list.site.build:
	@echo $(addprefix ${SITE_DIR}/,${STATICS}) \
		$(addprefix ${SITE_DIR}/,index.html) \
		$(addprefix ${SITE_DIR}/,$(addsuffix /index.html,$(basename ${PAGES})))

.PHONY: .do.site.build
.do.site.build:
	@${MAKE} --no-print-directory .list.site.build \
		| tr ' ' '\n' \
		| xargs -r -P ${PARALLELISM} -L 1 ${MAKE} --no-print-directory -s ${i}

.PHONY: site.gen
site.gen::
	@${MAKE} --no-print-directory site.clean
	@${MAKE} --no-print-directory site.build

.PHONY: site.clean
site.clean::
	$(call E,* ${_VT_BOLD}cleaning site...${_VT_NORMAL})
	@-rm -rf ${SITE_DIR}

BINDHOST	?= 0.0.0.0
BINDPORT	?= 8080
.PHONY: site.serve
site.serve::
	$(call E,+ ${_VT_BOLD}serving at http://${BINDHOST}:${BINDPORT}/${_VT_NORMAL})
	@ruby -run -e httpd -- --bind-address=${BINDHOST} --port=${BINDPORT} ${SITE_DIR}


########################################################################
### page.list and friends

...describe/%.markdown: %.markdown
	@cat $< \
		| ${SED} -ne '/^---$$/,/^\.\.\.$$/{p}'

..describe/%.markdown: %.markdown
	@cat $< \
		| ${SED} -ne '/^---$$/,/^\.\.\.$$/{p}' \
		| yaml2json - \
		| JSON.sh

.describe/%.markdown: %.markdown
	@printf "%-40s %s\n" \
		$(basename $<) \
		"$$(${MAKE} .$@ | grep '^\[\"title\"\]' | cut -f2)"

.PHONY: page.list
page.list::
	$(call E,* ${_VT_BOLD}listing pages${_VT_NORMAL})
	@echo ${PAGES} \
		| tr ' ' '\n' \
		| ${SED} -e 's/^/.describe\//' \
		| xargs -r -P ${PARALLELISM} -L 1 ${MAKE} --no-print-directory

ifeq (${n},)
  n := $(shell date +%Y-%m-%d)-page.markdown
else
  n := $(addsuffix .markdown,$(basename ${n}))
endif

.PHONY: page.new
page.new::
	@mkdir -p $$(dirname ${n})
	@if grep -qx -- "${n}" ${PAGE_FILE}; then true; else \
		echo ${n} >> ${PAGE_FILE}; \
	 fi
	@if [ \! -e "${n}" ]; then \
		(echo '---'; \
		 echo 'title: ""'; \
		 echo 'datetime: ""'; \
		 echo '...'; \
		 echo '') > ${n}; \
	 fi
	@git add -- ${PAGE_FILE} "${n}"

.PHONY: page.check
page.check::

.PHONY: page.edit
page.edit::

.PHONY: page.rm
page.rm::

.NOTPARALLEL: .link/%.markdown
.link/%.markdown: %.markdown
	@printf -- "- [%s](%s)\n" \
		"$$(${MAKE} ...describe/$< | grep '^title: ' | ${SED} -e 's/^title: //' | tr -d '\"')" \
		$(addprefix ${SITE_URL},$(addsuffix /,$(basename $<)))

.sitemap::
	$(Q)echo ${PAGES} \
		| tr ' ' '\n' \
		| ${SED} -e 's/^/.link\//' \
		| xargs -r -P $$(expr ${PARALLELISM} \* 4) -L 1 ${MAKE} MAKEFLAGS= --no-print-directory \
		| sort -nr -k3

########################################################################
### build rules

define markdown_to_html
	$(call E,→ [m4|pandoc] $1)
	${Q}${MKDIR} $(dir $2)
	+${Q}(echo "changequote(\`{{', \`}}')dnl"; cat $1) \
		| ${M4} \
		| ${PANDOC} --from markdown --to html5 ${PANDOC_FLAGS} \
		| ${SED} -e "s%@SITEURL@%${SITE_URL}%g" \
		> $2
endef

${SITE_DIR}/%/index.html: %.markdown ${TEMPLATE_FILE}
	$(call markdown_to_html,$<,$@)

${SITE_DIR}/index.html: index.markdown ${TEMPLATE_FILE}
	$(call markdown_to_html,$<,$@)

${SITE_DIR}/%.css: %.scss
	$(call E,→ [sassc] $<)
	$(Q)${MKDIR} $(dir $@)
	$(Q)${SASS} ${SASS_FLAGS} $< $@

COPY_EXTS	?= js svg png jpg pdf ps asc pgp

define copy_tgt
${SITE_DIR}/%.$(1): %.$(1)
	$(call E,→ [cp] $$<)
	$(Q)${MKDIR} $$(dir $$@)
	$(Q)cp $$< $$@
endef
$(foreach ext,${COPY_EXTS},$(eval $(call copy_tgt,${ext})))

${SITE_DIR}/.htaccess: .htaccess
	$(call E,→ [cp] $<)
	$(Q)${MKDIR} $(dir $@)
	$(Q)cp $< $@


################################################################################
### remotes

.PHONY: remote.list
remote.list::
	$(call E,* ${_VT_GREEN}remotes${_VT_NORMAL})
	@$(foreach r,${REMOTES},$(call Ee,${_VT_BOLD}$r${_VT_NORMAL} (${REMOTE.http.${r}}));)

define remote_targets
.PHONY: remote.sync.$(1)
remote.sync.$(1)::
	$(call E,* ${_VT_GREEN}rsyncing to $(1)...${_VT_NORMAL})
	$(Q)rsync -avu ${SITE_DIR}/ ${REMOTE.rsync.$(1)}
endef

$(foreach r,${REMOTES},$(eval $(call remote_targets,${r})))
