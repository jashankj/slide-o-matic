---
short-title: COMP1511 18s1 lec04
title: "Lecture 7: Assignment 1, Unit Testing, More Arrays+Functions"
...


<style>
pre {
    padding-top: 0em !important;
}
</style>

<main is="slide" class="title">
<h1><big><strong>Welcome!</strong></big></h1>

<h1>COMP1511 18s1<br />
    <small>Programming Fundamentals</small></h1>
</main>

<main is="slide" class="title">
<h1>COMP1511 18s1<br />
  <strong>— Lecture 7 —</strong><br />
  <small>Unit Testing, Arrays + Functions</small></h1>

Andrew Bennett  
<small><tt>&lt;andrew.bennett@unsw.edu.au&gt;</tt></small>


</main>

<main is="slide">

<p>&nbsp;</p>

<big><big><big><big><big>
<strong>
Before we begin...
</strong>
</big></big></big></big></big>

<p>&nbsp;</p>
<big>
__introduce__ yourself to the person sitting next to you  

<p>&nbsp;</p>


__why__ did they decide to study __computing__?
</big>
</main>


<main is="slide">

Overview
=======

<big><strong>after this lecture, you should be able to...</strong></big>

<!--
  - talk about the assignment (and thus asserts / unit testing)
  - talk more about the approach to solving a problem / breaking it down
    into steps etc (motivated by students in my tute struggling with how
to approach danish flag)
  - talk more about functions -- students are confused about passing
    variables into functions (need to reiterate that it passes the
contents of the variable, not the variable itself, so names don't matter
but order does -- had lots of students ask me about that [names vs order
of parameters] after the lecture)
  - play around more with arrays
      - I want to "accidentally" go over the end of an array or go
        backwards in an array or something interesting
      - I'm planning to do something like find the min value / find the
        max value (and hint at "could we print the numbers in sorted
order?" for the bored students)
-->


understand the basics of __unit testing__ using __asserts__

understand the basics of __designing a solution__ to a problem

have a rough understanding of __Assignment 1__  

understand more about passing __parameters__ into __functions__  

write programs using __arrays__ to solve simple problems


<small><small><small>
(__note__: you shouldn't be able to do all of these immediately after
watching this lecture. however, this lecture should (hopefully!) give
you the foundations you need to develop these skills. remember:
programming is like learning any other language, it takes consistent and
regular practice.)
</small>
</small>
</small>
</main>


<main is="slide">

Admin
=====

<big><big><big><strong>Don't panic!</strong></big></big></big>

lecture recordings are on WebCMS3  

course __style guide__ published

__weekly test__ due tonight  
<small>don't be scared!</small>

__assignment 1__ released!  

don't forget about __help sessions__!  
<small>see course website for details</small>


</main>

<section name="assignment-1">
<main is="slide">

time2code
====

(assignment spec on course website)

</main>
<main is="slide">

C Requirements
====

if statements  
functions  
asserts (unit testing)  

</main>


</section>

<section name="asserts">

<main is="slide">

Unit Tests
=====================

<big>
"As well as testing my whole program,  
I'll test each of the small parts of it."
</big>

faster and easier to check our __small units__  
and then check the whole program

</main>
<main is="slide" data-section-name="testing">

Testing with `assert`
=====================

<pre><code class="lang-clike">
#include &lt;assert.h&gt;
</code></pre>

...

<big>
<pre><code class="lang-clike">
    int i = 3;
    assert (i == 3);

    assert (doubled(3) == 6);

</code></pre>
</big>

</main>


</section>


<section name="functions">

<main is="slide" data-section-name="review">

Review: Features of Functions
=============================

a function can have zero or more parameter(s)

a function can only return zero or one value(s)

`* * *`

a function stores a local copy of parameters passed to it

the original values of variables remain unaltered

parameters received by the function,  
and local variables created by the function,  
are all __discarded__ when the function returns

</main>

<main is="slide">

Review: Function Parameters
===========

__names__ don't matter

__order__ matters

</main>

</section>



<section name="end">
<main is="slide">

Feedback?
=========


<strong><big>
<a href="http://bit.do/comp1511-feedback-week4" style="color:#f9b200">
bit.do/comp1511-feedback-week4</a>
</big></strong>

<hr>

<img
src="http://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/week04/comp1511-week4-feedback-qr.png" />

alternate link: <a href="https://andrewb3.typeform.com/to/WrAhfV"
style="color:#f9b200">
https://andrewb3.typeform.com/to/WrAhfV
</a>

<main is="slide">
end
</main>

</section>

<!-- code highlighted
<pre><code class="lang-clike">
if (condition 1) {
    if (condition 2) {
        if (condition 3) {
            // Do stuff
        } else if (condition 4) {
            // Do something else
        }
    } else if (condition 5) {
        if (condition 6) {
            // Do something completely different
        }
    } else {
        // In all other cases, do this.
    }
}
</code></pre>
-->


