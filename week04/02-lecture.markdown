---
short-title: COMP1511 18s1 lec04
title: "Lecture 6: Loops, Arrays"
...


<style>
pre {
    padding-top: 0em !important;
}
</style>

<main is="slide" class="title">
<h1><big><strong>Welcome!</strong></big></h1>

<h1>COMP1511 18s1<br />
    <small>Programming Fundamentals</small></h1>
</main>

<main is="slide" class="title">
<h1>COMP1511 18s1<br />
  <strong>— Lecture 6 —</strong><br />
  <small>Loops + Arrays</small></h1>

Andrew Bennett  
<small><tt>&lt;andrew.bennett@unsw.edu.au&gt;</tt></small>

loops inside loops  
stopping loops  
arrays

</main>

<main is="slide">

<p>&nbsp;</p>

<big><big><big><big><big>
<strong>
Before we begin...
</strong>
</big></big></big></big></big>

<p>&nbsp;</p>
<big>
__introduce__ yourself to the person sitting next to you  

<p>&nbsp;</p>


__why__ did they decide to study __computing__?
</big>
</main>



<main is="slide">

Feedback
=======

upload __lecture code__  


upload/incorporate __diagrams__  
in lecture recordings

<i>more</i> __diagrams__

go through programs __as a whole__  
before running them

lecture __subtitles__

</main>



<main is="slide">

Overview
=======

<big><strong>after this lecture, you should be able to...</strong></big>


write programs using __nested while loops__ to solve simple problems

understand how to __stop__ loops  
using __loop counters__ and __sentinel variables__

understand the basics of __arrays__

understand the basics of __designing a solution__ to a problem  
<small><small>(time permitting)</small></small>

<small><small><small>
(__note__: you shouldn't be able to do all of these immediately after
watching this lecture. however, this lecture should (hopefully!) give
you the foundations you need to develop these skills. remember:
programming is like learning any other language, it takes consistent and
regular practice.)
</small>
</small>
</small>
</main>


<main is="slide">

Admin
=====

<big><big><strong>Don't panic!</strong></big></big>

__lecture recordings__ are on WebCMS3  
<small>I'll try to add drawings/diagrams + upload to YouTube</small>

course __style guide__ published

__weekly test__ due wednesday  
<small>don't be scared!</small>

__assignment 1__ coming soon  
<small>more on that tomorrow!</small>

don't forget about __help sessions__!  
<small>see course website for details</small>

</main>

<section name="loops">
<main is="slide">

Loops
=====

what if we want to do something multiple times?

**Use a loop!**

<i>keep doing this __while__ this condition is true</i>

</main>
<main is="slide">

Anatomy of a Loop
=================

__initialisation__  
set up our variables

__condition__  
while "something"...

__statements__  
things we do inside our loop

__update__  
move along to the next iteration

</main>
<main is="slide">

Anatomy of a Loop
=================

<pre><code class="lang-clike">
// initialisation
int i = 0;

// condition
while (i < n) {
    // statements -- do something in the loop
    printf("Hello!\n");

    // update
    i++;
}
</code></pre>

</main>


<main is="slide">

Stopping: Loop Counters
====

<pre><code class="lang-clike">
// Print out "hello, world!" n times,
// where n is chosen by the user.

int num;
printf ("Enter a number: ");
scanf ("%d", &num);

int i = 0;
while (i < num) {
    printf ("hello, world!\n");
    i = i + 1;
}
</code></pre>


</main>

<main is="slide">

Stopping: Sentinel Value (Flag)
====

<pre><code class="lang-clike">
// Print out the number that the user entered
// Stop when they type 0

int n = 1;
while (n != 0) {
    printf ("You entered: %d\n", n);
    scanf("%d", &n);
}
</code></pre>

<!-- demos on this:

bar graph

scan numbers and reverse


snap
  - look at array overflowing / don't specify a limit
  - so just scan numbers until we hit a repeat
-->

</main>


<main is="slide">

Nested Loops
======

<pre><code class="lang-clike">
while (something) {
    while (somethingElse) {

    }
}
</code></pre>

</main>



<main is="slide">

Nested Loops
======

<pre><code class="lang-clike">
int i = 0;
while (i < n) {
    int j = 0;
    while (j < n) {
        // do something
        printf("!!");
        j++;
    }
    i++;
}
</code></pre>

</main>
</section>

<section name="arrays">

<main is="slide" data-section-name="memory" class="title">

<big><h1>revisiting: <strong>variables</strong></h1></big>

</main>


<main is="slide" data-section-name="memory">

Variables
============

variables are like a __box__  
that can hold a __value__  
of a certain __type__


<pre><code class="lang-clike">
int i = 5;
 ______
|      |
|   5  |
|______|
</code></pre>

</main>
<main is="slide" data-section-name="memory">

Variables
============

we can have as many variables as we like

<pre><code class="lang-clike">
int i = 5;
 ______
|      |
|   5  |
|______|

int age = 18;
 ______
|      |
|  18  |
|______|

double pi = 3.14;
 ______
|      |
| 3.14 |
|______|
</code></pre>

</main>
<main is="slide" data-section-name="memory">

Lots of Variables
============

sometimes we want to store a lot of related variables

<pre><code class="lang-clike">
int mark_student0 = 85;
 ______
|      |
|  85  |
|______|

int mark_student1 = 90;
 ______
|      |
|  90  |
|______|

int mark_student2 = 45;
 ______
|      |
|  45  |
|______|
</code></pre>
</main>
<main is="slide" data-section-name="memory">

Lots of Variables
============

sometimes we want to store a lot of related variables

<pre><code class="lang-clike">
int mark_student0 = 85;
int mark_student1 = 90;
int mark_student2 = 45;
</code></pre>

<pre><code class="lang-clike">
double averageMark = (mark_student1 + mark_student2 + mark_student3)/3;
printf("The average mark was: %lf\n", averageMark);
</code></pre>

<big><strong>This doesn't scale!</strong></big>

</main>


<main is="slide" class="title">

<big><h1>introducing: <strong>arrays</strong></h1></big>

</main>


<main is="slide" data-section-name="arrays">

Arrays
======

A series of boxes with a common type,  
all next to each other

<pre><code class="lang-other">
╔════╤════╤════╤════╦════╤════╤════╤════╦════╤════╤════╤════╦┄
║    │    │    │    ║    │    │    │    ║    │    │    │    ║
╚════╧════╧════╧════╩════╧════╧════╧════╩════╧════╧════╧════╩┄
</code></pre>

</main>




<main is="slide" data-section-name="arrays">

Arrays
======

A series of boxes with a common type,  
all next to each other

<pre><code class="lang-other">
  0    1    2    3    4    5    6    7    8    9    10    11
╔════╤════╤════╤════╦════╤════╤════╤════╦════╤════╤════╤════╦┄
║    │    │    │    ║    │    │    │    ║    │    │    │    ║
╚════╧════╧════╧════╩════╧════╧════╧════╩════╧════╧════╧════╩┄
</code></pre>



</main>

<main is="slide" data-section-name="arrays">

Why?
====

Suppose we need to compute statistics on class marks...

<pre><code class="lang-clike">
int mark_student0, mark_student1, mark_student2, ...;
mark_student0 = 85;
mark_student1 = 90;
mark_student2 = 45;
...
</code></pre>

becomes unfeasible if dealing with a lot of values  
... we'd need hundreds of individual variables!

</main>
<main is="slide" data-section-name="arrays">

Why?
====

Solution: Use an array!

<pre><code class="lang-clike">
int mark[1160];
mark[0] = 85;
mark[1] = 90;
mark[2] = 45;
...
</code></pre>

</main>
<main is="slide" data-section-name="arrays">

Arrays in C
===========

a collection of array __elements__  
each element must be the same type

we refer to arrays by their __index__  
valid indices for $$n$$ elements are $$0 \ldots n − 1$$

no real limit on number of elements

we __cannot__ assign, scan, or print whole arrays...  
but we __can__ assign, scan, and print elements

</main>
<main is="slide" data-section-name="arrays">

Arrays in C
===========

<pre><code class="lang-clike">
// Declare an array with 10 elements
// and initialises all elements to 0.
int myArray[10] = {0};

</code></pre>

<pre><code class="lang-other">
┌────┬────┬────┬────┬────┬────┬────┬────┬────┬────┐
│  0 │  0 │  0 │  0 │  0 │  0 │  0 │  0 │  0 │  0 │
└────┴────┴────┴────┴────┴────┴────┴────┴────┴────┘
| 0  | 1  | 2  | 3  | 4  | 5  | 6  | 7  | 8  | 9
</code></pre>

</main>
<main is="slide" data-section-name="arrays">

Arrays in C
===========

<pre><code class="lang-clike">
int myArray[10] = {0};
// Put some values into the array.
myArray[0] = 3;
myArray[5] = 17;

</code></pre>

<pre><code class="lang-other">
┌────┬────┬────┬────┬────┬────┬────┬────┬────┬────┐
│  3 │  0 │  0 │  0 │  0 │ 17 │  0 │  0 │  0 │  0 │
└────┴────┴────┴────┴────┴────┴────┴────┴────┴────┘
| 0  | 1  | 2  | 3  | 4  | 5  | 6  | 7  | 8  | 9
</code></pre>

</main>
<main is="slide" data-section-name="arrays">

Arrays in C
===========

<pre><code class="lang-clike">
int myArray[10] = {0};
// Put some values into the array.
myArray[0] = 3;
myArray[5] = 17;
myArray[10] = 42; // <-- Error
</code></pre>

<pre><code class="lang-other">
┌────┬────┬────┬────┬────┬────┬────┬────┬────┬────┐
│  3 │  0 │  0 │  0 │  0 │ 17 │  0 │  0 │  0 │  0 │ ???
└────┴────┴────┴────┴────┴────┴────┴────┴────┴────┘
| 0  | 1  | 2  | 3  | 4  | 5  | 6  | 7  | 8  | 9
</code></pre>

</main>
<main is="slide" data-section-name="arrays">

Reading an Array
================

`scanf()` can't read an entire array.  
this will only read 1 number:

<pre><code class="lang-clike">
#define ARRAY_SIZE 42
...
int array[ARRAY_SIZE];
scanf ("%d", &array);
</code></pre>

instead, you must read the elements one by one:

<pre><code class="lang-clike">
int i = 0;
while (i < ARRAY_SIZE) {
    scanf ("%d", &array[i]);
    i++;
}
</code></pre>

</main>
<main is="slide" data-section-name="arrays">

Printing an Array
=================

`printf()` also can't print an entire array.  
this won't compile...

<pre><code class="lang-clike">
#define ARRAY_SIZE 42
...
int array[ARRAY_SIZE];
printf ("%d", array);
</code></pre>

instead, you must print the elements one by one:

<pre><code class="lang-clike">
int i = 0;
while (i < ARRAY_SIZE) {
    printf ("%d", array[i]);
    i++;
}
</code></pre>

</main>
<main is="slide" data-section-name="arrays">

Copying an Array
================

given:

<pre><code class="lang-clike">
#define ARRAY_SIZE 5
int array1[ARRAY_SIZE] = {1, 4, 9, 16, 25};
int array2[ARRAY_SIZE];
</code></pre>

this won't compile...

<pre><code class="lang-clike">
array2 = array1;
</code></pre>

instead, you must copy the elements one by one:

<pre><code class="lang-clike">
int i = 0;
while (i < ARRAY_SIZE) {
    array2[i] = array1[i];
    i++;
}
</code></pre>

</main>
<!--
<main is="slide" data-section-name="arrays">

Array-ception
=============

an array may have elements of any type...  
including of array type!  
we call these __multi-dimensional__ arrays

here's an array of arrays of `int`:

<pre><code class="lang-clike">
int matrix[3][3] = {
    {1, 2, 3},
    {4, 5, 6},
    {7, 8, 9}
};
</code></pre>

it's a two-dimensional array.

<pre><code class="lang-clike">
printf ("%d\n", matrix[1][1]); // outputs... ?
</code></pre>

</main>
<main is="slide" data-section-name="arrays">

Array-ception
=============

the same caveats apply to multi-dimensional arrays:  
we can't read, print, or copy the whole array,  
but have to copy each non-array element...

this usually means we need to use __nested loops__

<pre><code class="lang-clike">
#define SIZE 42

int matrix[SIZE][SIZE] = { {0} };
int i = 0;
while (i < SIZE) {
    int j = 0;
    while (j < SIZE) {
        scanf ("%d", &matrix[i][j]);
        j++;
    }
    i++;
}
</code></pre>

</main>
-->



<!--
<main is="slide">

title
====

content

</main>
-->
<!--
<main is="slide">

Demo: Printing a Square
======

Scan in a number: __width__
Print out a square of __width__ * __width__ stars.

e.g. for width = 4:
<pre>
* * * *
* * * *
* * * *
* * * *
</pre>

</main>
-->

<section name="end">
<!--
<main is="slide">

Feedback?
=========


<strong><big>
<a href="http://bit.do/comp1511-feedback-week3" style="color:#f9b200">
bit.do/comp1511-feedback-week3</a>
</big></strong>

<hr>

<img
src="http://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/week03/comp1511-week3-feedback-qr.png" />

alternate link: <a href="https://andrewb3.typeform.com/to/KuVZP4"
style="color:#f9b200">
https://andrewb3.typeform.com/to/KuVZP4
</a>

</main>
-->

<main is="slide">
end
</main>

</section>

<!-- code highlighted
<pre><code class="lang-clike">
if (condition 1) {
    if (condition 2) {
        if (condition 3) {
            // Do stuff
        } else if (condition 4) {
            // Do something else
        }
    } else if (condition 5) {
        if (condition 6) {
            // Do something completely different
        }
    } else {
        // In all other cases, do this.
    }
}
</code></pre>
-->


