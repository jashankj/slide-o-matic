---
short-title: COMP1511 18s1 lec04
title: "Lecture 8: Strings"
...


<style>
pre {
    padding-top: 0em !important;
}
</style>

<main is="slide" class="title">
<h1><big><strong>Welcome!</strong></big></h1>

<h1>COMP1511 18s1<br />
    <small>Programming Fundamentals</small></h1>
</main>

<main is="slide" class="title">
<h1>COMP1511 18s1<br />
  <strong>— Lecture 8 —</strong><br />
  <small>Strings</small></h1>

Andrew Bennett  
<small><tt>&lt;andrew.bennett@unsw.edu.au&gt;</tt></small>

chars  
arrays of chars  
strings  

</main>

<main is="slide">

<p>&nbsp;</p>

<big><big><big><big><big>
<strong>
Before we begin...
</strong>
</big></big></big></big></big>

<p>&nbsp;</p>
<big>
__introduce__ yourself to the person sitting next to you  

<p>&nbsp;</p>


__why__ did they decide to study __computing__?
</big>
</main>


<!--
<main is="slide">

Feedback
=======

upload __lecture code__  


upload/incorporate __diagrams__  
in lecture recordings

<i>more</i> __diagrams__

go through programs __as a whole__  
before running them

lecture __subtitles__

</main>

-->

<main is="slide">

Overview
=======

<big><strong>after this lecture, you should be able to...</strong></big>


understand the basics of __chars__

understand what __ASCII__ is

understand the basics of __strings__

write programs using __strings__ to solve simple problems

<small><small><small>
(__note__: you shouldn't be able to do all of these immediately after
watching this lecture. however, this lecture should (hopefully!) give
you the foundations you need to develop these skills. remember:
programming is like learning any other language, it takes consistent and
regular practice.)
</small>
</small>
</small>
</main>


<main is="slide">

Admin
=====

<big><big><strong>Don't panic!</strong></big></big>

course __style guide__ published

__week 4 weekly test__ due friday  
<small>don't be scared!</small>

__assignment 1__ out now  
<small>work on it regularly!</small>

additional __autotests__ added to the assignment

don't forget about __help sessions__!  
<small>see course website for details</small>

</main>

<!--
<section name="more-arrays">

arrays as functions parameters
  constant length array
  array length as parameter
  special terminating elelemnt

uninitialized array elements - show them dcc dashdash valgrind

</section>
-->


<section name="beyond-numbers">

<main is="slide">

Beyond Numbers
=====

we've mostly seen numbers thus far  
`int age = 18;`  
`double pi = 3.14`

what else might we want to store?

</main>

<main is="slide">

Letters and Words
=====

what about words?

<pre><code class="lang-clike">
printf("andrew is awesome");
</code></pre>

</main>


<main is="slide">

Letters and Words
=====

what about words?

<pre><code class="lang-clike">
printf("andrew is awesome");
</code></pre>

__"andrew is awesome"__

</main>

<main is="slide">

Letters and Words
=====

words in C are called __strings__

<pre><code class="lang-clike">
printf("andrew is awesome");
</code></pre>

__"andrew is awesome"__

^ this is a __string__

</main>
</section>

<section name="strings-and-chars">

<main is="slide" class="title">

<big><h1>introducing: <strong>strings</strong></h1></big>


</main>

<main is="slide">

Strings
=====

a __string__ is an __array__ of __characters__.

<small>
<small>
note: a __character__ is a "printed or written letter or symbol".
</small>
</small>

</main>


<main is="slide">

Characters
=====

a __character__ generally refers to a  
__letter__, __number__, __punctuation__, etc.

in C we call it a `char`

</main>


<main is="slide">

Characters in C
=====

in C we call it a `char`

<pre><code class="lang-clike">
// making an int
int age = 18;

// making a char
char letter = 'A';
</code></pre>

`char`s go inside single quotes, i.e. `'`.  
strings go inside double quotes, i.e. `"`.
</main>


<main is="slide">

Characters in C
=====

`char` stores small integers.

8 bits (almost always).

mostly used to store __ASCII__ character codes

<small>
don't use for individual variables, only arrays  
only use `char` for characters  
<small>(not to store e.g. numbers between 0-9)</small>
</small>


</main>




</section>

<section name="ASCII">

<main is="slide">

ASCII
=====

__ASCII__ is a way of __mapping numbers__ to __characters__.

it contains:

upper and lower case __English letters__: A-Z and a-z  

__digits__: 0-9  

common __punctuation__ symbols  

special non-printing characters: e.g newline and space.

</main>

<main is="slide">

ASCII
=====

<big><strong>
you don’t have to memorize ASCII codes!
</strong></big>

single quotes give you the ASCII code for a character:
<pre><code class="lang-clike">
printf("%d", 'a'); // prints 97
printf("%d", 'A'); // prints 65
printf("%d", '0'); // prints 48
printf("%d", ' ' + '\n'); // prints 42 (32 + 10)
</code></pre>

don't put ASCII codes in your program - use __single quotes__
instead!

</main>

<main is="slide">

ASCII
=====

let's try it out!

</main>

</section>

<section name="more-chars">

<main is="slide">

Reading chars
=====

`getchar()`

reads a __byte__ from standard input  
returns an __int__  

returns a special value if it __can't__ read a byte  
otherwise returns an integer (0..255)  
(ASCII code)

<small>let's try it out!</small>

</main>

<main is="slide">

getchar
====

consider the following code:

<pre><code class="lang-clike">
int c1, c2;
printf("Please enter first character:\n");
c1 = getchar();
printf("Please enter second character:\n");
c2 = getchar();
printf("First %d\nSecond: %d\n", c1, c2);
</code></pre>

what should this do?

what does it actually do?

(how can we fix it?)

</main>

<main is="slide">

getchar
====

<pre><code class="lang-clike">
int c1, c2;
printf("Please enter first character:\n");
c1 = getchar();
printf("Please enter second character:\n");
c2 = getchar();
printf("First %d\nSecond: %d\n", c1, c2);
</code></pre>

what should this do? __read two typed characters__

what does it actually do? __read one typed character + enter__

</main>


<main is="slide">

getchar
====

how can we fix it?

<pre><code class="lang-clike">
int c1, c2;
printf("Please enter first character:\n");
c1 = getchar();

getchar(); // extra getchar to catch the newline

printf("Please enter second character:\n");
c2 = getchar();
printf("First %d\nSecond: %d\n", c1, c2);
</code></pre>


</main>


<main is="slide">

End Of Input
====

__scanf__ or __getchar__ will fail if there isn't any more input  
<small>eg if you're reading from a file and reach the end of the
file</small>

__getchar__ returns a special value to indicate no more input is
available

we call this value __EOF__

(how could you check this with __scanf__?)

</main>


<main is="slide">

Reading until End of Input
====

<pre><code class="lang-clike">
int c;
c = getchar();
while (c != EOF) {
    printf("’%c’ read, ASCII code is %d\n", c, c);
    c = getchar();
}
</code></pre>

reading numbers until end of input with scanf:

<pre><code class="lang-clike">
int num;
// scanf returns the number of items read
while (scanf("%d", &num) == 1) {
    printf("you entered the number: %d\n", num);
}
</code>
</pre>

</main>

</section>


<section name="strings">

<main is="slide">

Strings
=====

strings are an __array__ of __characters__


</main>


<main is="slide" data-section-name="arrays">

Remember Arrays?
======

A series of boxes with a common type,  
all next to each other

<pre><code class="lang-other">
  0    1    2    3    4    5    6    7    8    9    10    11
╔════╤════╤════╤════╦════╤════╤════╤════╦════╤════╤════╤════╦┄
║    │    │    │    ║    │    │    │    ║    │    │    │    ║
╚════╧════╧════╧════╩════╧════╧════╧════╩════╧════╧════╧════╩┄
</code></pre>

</main>

<main is="slide" data-section-name="arrays">

Arrays in C
===========

<pre><code class="lang-clike">
// Declare an array with 10 elements
// and initialises all elements to 0.
int myArray[10] = {0};

</code></pre>

<pre><code class="lang-other">
┌────┬────┬────┬────┬────┬────┬────┬────┬────┬────┐
│  0 │  0 │  0 │  0 │  0 │  0 │  0 │  0 │  0 │  0 │
└────┴────┴────┴────┴────┴────┴────┴────┴────┴────┘
| 0  | 1  | 2  | 3  | 4  | 5  | 6  | 7  | 8  | 9
</code></pre>

</main>
<main is="slide" data-section-name="arrays">

Arrays in C
===========

<pre><code class="lang-clike">
int myArray[10] = {0};
// Put some values into the array.
myArray[0] = 3;
myArray[5] = 17;

</code></pre>

<pre><code class="lang-other">
┌────┬────┬────┬────┬────┬────┬────┬────┬────┬────┐
│  3 │  0 │  0 │  0 │  0 │ 17 │  0 │  0 │  0 │  0 │
└────┴────┴────┴────┴────┴────┴────┴────┴────┴────┘
| 0  | 1  | 2  | 3  | 4  | 5  | 6  | 7  | 8  | 9
</code></pre>

</main>

<main is="slide">

Character Arrays
=========

we can make an array of __chars__ in the same way


<pre><code class="lang-clike">
char myArray[10] = {0};
// Put some values into the array.
myArray[0] = 65;
myArray[5] = 70;

</code></pre>

<pre><code class="lang-other">
┌────┬────┬────┬────┬────┬────┬────┬────┬────┬────┐
│ 65 │  0 │  0 │  0 │  0 │ 70 │  0 │  0 │  0 │  0 │
└────┴────┴────┴────┴────┴────┴────┴────┴────┴────┘
| 0  | 1  | 2  | 3  | 4  | 5  | 6  | 7  | 8  | 9
</code></pre>

</main>

<main is="slide">

How long is a piece of string?
=========

you don't always know the __length__ of a string in advance

e.g. __name__ could be "Andrew", or "Tom"  
<small>(6 characters vs 3 characters)</small>

<pre><code class="lang-clike">
// "Andrew" is 6 letters long
name[0] = 'A';
name[1] = 'n';
name[2] = 'd';
name[3] = 'r';
name[4] = 'e';
name[5] = 'w';

// "Tom" is 3 letters long
name[0] = 'T';
name[1] = 'o';
name[2] = 'm';
</code></pre>

</main>

<main is="slide">

How long is a piece of string?
=========

we need a way to know how long the string is!

<pre><code class="lang-clike">
name[0] = 'A'; name[1] = 'n'; name[2] = 'd';
name[3] = 'r'; name[4] = 'e'; name[5] = 'w';
</code></pre>
<pre><code class="lang-other">
┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐
│ A │ N │ D │ R │ E │ W │   │   │   │   │
└───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘
| 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9
</code></pre>

<small><small><small>
(please __never__ write code on one line like this!
it's only here so the slides fit)
</small></small></small>

</main>


<main is="slide">

How long is a piece of string?
=========

we need a way to know how long the string is!

<pre><code class="lang-clike">
name[0] = 'T'; name[1] = 'o'; name[2] = 'm';
</code></pre>


<pre><code class="lang-clike">
┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐
│'T'│'O'│'M'│ R │ E │ W │   │   │   │   │
└───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘
| 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9
</code></pre>

printing __name__ would print __TOMREW__

</main>


<main is="slide">

Null Terminator
=========

we do this in C using a __null terminator__

any function (e.g. printf) working with a string  
interprets this as "end of string".

<pre><code class="lang-clike">
name[0] = 'T';
name[1] = 'o';
name[2] = 'm';
name[3] = '\0';
</code></pre>

</main>
<main is="slide">

Null Terminator
=========

<pre><code class="lang-clike">
┌───┬───┬───┬────┬───┬───┬───┬───┬───┬───┐
│'T'│'O'│'M'│'\0'│ E │ W │   │   │   │   │
└───┴───┴───┴────┴───┴───┴───┴───┴───┴───┘
| 0 | 1 | 2 |  3 | 4 | 5 | 6 | 7 | 8 | 9
</code></pre>

printing __name__ would now print __TOM__

</main>

</section>

<section name="uninitialised-arrays">

<main is="slide">

Sidenote: Uninitialised Arrays
===============

what happens if we don't initialise our array?

<small>let's try it and see!</small>

</main>


<main is="slide">

Sidenote: Uninitialised Arrays
===============

what's wrong with this code?

<pre><code class="lang-clike">
int array[SIZE];

int i = 0;
while (i < SIZE) {
    printf("%d\n", array[i]);
    i++;
}
</code></pre>

</main>

<main is="slide">

Sidenote: Uninitialised Arrays
===============

the array has not been __initialised__

<pre><code class="lang-clike">
int array[SIZE];

int i = 0;
while (i < SIZE) {
    printf("%d\n", array[i]);
    i++;
}
</code></pre>

<pre><code class="lang-other">
┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐
│ ? │ ? │ ? │ ? │ ? │ ? │ ? │ ? │ ? │ ? │
└───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘
</code></pre>

</main>

<main is="slide">

Sidenote: Uninitialised Arrays
===============

solution: initialise the array first  
<small><small>
(note: you could also initialise all the values in a loop)
</small></small>

<pre><code class="lang-clike">
int array[SIZE] = {0};

int i = 0;
while (i < SIZE) {
    printf("%d\n", array[i]);
    i++;
}
</code></pre>

<pre><code class="lang-other">
┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐
│ 0 │ 0 │ 0 │ 0 │ 0 │ 0 │ 0 │ 0 │ 0 │ 0 │
└───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘
</code></pre>

</main>

<main is="slide">

Sidenote: Uninitialised Arrays
===============

__dcc__ can catch this for you if you tell it to use __valgrind__

<pre><code class="lang-other">
dcc -o blah blah.c --valgrind
</code></pre>
</main>




<!-- more stuff here first -->

<!--
<section name="arrays">
</section>

-->

<!-- end of arrays stuff -->

<main is="slide">
end
</main>

</section>

<!-- code highlighted
<pre><code class="lang-clike">
if (condition 1) {
    if (condition 2) {
        if (condition 3) {
            // Do stuff
        } else if (condition 4) {
            // Do something else
        }
    } else if (condition 5) {
        if (condition 6) {
            // Do something completely different
        }
    } else {
        // In all other cases, do this.
    }
}
</code></pre>
-->


