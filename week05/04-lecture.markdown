---
short-title: COMP1511 18s1 lec04
title: "Lecture 9: More Strings"
...


<style>
pre {
    padding-top: 0em !important;
}
</style>

<main is="slide" class="title">
<h1><big><strong>Welcome!</strong></big></h1>

<h1>COMP1511 18s1<br />
    <small>Programming Fundamentals</small></h1>
</main>

<main is="slide" class="title">
<h1>COMP1511 18s1<br />
  <strong>— Lecture 9 —</strong><br />
  <small>More Strings</small></h1>

<strike>Andrew Bennett  
<small><tt>&lt;andrew.bennett@unsw.edu.au&gt;</tt></small></strike>


Jashank Jeremy  
<small><tt>&lt;jashank.jeremy@unsw.edu.au&gt;</tt></small>


</main>

<main is="slide">

<p>&nbsp;</p>

<big><big><big><big><big>
<strong>
Before we begin...
</strong>
</big></big></big></big></big>

<p>&nbsp;</p>
<big>
__introduce__ yourself to the person sitting next to you  

<p>&nbsp;</p>


__why__ did they decide to study __computing__?
</big>
</main>


<main is="slide">

Overview
=======

<big><strong>after this lecture, you should be able to...</strong></big>

<!--
  - talk about the assignment (and thus asserts / unit testing)
  - talk more about the approach to solving a problem / breaking it down
    into steps etc (motivated by students in my tute struggling with how
to approach danish flag)
  - talk more about functions -- students are confused about passing
    variables into functions (need to reiterate that it passes the
contents of the variable, not the variable itself, so names don't matter
but order does -- had lots of students ask me about that [names vs order
of parameters] after the lecture)
  - play around more with arrays
      - I want to "accidentally" go over the end of an array or go
        backwards in an array or something interesting
      - I'm planning to do something like find the min value / find the
        max value (and hint at "could we print the numbers in sorted
order?" for the bored students)
-->


<!--
understand the basics of __unit testing__ using __asserts__

understand the basics of __designing a solution__ to a problem

have a rough understanding of __Assignment 1__  

understand more about passing __parameters__ into __functions__  

write programs using __arrays__ to solve simple problems
-->

<!--

initialising arrays with {}

initialising strings with = "blah" and = "{'b', 'l', ...}"


fgets


maybe playing with array overflowing? that tute example of writing
beyond the end?

string.h?

2d arrays?

argc/argv

-->

understand how to __initialise__ an __array__

understand the various ways to __initialise__ a __string__

have a basic understanding of functions from __string.h__

understand the basics of working with __command line arguments__  
(i.e. __argc__ and __argv__)


<small><small><small>
(__note__: you shouldn't be able to do all of these immediately after
watching this lecture. however, this lecture should (hopefully!) give
you the foundations you need to develop these skills. remember:
programming is like learning any other language, it takes consistent and
regular practice.)
</small>
</small>
</small>
</main>


<main is="slide">

Admin
=====

<big><big><big><strong>Don't panic!</strong></big></big></big>

__week 4 weekly test__ due friday  
<small>don't be scared!</small>

friday this week is a __public holiday__  
<small>if you have a friday tutelab: see course website for
details</small>

this week's __lab__ due __friday midsem break__  
<small>Friday 6th April</small>

__assignment 1__ due __sunday midsem break__  
<small>you __need__ to start __now__, if you haven't already</small>

don't forget about __help sessions__!  
<small>see course website for details</small>

</main>

<section name="initialisation">
<main is="slide">

Initialising Arrays
====

initialising arrays is __important__  
<small>(remember yesterday?)</small>

</main>

<main is="slide">

Revisiting: Uninitialised Arrays
===============

the array has not been __initialised__

<pre><code class="lang-clike">
int array[SIZE];

int i = 0;
while (i < SIZE) {
    printf("%d\n", array[i]);
    i++;
}
</code></pre>

<pre><code class="lang-other">
┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐
│ ? │ ? │ ? │ ? │ ? │ ? │ ? │ ? │ ? │ ? │
└───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘
</code></pre>

what should __printf__ print?  
this is __undefined behaviour__  
<small>(there's no rule in C about what should happen</small>

</main>

<main is="slide">

Revisiting: Uninitialised Arrays
===============

solution: initialise the array first  
<small><small>
(note: you could also initialise all the values in a loop)
</small></small>

<pre><code class="lang-clike">
int array[SIZE] = {0};

int i = 0;
while (i < SIZE) {
    printf("%d\n", array[i]);
    i++;
}
</code></pre>

<pre><code class="lang-other">
┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐
│ 0 │ 0 │ 0 │ 0 │ 0 │ 0 │ 0 │ 0 │ 0 │ 0 │
└───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘
</code></pre>

</main>


<main is="slide">

Initialising Arrays
===============

there are several ways to initialise an array

using an __array initialiser__

<pre><code class="lang-clike">
// array will be filled with all zeroes
int array[SIZE] = {0};
</code></pre>

<pre><code class="lang-other">
┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐
│ 0 │ 0 │ 0 │ 0 │ 0 │ 0 │ 0 │ 0 │ 0 │ 0 │
└───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘
</code></pre>

</main>


<main is="slide">

Initialising Arrays
===============

there are several ways to initialise an array

using an __array initialiser__

<pre><code class="lang-clike">
// array will be initialised with 1, 2, 3, 4, 5, then the rest 0
int array[SIZE] = {1, 2, 3, 4, 5};
</code></pre>

<pre><code class="lang-other">
┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐
│ 1 │ 2 │ 3 │ 4 │ 5 │ 0 │ 0 │ 0 │ 0 │ 0 │
└───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘
</code></pre>

</main>


<main is="slide">

Initialising Arrays
===============

there are several ways to initialise an array

using a __loop__

<pre><code class="lang-clike">
int i = 0;
while (i < SIZE) {
    array[i] = i;
}
</code></pre>

<pre><code class="lang-other">
┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐
│ 0 │ 1 │ 2 │ 3 │ 4 │ 5 │ 6 │ 7 │ 8 │ 9 │
└───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘
</code></pre>

(this is more __flexible__ and allows you to initialise with values of
your choice)

</main>

<main is="slide">

Initialising Strings
=========

we can initialise strings in a similar way

<pre><code class="lang-clike">
char name[SIZE] = {'A', 'N', 'D', 'R', 'E', 'W'};
</code></pre>
<pre><code class="lang-other">
┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐
│ A │ N │ D │ R │ E │ W │ 0 │ 0 │ 0 │ 0 │
└───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘
| 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9
</code></pre>

(remember: the remaining elements are initialised with zeroes)

</main>


<!--
pretty sure this is wrong, it will initialise
<main is="slide">

Initialising Strings
=========

we can initialise strings in a similar way, __but__

don't forget the __null terminator__!

<pre><code class="lang-clike">
char name[SIZE] = {'A', 'N', 'D', 'R', 'E', 'W', '\0'};
</code></pre>
<pre><code class="lang-other">
┌───┬───┬───┬───┬───┬───┬────┬───┬───┬───┐
│ A │ N │ D │ R │ E │ W │ \0 │ ? │ ? │ ? │
└───┴───┴───┴───┴───┴───┴────┴───┴───┴───┘
| 0 | 1 | 2 | 3 | 4 | 5 |  6 | 7 | 8 | 9
</code></pre>

(does it matter that the end of the string is uninitialised?)

</main>

-->

<main is="slide">

Initialising Strings
=========

there's a short-hand in C

<pre><code class="lang-clike">
char name[SIZE] = "ANDREW";
</code></pre>
<pre><code class="lang-other">
┌───┬───┬───┬───┬───┬───┬────┬───┬───┬───┐
│ A │ N │ D │ R │ E │ W │ \0 │ ? │ ? │ ? │
└───┴───┴───┴───┴───┴───┴────┴───┴───┴───┘
| 0 | 1 | 2 | 3 | 4 | 5 |  6 | 7 | 8 | 9
</code></pre>


</main>


<main is="slide">

Initialising Strings
=========

what happens if we try to set more than will fit?

<pre><code class="lang-clike">
#define SIZE 2
char name[SIZE] = "ANDREW";
</code></pre>
<pre><code class="lang-other">
┌───┬───┐
│ A │ N │ D │ R │ E │ W │ \0 │ ? │ ? │ ? │
└───┴───┘
| 0 | 1 | ? | ? | ? | ? |  ? | ? | ? | ?
</code></pre>

</main>


<main is="slide">

Initialising Strings
=========

if we leave the size out, C will automatically make it big enough

<pre><code class="lang-clike">
char name[] = "ANDREW";
</code></pre>
<pre><code class="lang-other">
┌───┬───┬───┬───┬───┬───┬────┐
│ A │ N │ D │ R │ E │ W │ \0 │
└───┴───┴───┴───┴───┴───┴────┘
| 0 | 1 | 2 | 3 | 4 | 5 |  6
</code></pre>

</main>


<main is="slide">

Initialising Strings
=========

if we leave the size out, C will automatically make it big enough

<pre><code class="lang-clike">
char name[] = {'A', 'N', 'D', 'R', 'E', 'W'};
</code></pre>
<pre><code class="lang-other">
┌───┬───┬───┬───┬───┬───┐
│ A │ N │ D │ R │ E │ W │
└───┴───┴───┴───┴───┴───┘
| 0 | 1 | 2 | 3 | 4 | 5 
</code></pre>

what's the problem here?

<small>try it and see!</small>

</main>


<main is="slide">

Initialising Strings
=========

if we leave the size out, C will automatically make it big enough

<pre><code class="lang-clike">
char name[] = {'A', 'N', 'D', 'R', 'E', 'W', '\0'};
</code></pre>
<pre><code class="lang-other">
┌───┬───┬───┬───┬───┬───┬────┐
│ A │ N │ D │ R │ E │ W │ \0 │
└───┴───┴───┴───┴───┴───┴────┘
| 0 | 1 | 2 | 3 | 4 | 5 |  6
</code></pre>

</main>










</section>

<section name="argc-argv">


<main is="slide" class="title">

<big><h1>introducing:  
<strong>command line arguments</strong></h1></big>

</main>

<main is="slide">

Command Line Arguments
====

"0 or more" strings specified when the program runs

you've already seen these

`dcc -o hello hello.c`

here, __dcc__ is being run with 3 command line arguments:  
`-o`, `hello`, `hello.c`

</main>


<main is="slide">

Command Line Arguments
====

"0 or more" strings specified when the program runs

`./hello`

the program __hello__ has 0 command line arguments

</main>


<main is="slide">

Command Line Arguments
====

"0 or more" strings specified when the program runs

`./hello some thing goes here`

the program __hello__ has 4 command line arguments

</main>


<main is="slide">

Command Line Arguments
====

we can access these in our program by changing the  
__signature__ of the __main__ function

<pre><code class="lang-clike">
int main (int argc, char *argv[]) {
    // code goes here
}
</code></pre>

__argc__ stores the __number__ of arguments

__argv__ stores the __contents__ of the arguments

</main>



<main is="slide">

Command Line Arguments
====

`./program hello there`


<small>this has two arguments: "hello" and "there"</small>

<pre><code class="lang-clike">
int main (int argc, char *argv[]) {
    // argc is 2
    printf("%d\n", argc);

    // print out all of the arguments
    int i = 0;
    while (i < argc) {
        printf("Argument %d is: %s\n", i, argv[i]);
        i++;
    }
}
</code></pre>

__argc__ stores the __number__ of arguments

__argv__ stores the __contents__ of the arguments

</main>


</section>

<section name="fgets">
<main is="slide">

fgets
====

`fgets(array, array size, stream)` reads a line of text

__array__ - char array in which to store the line  
__array size__ - the size of the array  
__stream__ - where to read the line from, e.g. stdin

fgets won't try to store more than __array size__ chars in the array

<big><strong>never</strong></big> use the function `gets`! (why?)

</code></pre>


</main>
</section>



<section name="string.h">

<main is=slide>

string.h
=======

<pre><code class="lang-clike">
#include &lt;string.h&gt;
// string length (not including ’\0’)
int strlen(char *s);
// string copy
char *strcpy(char *dest, char *src);
char *strncpy(char *dest, char *src, int n);
// string concatenation/append
char *strcat(char *dest, char *src);
char *strncat(char *dest, char *src, int n);
</code></pre>
</main>

<main is=slide>

string.h
=======

<pre><code class="lang-clike">
#include &lt;string.h&gt;
// string compare
int strcmp(char *s1, char *s2);
int strncmp(char *s1, char *s2, int n);
int strcasecmp(char *s1, char *s2);
int strncasecmp(char *s1, char *s2, int n);
// character search
char *strchr(char *s, int c);
char *strrchr(char *s, int c);

</code></pre>

</main>

<!--

<section name="section">
<main is="slide">

title
====

contents

</main>
</section>

<section name="end">
<main is="slide">

Feedback?
=========


<strong><big>
<a href="http://bit.do/comp1511-feedback-week4" style="color:#f9b200">
bit.do/comp1511-feedback-week4</a>
</big></strong>

<hr>

<img
src="http://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/week04/comp1511-week4-feedback-qr.png" />

alternate link: <a href="https://andrewb3.typeform.com/to/WrAhfV"
style="color:#f9b200">
https://andrewb3.typeform.com/to/WrAhfV
</a>

-->

<section name="end">
<main is="slide">
end
</main>

</section>

<!-- code highlighted
<pre><code class="lang-clike">
if (condition 1) {
    if (condition 2) {
        if (condition 3) {
            // Do stuff
        } else if (condition 4) {
            // Do something else
        }
    } else if (condition 5) {
        if (condition 6) {
            // Do something completely different
        }
    } else {
        // In all other cases, do this.
    }
}
</code></pre>
-->


