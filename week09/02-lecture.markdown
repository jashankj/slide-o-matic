---
short-title: COMP1511 18s1 lec14
title: "Lecture 14: Pointers+Structs+malloc"
...


<style>
pre {
    padding-top: 0em !important;
}
</style>

<main is="slide" class="title">
<h1><big><strong>Welcome!</strong></big></h1>

<h1>COMP1511 18s1<br />
    <small>Programming Fundamentals</small></h1>
</main>

<main is="slide" class="title">
<h1>COMP1511 18s1<br />
  <strong>— Lecture 14 —</strong><br />
  <small>Pointers + Structs + malloc</small></h1>

Andrew Bennett  
<small><tt>&lt;andrew.bennett@unsw.edu.au&gt;</tt></small>

</main>

<main is="slide">

<p>&nbsp;</p>

<big><big><big><big><big>
<strong>
Before we begin...
</strong>
</big></big></big></big></big>

<p>&nbsp;</p>
<big>
__introduce__ yourself to the person sitting next to you  

<p>&nbsp;</p>

how are they going with __assignment 2__?

</big>

</main>


<!--
<main is="slide">

Feedback
=======

upload __lecture code__  


upload/incorporate __diagrams__  
in lecture recordings

<i>more</i> __diagrams__

go through programs __as a whole__  
before running them

lecture __subtitles__

</main>

-->

<main is="slide">

Overview
=======

<big><strong>after this lecture, you should be able to...</strong></big>

make progress on __assignment 2__

have a better understanding of __pointers__:  
__what__ pointers are  
__how__ to use pointers  
__why__ we use pointers

have a better understanding of __structs__

have a better understanding of __memory__ in C:  
dynamic memory allocation using __malloc__  
the difference between 

<small><small><small>
(__note__: you shouldn't be able to do all of these immediately after
watching this lecture. however, this lecture should (hopefully!) give
you the foundations you need to develop these skills. remember:
programming is like learning any other language, it takes consistent and
regular practice.)
</small>
</small>
</small>
</main>


<main is="slide">

Admin
=====

<big><big><big><strong>Don't panic!</strong></big></big></big>

__assignment 2__  
(if you haven't started yet, start <strong>ASAP</strong>  
<small>deadline extended to <strong>Sunday 13th May</strong></small>

__assignment 1__  
<small>tutor marking/feedback in progress</small>

__week 8 weekly test__ due tomorrow  
<small>don't be scared!</small>

don't forget about __help sessions__!  
<small>see course website for details</small>

</main>

<!--
<section name="more-arrays">

arrays as functions parameters
  constant length array
  array length as parameter
  special terminating elelemnt

uninitialized array elements - show them dcc dashdash valgrind

</section>
-->

<section name="section">

<main is="slide" class="title">

<big><h1>let's talk about <strong>pointers</strong></h1></big>


</main>


<main is="slide">

Pointers?
=====

before we talk about pointers, let's take a step back...

</main>

<main is="slide">

Variables
=====

think all the way back to week 1....

<pre><code class="lang-clike">
int age = 16;
</code></pre>

what does this actually _mean_?

</main>

<main is="slide">

Variables and Functions
=====

<pre><code class="lang-clike">
int main(void) {
    int age = 16;
    int height = 185;
}
</code></pre>

</main>

<main is="slide">

Variables and Functions and Arrays
=====

<pre><code class="lang-clike">
#define SIZE 5

int main(void) {
    int age = 16;
    int array[SIZE];
    foo(array);
}

void foo(int array[SIZE]) {
    int num = 10;
    array[0] = 100;
}

</code></pre>

</main>


<main is="slide">

Variables and Functions and Arrays and Pointers
=====

<pre><code class="lang-clike">
#define SIZE 5

int main(void) {
    int age = 16;
    int array[SIZE];
    foo(array, &age);
}

void foo(int array[SIZE], int *age) {
    int num = 10;
    array[0] = 100;
    *age = 21;
}

</code></pre>

</main>

<!-- do some lecture demos here - actually run the above code, play
around with it, etc -->

<section name="structs">

<main is="slide" class="title">

<big><h1>re-visiting: <strong>structs</strong></h1></big>


</main>


<main is="slide">

Arrays
=====

arrays are a collection of many of the __same type__ of variable

<pre><code class="lang-clike">
int array[10];
</code></pre>

<pre><code class="lang-clike">
// ten boxes that can each hold 1 int
[ ][ ][ ][ ][ ][ ][ ][ ][ ][ ]
</code></pre>


<pre><code class="lang-clike">
// ten boxes that can each hold 1 int
[0][1][2][3][4][5][6][7][8][9]
</code></pre>


</main>

<main is="slide">

Structs
=====

structs are a collection of many of __different types__ of variables

<pre><code class="lang-clike">
struct student {
    int zid;
    char name[MAX_NAME_LEN];
    int ass1_mark;
};
</code></pre>

<pre><code class="lang-clike">
// one box that can hold an int
[5112345]
// MAX_NAME_LEN boxes that can hold a char
[A][n][d][r][e][w][\0][ ][ ]
// one box that can hold an int
[94.5]
</code></pre>

</main>

<main is="slide">

Structs
=====

structs are a collection of many of __different types__ of variables

<pre><code class="lang-clike">
struct student {
    int zid;
    char name[MAX_NAME_LEN];
    int ass1_mark;
};
</code></pre>

<pre><code class="lang-clike">
struct student andrew;
andrew.zid = 5112345;
andrew.ass1_mark = 94.5;
strcpy(andrew.name, "Andrew");
</code></pre>

<pre><code class="lang-clike">
// one box that can hold an int
[5112345]
// MAX_NAME_LEN boxes that can hold a char
[A][n][d][r][e][w][\0][ ][ ]
// one box that can hold an int
[94.5]
</code></pre>
</main>


<main is="slide">

Arrays of Structs?
=====

<pre><code class="lang-clike">
struct student {
    int zid;
    char name[MAX_NAME_LEN];
    int ass1_mark;
};
</code></pre>

<pre><code class="lang-clike">
struct student students[NUM_STUDENTS];
// fill out one student struct in the array of structs
students[0].zid = 5112345;
students[0].ass1_mark = 94.5;
strcpy(students[0].name, "Andrew");

// fill out another student struct in the array of structs
students[1].zid = 9100123;
students[2].ass1_mark = 64.2;
strcpy(students[3].name, "Andrew");
</code></pre>


</main>
</section>

<!-- demos! write a function to scan in a single student, etc -->

<section name="section">

<main is="slide" class="title">

<big><h1>let's play: <strong>Intensity</strong></h1></big>

</main>

<main is="slide">

Intensity
=====

your task: write a program to __play__ the game _Intensity_

the _Intensity_ __referee__ manages the game  
<small>shuffles cards  
deals cards  
asks players for moves  
etc</small>

all input is given over __standard input__  
<small>(i.e. scanf)</small>

all output is given over __standard output__  
<small>(i.e. printf)</small>

</main>



<main is="slide">

Stateless AI
=====

an important concept to understand: your AI is __stateless__

it comes to life for __one__ single move  
reads the input  
thinks about what to do  
prints out its decision

</main>

<main is="slide">

Intensity Referee
=====

the <strong><i>Intensity</i> Referee</strong> runs the game

`1511 intensity_referee`

you can run your AI against it:

`1511 intensity_referee your_ai_code.c`

you can play interactively:

`1511 intensity_referee -i`

<!-- demo referee? -->

</main>


<main is="slide">

Valid Cards To Play
=====

<img src="http://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/intensity.png">

</main>

</section>



<section name="section">

<main is="slide" class="title">

<big><h1>revisiting: <strong>memory</strong></h1></big>

</main>


<main is="slide">

Scope and Lifetimes
=====

the variables inside a function only exist as long as the function does

once your function returns, the variables inside are "gone"

(this is why you can't return an array from a function!)

<!-- demo with arrays returning from functions -->

</main>

<main is="slide">

Lifetimes
=====

what if we need something to "stick around" for longer?

two options:  
make it in a "parent" function  
dynamically allocate memory

</main>

<main is="slide">

Lifetimes
=====

make it in a "parent" function

<pre><code class="lang-clike">
void foo(void) {
    int array[SIZE];
    bar(array);
    printf("%d", array[0]);
}

void bar(int array[SIZE]) {
    array[0] = 123;
}
</code></pre>

</main>


<main is="slide">

Lifetimes
=====

dynamically allocate memory

<pre><code class="lang-clike">
void foo(void) {
    int *array = bar();
    printf("%d", array[0]);
}

int *bar(void) {
    int *array = malloc(SIZE * sizeof(int));
    array[0] = 123;
    return array;
}
</code></pre>


</main>

<main is="slide">

malloc?
=====

what if we need something to "stick around" for longer?

two options:  
make it in a "parent" function  
dynamically allocate memory

</main>


</section>

<section name="end">

<!--
<main is="slide">

Feedback?
=========


<strong><big>
<a href="http://bit.do/comp1511-feedback-week4" style="color:#f9b200">
bit.do/comp1511-feedback-week4</a>
</big></strong>

<hr>

<img
src="http://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/week04/comp1511-week4-feedback-qr.png" />

alternate link: <a href="https://andrewb3.typeform.com/to/WrAhfV"
style="color:#f9b200">
https://andrewb3.typeform.com/to/WrAhfV
</a>
</main>

-->

<main is="slide">
end
</main>

</section>

<!-- code highlighted
<pre><code class="lang-clike">
if (condition 1) {
    if (condition 2) {
        if (condition 3) {
            // Do stuff
        } else if (condition 4) {
            // Do something else
        }
    } else if (condition 5) {
        if (condition 6) {
            // Do something completely different
        }
    } else {
        // In all other cases, do this.
    }
}
</code></pre>
-->


