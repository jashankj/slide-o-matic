---
short-title: COMP1511 18s1 lec14
title: "Lecture 15: malloc + Lists"
...


define(START_CODE, {{<pre><code class="lang-clike">}})
define(END_CODE, {{</code></pre>}})

define(C_START, {{<pre><code class="lang-clike">}})
define(C_END, {{</code></pre>}})


define(CSTART, {{<pre><code class="language-c">}})
define(CEND, {{</code></pre>}})

<style>
pre {
    /*padding-top: 0em !important;*/
}
</style>

<main is="slide" class="title">
<h1><big><strong>Welcome!</strong></big></h1>

<h1>COMP1511 18s1<br />
    <small>Programming Fundamentals</small></h1>
</main>

<main is="slide" class="title">
<h1>COMP1511 18s1<br />
  <strong>— Lecture 15 —</strong><br />
  <small>malloc + Lists</small></h1>

Andrew Bennett  
<small><tt>&lt;andrew.bennett@unsw.edu.au&gt;</tt></small>

</main>

<!--
<main is="slide">

<p>&nbsp;</p>

<big><big><big><big><big>
<strong>
Before we begin...
</strong>
</big></big></big></big></big>

<p>&nbsp;</p>
<big>
__introduce__ yourself to the person sitting next to you  

<p>&nbsp;</p>

how are they going with __assignment 2__?

</big>

</main>
-->
<!--

Intending cover slowly creation of list nodes, linking them together,
traversing list, 
computing sum & length of list

-->

<!--
<main is="slide">

Feedback
=======

upload __lecture code__  


upload/incorporate __diagrams__  
in lecture recordings

<i>more</i> __diagrams__

go through programs __as a whole__  
before running them

lecture __subtitles__

</main>

-->

<main is="slide">

Overview
=======

<big><strong>after this lecture, you should be able to...</strong></big>

have a better understanding of __malloc__

be able to reason about dynamic memory management

have a basic understanding of __lists__

understand the fundamentals of __self-referential__ and __linked__ data
structures




<small><small><small>
(__note__: you shouldn't be able to do all of these immediately after
watching this lecture. however, this lecture should (hopefully!) give
you the foundations you need to develop these skills. remember:
programming is like learning any other language, it takes consistent and
regular practice.)
</small>
</small>
</small>
</main>


<main is="slide">

Admin
=====

<big><big><big><strong>Don't panic!</strong></big></big></big>

__assignment 2__  
(if you haven't started yet, start <strong>ASAP</strong>)  
<small>deadline extended to <strong>Sunday 13th May</strong></small>

__assignment 1__  
<small>tutor marking/feedback in progress</small>

__week 8 weekly test__ due tonight  
<small>don't be scared!</small>

don't forget about __help sessions__!  
<small>see course website for details</small>

</main>


<section name="malloc">
<main is="slide">

malloc
====

allocates memory in "the heap"

memory "lives" _forever_ until we __free__ it  
<small>(or the program ends)</small>

__syntax__:

CSTART
malloc(number of bytes to allocate);
CEND


returns a __pointer__ to the block of allocated memory  
<small>(i.e. the __address__ of the memory, so we know how to find it!)</small>

</main>

<main is="slide">

malloc -- how many bytes?
====

CSTART
malloc(number of bytes to allocate);
CEND

if we want __1000__ ints, how many __bytes__ is that?

1000?

how big is an int?

</main>

<main is="slide">

sizeof
====

we can find out the size of a type with __sizeof__

syntax:
CSTART
sizeof(type);
// e.g. for an int:
sizeof(int);
CEND

<pre><code class="lang-clike">
printf("%ld", sizeof (char));    // 1
printf("%ld", sizeof (int));     // 4 commonly
printf("%ld", sizeof (double));  // 8 commonly
printf("%ld", sizeof (int[10])); // 40 commonly
printf("%ld", sizeof (int *));   // 4 or 8 commonly
printf("%ld", sizeof "hello");   // 6
</code></pre>
</main>


<main is="slide">

malloc: syntax
====

CSTART
// allocating enough memory for 1000 ints
malloc(1000 * sizeof(int));
CEND

remember, malloc returns the __address__ of the memory it's allocated:

CSTART
// allocating enough memory for 1000 ints
int *p = malloc(1000 * sizeof(int));
CEND

</main>

<main is="slide">

malloc -- when things go wrong
========

what happens if the allocation __fails__?

CSTART
int *p = malloc(1000 * sizeof(int));
if (p == NULL) {
    fprintf(stderr, "Error: couldn't allocate memory!\n");
    exit(1);
}
CEND

</main>



<main is="slide">

free
====

when we're done with the memory, we need to __free__ it

CSTART
void free (void *obj);
CEND

release memory associated with a reference.  
must be the same reference we got when allocating!

</main>


<main is="slide">

Newton's 3rd Law of Memory Management
========

_"For every malloc, there is an equal and opposite free."_

__why?__

memory is a _finite_ resource.

leaking memory is bad practice,  
especially in long-lived programs. 

(see, e.g., Chrome)

</main>


<main is="slide">

Putting it together
=========


<pre><code class="lang-clike">
int *p;
p = malloc(100000000 * sizeof (int));
if (p == NULL) {
    printf("Error: array could not be allocated.\n");
    exit(1);
}

// we can now use the pointer
// ... lots of things to do

// free up the memory that was used
free(p);
</code></pre>
</main>


<section name="lists">

<main is="slide" class="title">

<big><h1>let's look at something <strong>new</strong>...</h1></big>

</main>


<main is="slide">

Remember Arrays?
====

arrays: a _contiguous_ block of memory  
(a continuous series of boxes in memory)

each item has a known location --  
it's right after the previous item

CSTART
int array[7] = { 3, 1, 4, 1, 5, 9, 2 };
CEND

gives us a sequence of elements in memory:

CSTART
┄─┬────┬────┬────┬────┬────┬────┬────┬─┄
  │  3 │  1 │  4 │  1 │  5 │  9 │  2 │  
┄─┴────┴────┴────┴────┴────┴────┴────┴─┄
CEND

</main>

<main is="slide">

Varying the Length
====

what if we don't know how big our array needs to be in advance?

what if we need to __increase__ the length?

in C, we have dynamic allocation...  
but that's still (effectively) fixed in length

we need a new way to represent  
_collections_ of things

</main>

<main is="slide">

Discontinuity
=============

We could have several pointers to separate allocations:

CSTART
int *a = malloc (1 * sizeof (int));
*a = 3;
int *b = malloc (1 * sizeof (int));
*b = 1;
int *c = malloc (1 * sizeof (int));
*c = 4;
// ... and so on
CEND

we'd have to hold on to all those pointers...  
and we don't have a nice way to do that.

CSTART
┄┬────┬┄    ┄┬────┬┄    ┄┬────┬┄    ┄┬────┬┄
 │  3 │      │  9 │      │  5 │      │  2 │ 
┄┴────┴┄    ┄┴────┴┄    ┄┴────┴┄    ┄┴────┴┄
      ┄┬────┬┄    ┄┬────┬┄    ┄┬────┬┄      
       │  1 │      │  4 │      │  1 │       
      ┄┴────┴┄    ┄┴────┴┄    ┄┴────┴┄      
CEND

</main>

<main is="slide" class="title">

<big>
... what if each item knows  
where the __next__ one is?
</big>

</main>

<main is="slide" class="title">

<big><h1>introducing: <strong>lists</strong></h1></big>

</main>


<main is="slide">

Continual Discontinuity
=======================

<pre class="text-center">
 ╭───╮   ╭───╮   ╭───╮   ╭───╮ 
 │ 3 ├─╮ │ 5 ├───│ 9 ├───│ 2 ├╳
 ╰───╯ │ ╰───╯   ╰───╯   ╰───╯ 
       │   ╰───────────────╮   
     ╭───╮   ╭───╮   ╭───╮ │   
     │ 1 ├───│ 4 ├───│ 1 ├─╯   
     ╰───╯   ╰───╯   ╰───╯     
</pre>

</main>
<main is="slide">

Continual Discontinuity
=======================

a sequence of __nodes__,  
each with a __value__ and a __next__

<pre class="text-center">
╭───╮  
│ 3 ├─*
╰───╯  
</pre>

</main>
<main is="slide">

Continual Discontinuity
=======================

CSTART
struct node {
    struct node *next;
    int          data;
};
CEND

</main>
<main is="slide">

One Fish...
===========

<pre class="text-center">
╭───╮           
│ 3 ├─*         
╰───╯           
  a             
</pre>

CSTART
struct node a = { 3 };





CEND

</main>
<main is="slide">

Two Fish...
===========

<pre class="text-center">
╭───╮    ╭───╮  
│ 3 ├─*  │ 1 ├─*
╰───╯    ╰───╯  
  a        b    
</pre>

CSTART
struct node a = { 3 };
struct node b = { 1 };



CEND

</main>
<main is="slide">

Red Fish...
===========

<pre class="text-center">
╭───╮    ╭───╮  
│ 3 ├────│ 1 ├─*
╰───╯    ╰───╯  
  a        b    
</pre>

CSTART
struct node a = { 3 };
struct node b = { 1 };

a.next = &b;


CEND

</main>
<main is="slide">

Blue Fish!
==========

<pre class="text-center">
╭───╮    ╭───╮  
│ 3 ├────│ 1 ├─╳
╰───╯    ╰───╯  
  a        b    
</pre>

CSTART
struct node a = { 3 };
struct node b = { 1 };

a.next = &b;
b.next = NULL;

CEND

</main>
<main is="slide">

... pointers?
=============

<pre class="text-center">
╭───╮    ╭───╮  
│ 3 ├────│ 1 ├─╳
╰───╯    ╰───╯  
  a        b    
</pre>

CSTART
struct node a = { 3 };
struct node b = { 1 };

a.next = &b;
*(a.next).next = NULL;

CEND

</main>
<main is="slide">

... pointers!
=============

<pre class="text-center">
╭───╮    ╭───╮    ╭───╮  
│ 3 ├────│ 1 ├────│ 4 ├─╳
╰───╯    ╰───╯    ╰───╯  
  a        b        c    
</pre>

CSTART
struct node a = { 3 };
struct node b = { 1 };
struct node c = { 4 };
a.next = &b;
a.next->next = &c;
a.next->next->next = &d;
CEND

</main>

<!--
<section name="section">
<main is="slide">

title
====

contents

</main>

</section>
-->



<section name="end">

<!--
<main is="slide">

Feedback?
=========


<strong><big>
<a href="http://bit.do/comp1511-feedback-week4" style="color:#f9b200">
bit.do/comp1511-feedback-week4</a>
</big></strong>

<hr>

<img
src="http://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/week04/comp1511-week4-feedback-qr.png" />

alternate link: <a href="https://andrewb3.typeform.com/to/WrAhfV"
style="color:#f9b200">
https://andrewb3.typeform.com/to/WrAhfV
</a>
</main>

-->

<main is="slide">
end
</main>

</section>

<!-- code highlighted
<pre><code class="lang-clike">
if (condition 1) {
    if (condition 2) {
        if (condition 3) {
            // Do stuff
        } else if (condition 4) {
            // Do something else
        }
    } else if (condition 5) {
        if (condition 6) {
            // Do something completely different
        }
    } else {
        // In all other cases, do this.
    }
}
</code></pre>
-->




