---
short-title: COMP1511 18s1 lec10
title: "Lecture 10: Strings+Arrays+Functions"
...


<style>
pre {
    padding-top: 0em !important;
}
</style>

<main is="slide" class="title">
<h1><big><strong>Welcome!</strong></big></h1>

<h1>COMP1511 18s1<br />
    <small>Programming Fundamentals</small></h1>
</main>

<main is="slide" class="title">
<h1>COMP1511 18s1<br />
  <strong>— Lecture 10 —</strong><br />
  <small>Strings+Arrarys+Functions</small></h1>


<strike>Andrew Bennett  
<small><tt>&lt;andrew.bennett@unsw.edu.au&gt;</tt></small></strike>


Jashank Jeremy  
<small><tt>&lt;jashank.jeremy@unsw.edu.au&gt;</tt></small>

</main>

<main is="slide">

<p>&nbsp;</p>

<big><big><big><big><big>
<strong>
Before we begin...
</strong>
</big></big></big></big></big>

<p>&nbsp;</p>
<big>
__introduce__ yourself to the person sitting next to you  

<p>&nbsp;</p>


__why__ did they decide to study __computing__?
</big>
</main>


<!--
<main is="slide">

Feedback
=======

upload __lecture code__  


upload/incorporate __diagrams__  
in lecture recordings

<i>more</i> __diagrams__

go through programs __as a whole__  
before running them

lecture __subtitles__

</main>

-->

<main is="slide">

Overview
=======

<big><strong>after this lecture, you should be able to...</strong></big>

understand the basics of working with __getchar__, __putchar__,
__fgets__

write programs using __strings__ to solve simple problems

have a deeper understanding about __arrays__

have a deeper understanding about __calling functions__ and __function
parameters__

have a deeper understanding about __passing values__ into functions


<small><small><small>
(__note__: you shouldn't be able to do all of these immediately after
watching this lecture. however, this lecture should (hopefully!) give
you the foundations you need to develop these skills. remember:
programming is like learning any other language, it takes consistent and
regular practice.)
</small>
</small>
</small>
</main>


<main is="slide">

Admin
=====

<big><big><strong>Don't panic!</strong></big></big>


__assignment 1__ due <big><strong>TONIGHT</strong></big>  
<small>you can do it!</small>

__week 5 weekly test__ due thursday  
<small>don't be scared!</small>

__lab marks__ released  
<small>post in class forum || email your tutor</small>

don't forget about __help sessions__!  
<small>see course website for details</small>

</main>

<!--
<section name="more-arrays">

arrays as functions parameters
  constant length array
  array length as parameter
  special terminating elelemnt

uninitialized array elements - show them dcc dashdash valgrind

</section>
-->


<section name="more strings">

<main is="slide">

remember strings?
=====

a __string__ is an __array__ of __characters__

<pre><code class="lang-clike">
char name[] = "ANDREW";
</code></pre>
<pre><code class="lang-other">
┌───┬───┬───┬───┬───┬───┬────┐
│ A │ N │ D │ R │ E │ W │ \0 │
└───┴───┴───┴───┴───┴───┴────┘
| 0 | 1 | 2 | 3 | 4 | 5 |  6
</code></pre>

</main>


<main is="slide">

remember strings?
=====

characters store __ASCII__ values

<pre><code class="lang-clike">
char name[] = "ANDREW";
</code></pre>

<pre><code class="lang-other">
┌───┬───┬───┬───┬───┬───┬────┐
│ A │ N │ D │ R │ E │ W │ \0 │
└───┴───┴───┴───┴───┴───┴────┘
| 0 | 1 | 2 | 3 | 4 | 5 |  6
</code></pre>

is equivalent to

<pre><code class="lang-other">
┌───┬───┬───┬───┬───┬───┬────┐
│65 │78 │68 │83 │69 │87 │ 0  │
└───┴───┴───┴───┴───┴───┴────┘
| 0 | 1 | 2 | 3 | 4 | 5 |  6
</code></pre>


</main>

<main is="slide">

remember strings?
=====

never use the ASCII values directly

<pre><code class="lang-clike">

char name[] = "ANDREW";

// Prints out A
printf("name[0] as a char is: %c\n");

// Prints out 65
printf("name[0] as an int is: %d\n");

</code></pre>


</main>

<main is="slide">

remember strings?
=====

__never__ use the ASCII values directly

<pre><code class="lang-clike">
int some_letter = 'A';
int another_letter = 65;

assert(some_letter == another_letter);
</code></pre>

we can acccess the ASCII value for the letter A with 'A'.

much better to use 'A' than 65 -- __why__?

</main>


<main is="slide">

letters are just ASCII values are just letters
=====

ASCII values are __sequential__

<pre><code class="lang-clike">
printf("the ascii value for %c is: %d\n", 'A', 'A');
printf("the ascii value for %c is: %d\n", 'B', 'B');
printf("the ascii value for %c is: %d\n", 'C', 'C');
</code></pre>

</main>

<main is="slide">

letters are just ASCII values are just letters
=====

this means we can do cool things

<pre><code class="lang-clike">
// what will something be?
int something = 'B' - 'A';
</code></pre>

</main>


<main is="slide">

getchar and putchar
=====

`getchar()`

reads a character from standard input  
returns an __int__  

`putchar('A')`

prints a character to standard output

<small>let's try it!</small>
</main>


<main is="slide">

using getchar and putchar in a loop
=====

<pre><code class="lang-clike">
while (c != EOF) {
    printf("%c", c);
    c = getchar();
}
</code></pre>
</main>

<main is="slide">

using getchar and putchar in a loop
=====

<pre><code class="lang-clike">
int c = ????
while (c != EOF) {
    printf("%c", c);
    c = getchar();
}
</code></pre>
</main>


<main is="slide">

using getchar and putchar in a loop
=====

<pre><code class="lang-clike">
int c = getchar();
while (c != EOF) {
    printf("%c", c);
    c = getchar();
}
</code></pre>
</main>

</section>

<!--
getchar/putchar/fgets
strings
arrays
function parameters, value passing
-->


<section name="strings">

<main is="slide" class="title">

<big><h1>up next: <strong>beyond getchar()</strong></h1></big>

</main>



<main is="slide">

More input
=====

what if we wanted to scan more than one character at a time?

</main>

<main is="slide">

More input
=====

(re-)introducing: __fgets__

</main>
<main is="slide">

More input
====

`fgets(array, array size, stream)` reads a line of text

__array__ - char array in which to store the line  
__array size__ - the size of the array  
__stream__ - where to read the line from, e.g. stdin

fgets won't try to store more than __array size__ chars in the array

<small>let's try it out!</small>
</main>

<!--
prompt user for input; fgets
what's your name
hello %s


format string
-->

<main is="slide">

fgets vs gets
====

<big><strong>never</strong></big> use the function `gets`! (why?)

<small><small>man pages + demo</small></small>


</main>

</section>


<section name="arrays">

<main is="slide" class="title">

<big><h1>up next: <strong>where is everything?</strong></h1></big>

</main>



<main is="slide">

Arrays
=====

what __are__ arrays?

</main>

<main is="slide">

Arrays in memory
=====

how are they __stored__ in memory?

</main>


<main is="slide">

Arrays in memory
=====

what __else__ is stored in memory?

<small>hint: everything!</small>
</main>

<main is="slide">

Everything in memory
=====

why does this matter?

</main>

<main is="slide">

Function memory
=====

variables in a function can only be accessed by that function.

__why__?

</main>



</section>



<section name="functions">

<main is="slide" class="title">

<big><h1>up next: <strong>calling functions</strong></h1></big>

</main>


<main is="slide">

Passing values into functions
=====

functions receive a __copy__ of the __value__ of the function parameter

</main>


<main is="slide">

Passing arrays into functions?
=====

if functions can't modify anything outside of their function  
how do arrays work?


<small><small><a
href="https://cgi.cse.unsw.edu.au/~cs1511/lab/05/farnarkle.c">farnarkle.c</a></small></small>
</main>


<main is="slide">

Farnarkles
=====


<pre><code class="lang-clike">
int hidden_tiles[N_TILES];
printf("Enter hidden tiles: ");
read_tiles(hidden_tiles);
print_tiles(hidden_tiles);
test_farnarkle_ai(hidden_tiles)
</code></pre>



<small><small><a
href="https://cgi.cse.unsw.edu.au/~cs1511/lab/05/farnarkle.c">farnarkle.c</a></small></small>
</main>


</section>
















<section name="end">

<!--
<main is="slide">

Feedback?
=========


<strong><big>
<a href="http://bit.do/comp1511-feedback-week4" style="color:#f9b200">
bit.do/comp1511-feedback-week4</a>
</big></strong>

<hr>

<img
src="http://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/week04/comp1511-week4-feedback-qr.png" />

alternate link: <a href="https://andrewb3.typeform.com/to/WrAhfV"
style="color:#f9b200">
https://andrewb3.typeform.com/to/WrAhfV
</a>
</main>

-->

<main is="slide">
end
</main>

</section>

<!-- code highlighted
<pre><code class="lang-clike">
if (condition 1) {
    if (condition 2) {
        if (condition 3) {
            // Do stuff
        } else if (condition 4) {
            // Do something else
        }
    } else if (condition 5) {
        if (condition 6) {
            // Do something completely different
        }
    } else {
        // In all other cases, do this.
    }
}
</code></pre>
-->


