---
short-title: COMP1511 18s1 lec04
title: "Lecture 11: Files, Memory"
...


<style>
pre {
    padding-top: 0em !important;
}
</style>

<main is="slide" class="title">
<h1><big><strong>Welcome!</strong></big></h1>

<h1>COMP1511 18s1<br />
    <small>Programming Fundamentals</small></h1>
</main>

<main is="slide" class="title">
<h1>COMP1511 18s1<br />
  <strong>— Lecture 11 —</strong><br />
  <small>Files, Memory</small></h1>

Andrew Bennett  
<small><tt>&lt;andrew.bennett@unsw.edu.au&gt;</tt></small>

</main>


<!--
<main is="slide">

Feedback
=======

upload __lecture code__  


upload/incorporate __diagrams__  
in lecture recordings

<i>more</i> __diagrams__

go through programs __as a whole__  
before running them

lecture __subtitles__

</main>

-->

<main is="slide">

Overview
=======

<big><strong>after this lecture, you should be able to...</strong></big>

have a basic understanding of the different types of i/o in C

read and write files using C i/o functions and file redirection

feel more comfortable working with argc/argv

have a basic understanding of memory in C

<!--

more on i/o

reading/writing files

file redirection

argc/argv

memory

pointers

-->

<small><small><small>
(__note__: you shouldn't be able to do all of these immediately after
watching this lecture. however, this lecture should (hopefully!) give
you the foundations you need to develop these skills. remember:
programming is like learning any other language, it takes consistent and
regular practice.)
</small>
</small>
</small>
</main>


<main is="slide">

Admin
=====

<big><big><strong>Don't panic!</strong></big></big>


__assignment 1__ due <big><strong>YESTERDAY</strong></big>  
<small>nice work :)</small>

__week 5 weekly test__ due thursday  
<small>don't be scared!</small>

__lab marks__ released  
<small>post in class forum || email your tutor</small>

don't forget about __help sessions__!  
<small>see course website for details</small>

</main>

<!--
<section name="more-arrays">

arrays as functions parameters
  constant length array
  array length as parameter
  special terminating elelemnt

uninitialized array elements - show them dcc dashdash valgrind

</section>
-->


<section name="io">

<main is="slide" class="title">

<big><h1>introducing: <strong>i/o</strong></h1></big>

(<strong>i</strong>nput/<strong>o</strong>utput)


</main>



<main is="slide">

input/output?
=====

__i/o__  
(<strong>i</strong>nput/<strong>o</strong>utput)

various ways our programs can take input and give output


</main>


<main is="slide">

input/output?
=====

you've already seen several ways

printf, scanf, getchar, putchar, fgets, ....

</main>

<main is="slide">

input/output?
=====

these all work with __stdin__ and __stdout__  
<small>(standard input and standard output)</small>

__stdin__  
scanf, getchar, fgets

__stdout__  
printf, putchar

</main>

<main is="slide">

Other i/o
=====

__stdin__ and __stdout__ go to the terminal

but there are other options

__stderr__  
(standard error)

__files__  
(e.g. "input.txt")

</main>

<main is="slide">

stderr
=====

still goes to the terminal

__semantically__ different to __stdout__

used to print __errors__

can be redirected separately to __stdout__

</main>

<main is="slide">

stderr
=====

can access with __fprintf__

<pre><code class="lang-clike">
fprintf(stderr, "Uh oh, something went wrong!\n");
</code></pre>

</main>


<main is="slide">

fprintf
=====

just like printf, but works with __files__

<pre><code class="lang-clike">
// stdout is a "file descriptor"
fprintf(stdout, "Hello, world!\n");

// stderr is also a "file descriptor"
fprintf(stderr, "Uh oh, something went wrong!\n");

// can also work with real files
FILE\* output_file = fopen("output.txt", "w");
fprintf(output_file, "Hello!\n");
</code></pre>

</main>
</section>

<section name="files">


<main is="slide">

Files in C
=====

we can work with files in C

__printing__ content to a file (just like printf)

__scanning__ content from a file (just like scanf)

</main>

<main is="slide">

Files in C
=====

we saw the syntax earlier:

<pre><code class="lang-clike">
// opens a file called  "output.txt" in "writing" mode
FILE\* output_file = fopen("output.txt", "w");

// prints "Hello!" to the file
fprintf(output_file, "Hello!\n");
</code></pre>


</main>


<main is="slide">

file redirection
=====

we can also get the terminal to handle the file input/output for us


<pre><code class="lang-clike">
// in a file "blah.c"
printf("Hello from a normal printf\n");
</code></pre>

<pre><code class="lang-bash">
dcc -o blah blah.c
</code></pre>

<pre><code class="lang-bash">
./blah > output.txt
</code></pre>

<pre><code class="lang-bash">
$ cat output.txt
Hello from a normal printf
</code></pre>

</main>



<main is="slide">

string.h
=====

<big><strong>#include &lt;string.h&gt;</strong></big>

contains lots of functions to work with strings

`man 3 string`

---

note: many of these you can (and should, for practice) write yourself

e.g.: `int strlen(char *string)` -- takes a string, returns the length.  
how would you write it yourself?
</main>

<main is="slide">

well...
=====

let's try it out!

</main>

</section>

<section name="break">
<main is="slide">

<a class="orange" href="https://www.youtube.com/watch?v=k4kKVYxxliY">
https://www.youtube.com/watch?v=k4kKVYxxliY
</a>

</main>
</section>

<section name="memory">


<main is="slide" class="title">

<big><h1>up next: <strong>memory</strong></h1></big>

</main>



<main is="slide">

Memory
=====

effectively a GIANT array  
<small>(4 GB big)</small>

everything is stored in memory _somewhere_

variables are stored in memory

the code for your program is stored in memory

the code for library functions you use is stored in memory  
<small>(printf, scanf, ...)</small>

since everything is stored in memory, it has an __address__  
<small>(similar to your home address)</small>

</main>

<main is="slide">

Memory Addresses
=====

since everything is stored _somewhere_ in memory, everything has an __address__.

we can get the address with <big><strong>&</strong></big> ("address of")

we print memory addresses in __hexidecimal__  
<small>(why?)</small>

</main>

<main is="slide">

Memory Layout
=====

different things are stored in different places

variables are stored on __the stack__

dynamic memory (more on that next week) is stored on __the heap__


<pre><code class="lang-other">
┌────────────────┐
│   libraries &  │              0x00000000
│      magic     │
├────────────────┤
│  your program  │             ~0x00600000
├────────────────┤
│                │             ~0x10000000
│    the HEAP    │
┆                ┆


┆                ┆
│                │
│   the STACK    │
│                │              0xBFFFFFFF
├────────────────┤
│   more magic   │              0xFFFFFFFF
└────────────────┘

</code></pre>


<img src="https://www.cse.unsw.edu.au/~andrewb/slides/week06/memory.svg" />

<p>&nbsp;</p>

<small><small>
note: some people draw memory "upside down"  
(0xFFFFFFFF at the top)  
keep an eye out for which way up any given diagram is
</small>
</small>
</main>

</section>

<section name="references">

<main is="slide" class="title">

<big><h1>up next: <strong>references</strong></h1></big>

</main>



<main is="slide">

address of
====

from earlier:

we can get the address with <big><strong>&</strong></big> ("address of")

<pre><code class="lang-clike">
int number = 10;
printf("The address of number is: %p\n", &number);
</code></pre>

we sometimes call this a "reference"  
i.e. "&number" is a __reference__ to the variable "number"

</main>

<main is="slide">

address of
====

when we have the __address__ of something, we know how to get to it

<pre><code class="lang-clike">
int number = 10;
printf("The address of number is: %p\n", &number);

// might print something like:
// The address of number is: 0xffd53f50
</code></pre>

</main>

<main is="slide">

going to the address of
====

when we have the __address__ of something, we know how to get to it

we use the `*` operator for this: __dereference__

<small>
("de-reference" -- go to the reference, to get the value there)
</small>

<pre><code class="lang-clike">
int number = 10;
printf("The address of number is: %p\n", &number);

// might print something like:
// The address of number is: 0xffd53f50

// we can print out the value at that address:
printf("The value at address %p is %d\n",
        0xffd53f50, *0xffd53f50);

// should print out "10"
</code></pre>

</main>


<main is="slide">

going to the address of
====

we could do this directly in the printf:

<pre><code class="lang-clike">
int number = 10;
printf("The address of number is: %p\n", &number);
// might print something like:
// The address of number is: 0xffd53f50

printf("The value at address %p is %d\n",
    &number,  *(&number));

// should print out something like
// The value at address 0xffd53f50 is 10




// we can print out the value at that address:
printf("The value at address %p is %d\n",
        0xffd53f50, *0xffd53f50);

// should print out something like
// The value at address 0xffd53f50 is 10
</code></pre>

</main>

<main is="slide">

storing the address of
====

"but Andrew, why would we do that when we could just print out number
directly?"

exactly.

---

addresses are most useful for telling things far away (i.e. __other__
functions)  
about the address of something in __your__ function.

we have a special type for storing the addresses of values

<pre><code class="lang-clike">
int number = 10;
int *number_address = &number;

printf("The address of number is: %p\n", &number);

printf("The value at address %p is %d\n",
    &number,  *(&number));

printf("The value at address %p is %d\n",
    number_address  *number_address);
</code></pre>

we call these __pointers__ because they "point" to the variable whose
address they store

</main>

<main is="slide">

storing the address of
====

pointer syntax:

<pre><code class="lang-clike">
[type] *[some_name] = &[something];
</code></pre>

e.g. 

<pre><code class="lang-clike">
int *my_pointer = &my_variable;
</code></pre>

importantly: the __value__ of the pointer is the __address__ of the
variable it points to

</main>

<main is="slide">

storing the address of
====

confused?

that's okay.

let's try it out!

</main>



</section>
<section name="end">

<!--
<pre><code class="lang-clike">
<big><pre>
┌────────────────┐
│ DANGER! MAGIC! │             0x00000000
├────────────────┤
│ libraries &    │             0x00200000
│    system code │
├────────────────┤
│  your program  │             0x00400000
├────────────────┤
┆                ┆


┆                ┆
│ the CALL STACK │             0xFFBFFFFF
├────────────────┤
│ DANGER! MAGIC! │             0xFFFFFFFF
└────────────────┘
</pre></big>
</code></pre>


-->

<!--
<main is="slide">

Feedback?
=========


<strong><big>
<a href="http://bit.do/comp1511-feedback-week4" style="color:#f9b200">
bit.do/comp1511-feedback-week4</a>
</big></strong>

<hr>

<img
src="http://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/week04/comp1511-week4-feedback-qr.png" />

alternate link: <a href="https://andrewb3.typeform.com/to/WrAhfV"
style="color:#f9b200">
https://andrewb3.typeform.com/to/WrAhfV
</a>
</main>

-->

<main is="slide">
end
</main>

</section>

<!-- code highlighted
<pre><code class="lang-clike">
if (condition 1) {
    if (condition 2) {
        if (condition 3) {
            // Do stuff
        } else if (condition 4) {
            // Do something else
        }
    } else if (condition 5) {
        if (condition 6) {
            // Do something completely different
        }
    } else {
        // In all other cases, do this.
    }
}
</code></pre>
-->


