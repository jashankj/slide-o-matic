---
short-title: COMP1511 18s1 lec01
title: "Lecture 0: Hello, World!"
...


<main is="slide" class="title">
<h1><big><strong>Welcome!</strong></big></h1>

<h1>COMP1511 18s1<br />
    <small>Programming Fundamentals</small></h1>
</main>

<section name="the beginning">

<main is="slide" class="title">
<h2><strong>— Lecture 0 —</strong><br />
    Hello, World!</h1>

Andrew Bennett  
<small><tt>&lt;andrew.bennett@unsw.edu.au&gt;</tt></small>

course introduction  
the big picture  
many sights to C

</main>

</section>


<section name="big picture">
<main is="slide" data-section-name="big-picture" class="title">

<h1><big><strong>the bigger picture...</strong></big></h1>

</main>
<main is="slide" data-section-name="big-picture">

what is programming?
====================

</main>
<main is="slide" data-section-name="big-picture">

doesn't have to be in a language like C...
==========================================

https://www.youtube.com/watch?v=FN2RM-CHkuI

<!-- may seem like a silly video, but it's a very good parallel to
programming.

if you're not specific enough, things can go wrong. computers can't
understand english. -->

</main>
<main is="slide" data-section-name="big-picture">

why is programming awesome?
===========================

<!-- ask the audience.

"how many of you have programmed before?" do you think that programming
is awesome? why?

some ideas from last sem:
  - magic
  - can build cool things
  - save time using algorithms/code: computers can do dumb things very
    quickly.
  - teaching a computer to do things for you
  - can do amazing and powerful things: go to space, play games, make
    movies
  - cost of progamming low, cheap equipment.

my ideas from last sem:
  - make amazing beautiful thngs
  - in civeng, planning and designing a bridge may be fairly
    straightforward, but actually building that bridge is really hard.
    in programming, the effort required to make your thing is much less.
  - solve problems and help people
    - how many of you have used google maps?
-->

</main>
</section>

<section name="intro">
<main is="slide" data-section-name="intro">

What?
=====

<big>
this is a course where you will...

__learn to program__

__become a confident programmer__

__write code you're proud of__

__discover the joys of programming__
</big>

</main>

<main is="slide" data-section-name="intro">

It can be tricky at first
========================

<big>
code won't compile

not sure what's going on  

easy to lose track of what you're trying to do


__is "it works" good enough?__

</big>
</main>


<main is="slide" data-section-name="intro">

Who's teaching?
===============

3x lecturers:  
__Mr Andrew Bennett__  
(that's me!)

__Dr Andrew Taylor__  

__Dr John 'jas' Shepherd__  

course convenor:  
__Dr Andrew Taylor__

course administrator:  
__Mei Cheng Whale__  

tutors + lab assistants:  
__too many to list!__

<!--
__Zain Afzal__, 
__Alex Hinds__, 
__Jashank Jeremy__, 
__Helena Kertesz__,  
__Stephen Leung__, 
__Alex Linker__, 
__Curtis Millar__,  
__Riyasat Saber__, 
__Mark Sonnenschein__, 
__Victoria Xu__  
are your tutors...

they'll be working alongside our cast of assistant tutors  
<small>
__Gal Aharon__, 
__George Fidler__, 
__Colm Flanagan__, 
__Nicholas Hiebl__, 
__Peter Kerr__, 
__Carey Li__, 
__James O'Brien__, 
__Connor O'Shea__, 
__Zac Partridge__, 
__Ben Pieters-Hawke__, 
__Keiran Sampson__, 
__Jack She__, 
__Alvin Sho__, 
__Jared Steiner__, 
__Adam Stucci__, 
__Ian Thorvaldson__, 
__Cadel Watson__, 
__Dean Wunder__, 
__Anna Zhang__
</small>
-->

</main>
<main is="slide" data-section-name="intro">

Who's learning?
===============

<p>...</p>

<strong><big>You!</big></strong>

</main>

<main is="slide" data-section-name="intro">

Three types of students
===============

<big>
red

yellow

green
</big>

<!-- red: programming forever, dream in assembly, very confident
challenge in this course: unlearning bad habits
thinking in different ways
may have more knowledge, but still have a lot to learn
may not realise how much they have to learn

yellow: a bit of experience, somewhat confident

green: never programmed before, don't know what's going on, little
scared/alarmed, out of their depth?

can feel isolated: you're not the only one, and you're not alone!

can feel like you're behind, other students know things you don't etc

often more open-minded and open to learning more than "red" students

many of the best students we've ever had didn't have any programming
experience to start with


-->

</main>



<main is="slide" data-section-name="intro">

Should I take COMP1511?
====



</main>


<main is="slide" data-section-name="intro">

How?
====

__Lectures__  
introduce theory and practice of programming

__Tutorials__ and __Laboratories__  
reinforce ideas and concepts
with hands-on examples

__Assignments__  
assess understanding of C,
problem-solving skills

__Weekly Coding Tests__  
regular and realistic feedback of
your understanding of course content

__Final Exam__  
a 3-hour theory and practical extravaganza,
in CSE laboratories

</main>
<main is="slide" data-section-name="intro">

Assessment
==========

__assignment 0__  
worth 6%, due week ~5

__assignment 1__  
worth 12%, due week ~9

__assignment 2__  
worth 12%, due week ~12

__weekly coding tests__  
best 8 of 10, worth 8% total, weeks 3-12

__lab exercises__  
groups, worth 12% total, due weekly

__final exam__  
3h theory+practical exam, worth 50%, during the exam period

</main>
<main is="slide" data-section-name="intro">

Communications
==============

official communications from the course  
will come to your UNSW email address  
make sure you can receive emails!

if you set up email forwarding, __test it!__

when you send emails,  
send them from your UNSW email address  
and include your zID...  
don't email from personal email addresses!

to get in touch with the course urgently  
email `<cs1511@cse.unsw.edu.au>`

</main>

<main is="slide">

Course Forum
============

ask anything about the course / computing

receive answers from your tutors and classmates

<small>
link is on course website, or here:
<a href="https://edstem.org/courses/1950/">
https://edstem.org/courses/1950/</a>
</small>

</main>

<main is="slide" data-section-name="intro">

Course Evaluation and Development
=================================

assessed with __myExperience__
and the __Sturep Survey__

`* * *`

informal feedback during the semester is very welcome!

let us know of any problems as soon as they arise  
we can't fix problems we don't know about

<!--

talk to me, andrewt, your tutor, ...

Students are also encouraged to provide informal feedback during the
session, and to let the lecturer in charge know of any problems, as soon
as they arise. Suggestions will be listened to very openly, positively,
constructively, and thankfully, and every reasonable effort will be made
to address them.
-->

</main>
<main is="slide" data-section-name="intro">

Conduct and Integrity
=====================

treat people with __courtesy__ and __respect__  
... we are __all__ humans ...

`* * *`

pretending someone else's work is yours is __not okay__.

CSE is a bit different to other places...  
we don't care *how* you reference,  
we just care *that* you reference

<small>__important__: read the course outline!</small>

<!-- inclusive learning environment for ALL students, for EVERYBODY.

working with others is one of the most important/useful things in
computing

two people working together can achieve much more than twice as much as
one person working alone.

plagiarism+conduct is more than just box ticking, we do genuinely care.
this is very important.

-->


</main>
<main is="slide" data-section-name="intro">

More information?
=================

course material lives on WebCMS3  
<big>[`webcms3.cse.unsw.edu.au/COMP1511/18s1`](https://webcms3.cse.unsw.edu.au/COMP1511/18s1/)</big>

`* * *`

<big>please read the __course outline!__</big>

</main>
<main is="slide" data-section-name="intro">

How to do well in this course
=============================

__practice consistently__ across the __entire__ course

__prepare__ for all tutorials and labs  
by attempting the questions __before__ your class.

attend all tutorials and labs.  
__ask questions__.  
use your resources.  

make a list of each compile error you get,  
and how you fixed it.  
(they will come back to haunt you repeatedly...
this will be invaluable.)

</main>
</section>

<section name="resources">
<main is="slide" data-section-name="resources">

Resources
=========

__optional__ textbook: Alistair Moffat  
_Programing, Problem Solving, and Abstraction with C_  
(Pearson Educational, 2003; ISBN 978 1 74103 080 3)

__Google is your friend__,  
as is Stack Overflow,  
especially when debugging compile errors

</main>
<main is="slide" data-section-name="resources">

Getting Help
============

read the __course forum__  
ask your questions there, if they're not answered

ask one of your COMP1511 peers

ask your tutor!
<small>(they are all very friendly :-)</small>

talk to me after lectures

</main>
<main is="slide" data-section-name="resources">

Getting Started
===============

Before the end of this week, you should:

do __Lab 1__.

be able to write a 'hello world' program  
from your __CSE account at uni__, and

be able to write the 'hello world' program  
from your __home computer__.

</main>
</section>
<section name="orientation">
<main is="slide" data-section-name="orientation">

The CSE Labs
============

CSE has lab computers...

unlike other workstations at UNSW,  
these don't run Windows;  
they run Linux, which is very different

the easiest way to use these  
(if you're not in a lab)  
is using VLAB

use your __zID__ and __zPass__ to log in  
if you don't have a zID/zPass,  
you should fix that asap!

</main>
<main is="slide" data-section-name="orientation">

It's All Text!
==============

we write programs in a __text editor__  
very different to (e.g.) Word or Pages

we'll be programming in __C__  
which has well-defined rules for how the language works,  
which means we can use this to describe  
something that can be turned into a program  
that the computer can run

</main>
<main is="slide" data-section-name="orientation" class="title">

<h1><strong>let's try it!</strong></h1>

<!-- 

open VLAB

open a terminal

explain terminal / command line

list files

change directory

make directory

gedit (without &)

gedit (with &)

type out hello world
  - start out with comments
  - name, date

  - #includes

  - int main(void): function, the thing that happens when your program
    runs

  - printf

  - return 0: need to do at the end of program to say it's worked.

save it, now we can see w/ ls

computer can't understand C

we write C for humans to read, and then turn C into the language
computers can read

compiling

-->

</main>
<main is="slide" data-section-name="orientation">

Hello World
===========

``` c
// Prints out a friendly message.
// Andrew Bennett <andrew.bennett@unsw.edu.au>
// 2018-02-27

#include <stdio.h>

int main (void) {




    return 0;
}
```

</main>
<main is="slide" data-section-name="orientation">

Hello World
===========

``` c
// Prints out a friendly message.
// Andrew Bennett <andrew.bennett@unsw.edu.au>
// 2018-02-27

#include <stdio.h>

int main (void) {

    // Print out the famous 'hello world' message.
    printf ("Hello, world!\n");

    return 0;
}
```

</main>
<main is="slide" data-section-name="orientation">

Navigating on Unix
==================

__pwd__ shows where you currently are

<pre>$ <b>pwd</b>
/import/ravel/2/andrewb</pre>

__ls__ lists the items in the current directory

<pre>$ <b>ls</b>
18s1    bin     lib     public_html     tmp     web</pre>

__mkdir__ makes a new directory

<pre>$ <b>mkdir cs1511</b>
$ <b>ls</b>
18s1    bin     cs1511  lib     public_html     tmp     web</pre>

</main>
<main is="slide" data-section-name="orientation">

Navigating on Unix
==================

__cd__ changes directory

<pre>$ <b>cd cs1511</b>
$ <b>pwd</b>
/import/ravel/2/andrewb/cs1511
$ <b>ls</b>
$ </pre>

__cd ..__ changes into the previous directory

<pre>$ <b>cd ..</b>
$ <b>pwd</b>
/import/ravel/2/andrewb</pre>

</main>
<main is="slide" data-section-name="orientation">

Writing a Program
=================

to create a C program from the terminal,  
open a text editor like __gedit__

<pre>$ <b>gedit hello.c &</b></pre>

once the code is written and saved...  
compile it with __dcc__!

<pre>$ <b>dcc -o hello hello.c</b></pre>

</main>
<main is="slide">
end
</main>
</section>
<!--

<main is="slide" data-section-name="orientation">

Programming is a construction exercise
======================================

Think about the problem

Write down a proposed solutions

Break each step into smaller steps

Convert the basic steps into instructions
in the programming language

Use an *editor* to create a *file* that contains the program

Use the *compiler* to check the *syntax* of the program

Test the program on a range of data

</main>
<main is="slide" data-section-name="orientation">

Compiling
=========

A C program must be translated into *machine code* to be run.

this process is known as *compilation*,  
and is performed by a *compiler*.

We will use a compiler named __dcc__ for COMP1511  
dcc is actually a custom wrapper around a compiler named clang.

Another widely used compiler is called __gcc__.

</main>
</section>

<section name="variables">

<main is="slide" data-section-name="variables">

Variables and Types
===================

__Variables__ are used to store data...  
think "boxes"

Each variable has a __data type__...  
this is the size and structure of the "box"

For now, we are using three data types:

__char__ stores characters:  
`A`, `e`, `#`

__int__ stores whole numbers:  
`2`, `17`, `-5`

__float__ stores "floating-point" numbers:
`3.14159`, `2.71828`

</main>
<main is="slide" data-section-name="variables">

Variables
=========

__declare__  
the first time a variable is mentioned,  
we need to specify its type.

__initialise__  
before using a variable we need to assign it a value.

__assign__  
to give a variable a value.

``` c
int num; // Declare
num = 5; // Initialise (also Assign)
...
num = 27; // Assign
```

</main>
<main is="slide" data-section-name="variables">

Variables
=========

We can also declare and initialise in the same step:

``` c
int num = 5; // Declare and Initialise
...
num = 27; // Assign
```

</main>
<main is="slide" data-section-name="variables">

Variable Naming (and other identifiers)
=======================================

must be made up of letters, digits and underscores (`_`)  
the first character must be a letter  
are case sensitive (`num1` and `Num1` are different)

Keywords like  
`if`, `while`, `do`, `int`, `char`, `float`  
cannot be used as identifiers

</main>


</section>
-->
