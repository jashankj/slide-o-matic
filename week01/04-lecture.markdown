---
short-title: COMP1511 18s1 lec02
title: "Lecture 1: Numbers In, Numbers Out"
...


<main is="slide" class="title">
<h1><big><strong>Welcome!</strong></big></h1>

<h1>COMP1511 18s1<br />
    <small>Programming Fundamentals</small></h1>
</main>

<main is="slide" class="title">
<h1>COMP1511 18s1<br />
  <strong>— Lecture 1 —</strong><br />
  <small>Numbers In, Numbers Out</small></h1>

Andrew Bennett  
<small><tt>&lt;andrew.bennett@unsw.edu.au&gt;</tt></small>

more printf  
variables  
scanf

</main>

<main is="slide">

<p>&nbsp;</p>

<big><big><big><big><big>
<strong>
Before we begin...
</strong>
</big></big></big></big></big>

<p>&nbsp;</p>
<big>
__introduce__ yourself to the person sitting next to you  

<p>&nbsp;</p>


__why__ did they decide to study __computing__?
</big>

</main>


<main is="slide" data-section-name="admin">

Admin
=====

tutorials and laboratories start in week 1  
(some of you have already had tutes and labs)

lecture recordings are on WebCMS 3

make sure you have __home computing__ set up

</main>

<main is="slide" data-section-name="admin">

Getting Help
=====

<strong>
<big>post on the course forum</big>  
</strong>
I (or others) will answer so everyone can see

<strong>
<big>talk to me after the lecture</big>
</strong>

<strong>
<big>talk to your tutor</big>
</strong>

<strong>
<big>help labs</big>
</strong>


admin:  
<big>
__cs1511@cse.unsw.edu.au__</big>

if it needs to go to me directly:  
<big>__andrew.bennett@unsw.edu.au__</big>


</main>

<section name="review">
<main is="slide">

Hello World
===========

``` c
// Prints out a friendly message.
// Andrew Bennett <andrew.bennett@unsw.edu.au>
// 2018-02-27

#include <stdio.h>

int main (void) {

    // Print out the famous 'hello world' message.
    printf ("Hello, world!\n");

    return 0;
}
```

</main>
<main is="slide" data-section-name="orientation">

Navigating on Unix
==================

__pwd__ shows where you currently are

<pre>$ <b>pwd</b>
/import/ravel/2/andrewb</pre>

__ls__ lists the items in the current directory

<pre>$ <b>ls</b>
18s1    bin     lib     public_html     tmp     web</pre>

__mkdir__ makes a new directory

<pre>$ <b>mkdir cs1511</b>
$ <b>ls</b>
18s1    bin     cs1511  lib     public_html     tmp     web</pre>

</main>
<main is="slide" data-section-name="orientation">

Navigating on Unix
==================

__cd__ changes directory

<pre>$ <b>cd cs1511</b>
$ <b>pwd</b>
/import/ravel/2/andrewb/cs1511
$ <b>ls</b>
$ </pre>

__cd ..__ changes into the previous directory

<pre>$ <b>cd ..</b>
$ <b>pwd</b>
/import/ravel/2/andrewb</pre>

</main>
<main is="slide" data-section-name="orientation">

Writing a Program
=================

to create a C program from the terminal,  
open a text editor like __gedit__

<pre>$ <b>gedit hello.c &</b></pre>

once the code is written and saved...  
compile it with __dcc__!

<pre>$ <b>dcc -o hello hello.c</b></pre>

</main>

<main is="slide" data-section-name="orientation">

Programming is a construction exercise
======================================

think about the problem

write down a proposed solution

break each step into smaller steps

convert the basic steps into instructions
in the programming language

use an *editor* to create a *file* that contains the program

use the *compiler* to check the *syntax* of the program

test the program on a range of data

</main>
<main is="slide" data-section-name="orientation">

Compiling
=========

<big>
remember: we write C programs for __humans__ to read.
</big>

a C program must be translated into *machine code* to be run.

this process is known as *compilation*,  
and is performed by a *compiler*.

we will use a compiler named __dcc__ for COMP1511  
dcc is actually a custom wrapper around a compiler named clang.

another widely used compiler is called __gcc__.

</main>


<main is="slide">

The overall process
===================

<img
src="http://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/week01/compiling-diagram.png" style="width:800px">

</section>

<section name="printf">

<main is="slide" data-section-name="printf">

(re-)introducing: printf
========================

<big>`printf ("hello world!\n")`</big>

prints the text  
<big>`"hello world!"`</big>  
to the terminal

</main>
<main is="slide" data-section-name="printf">

Printing more than just words
=============================

<big>Can we print more than just words?</big>

</main>
<main is="slide" data-section-name="printf">

Printing more than just words
=============================

<big>Can we print more than just words?</big>

...  
__Yes!__

<big><tt>print<b>f</b></tt></big>  
the _f_ stands for "formatted"

</main>
<main is="slide" data-section-name="printf" class="title">

Let's try it out!
=================

<!--

Start out by typing out a hello world program again - good practice for
"how do we put the whole thing together".

then change it to print out numbers: hardcoded, "the sum of 3 + 5 is:"

can't change dynamically

variables

-->


</main>
<main is="slide" data-section-name="printf">

``` c
// Prints out the sum of two numbers.
// Andrew Bennett <andrew.bennett@unsw.edu.au>
// 2018-02-28

#include <stdio.h>

int main (void) {
    // Sum two numbers, and print them out.
    printf ("The sum of 3 and 5 is %d\n", 3 + 5);

    return 0;
}
```

</main>
<main is="slide" data-section-name="printf" class="title">


we can change the numbers  
to add different values together  
and print them out!

but that's boring if it can't be dynamic,  
and it sucks to do it by hand.

</main>



<section name="variables">

<main is="slide" data-section-name="variables" class="title">

<h1>Introducing<br />
  <big>__variables!__</big></h1>

</main>
<main is="slide" data-section-name="variables">

Variables and Types
===================

__Variables__ are used to store data...  
think "boxes"

Each variable has a __data type__...  
this is the size and structure of the "box"

For the next few weeks, we'll only use two data types:

__int__ stores whole numbers:  
`2`, `17`, `-5`

__float__ stores "floating-point" numbers:  
`3.14159`, `2.71828`

<small>
we'll look at other types in future weeks. can you think of any other
types that would be useful?
</small>

<!--
__char__ stores characters:  
`A`, `e`, `#`


-->

</main>
<main is="slide" data-section-name="variables">

Variables
=========

__declare__  
the first time a variable is mentioned,  
we need to specify its type.

__initialise__  
before using a variable we need to assign it a value.

__assign__  
to give a variable a value.

``` c
int num; // Declare
num = 5; // Initialise (also Assign)
...
num = 27; // Assign
```

</main>
<main is="slide" data-section-name="variables">

Variables
=========

we can also declare and initialise in the same step:

``` c
int num = 5; // Declare and Initialise
...
num = 27; // Assign
```

</main>
<main is="slide" data-section-name="variables">

Variable Naming (and other identifiers)
=======================================

must be made up of letters, digits and underscores (`_`)  
the first character must be a letter  
are case sensitive (`num1` and `Num1` are different)

Keywords like  
`if`, `while`, `do`, `int`  
cannot be used as identifiers

</main>
<main is="slide" data-section-name="variables">

Printing Variables Out
======================

__No variables:__

``` c
printf ("Hello World\n");
```

__A single variable:__

``` c
int num = 5;
printf ("num is %d\n", num);
```

</main>
<main is="slide" data-section-name="variables">

Printing Variables Out
======================

__More than one variable:__

``` c
int num1 = 5;
int num2 = 17;
printf("num1 is %d and num2 is %d\n", num1, num2);
```

The order of arguments  
is the order they will appear:

``` c
int num1 = 5;
int num2 = 17;
printf ("num2 is %d and num1 is %d\n", num2, num1);
```

</main>
<main is="slide" data-section-name="variables">

`printf`'s placeholders
=======================

_int_ uses __`%d`__  
<!--
_char_ uses __`%c`__  
_float_ uses __`%f`__  
_double_ uses __`%lf`__

__Try it yourself!__  
Copy the code from the previous slide  
into a C program, and run it.

Change the program so it  
declares, initialises and prints  
a `char`, a `float` and a `double`.
-->

</main>
<main is="slide" data-section-name="scanf">

Numbers in: `scanf`
===================

``` c
int num = 0;
scanf ("%d", &num);
printf ("num = %d\n", num);
```

Note that the variable is still initialised.  
(Not necessary, but good practice.)

Note the & before the variable name.  
__Don't forget it!__

</main>
<main is="slide" data-section-name="scanf">

Reading Variables In
====================

__Multiple variables (space separated):__

``` c
int num1 = 0;
int num2 = 0;
scanf ("%d %d", &num1, &num2);
printf ("num1 = %d and num2 = %d\n", num1, num2);
```

__Multiple variables (comma separated):__

``` c
int num1 = 0;
int num2 = 0;
scanf ("%d, %d", &num1, &num2);
printf ("num1 = %d and num2 = %d\n", num1, num2);
```

Note the space or comma between the variables.

</main>
<!--

<main is="slide" data-section-name="scanf">

Try it yourself: `scanf`
========================

Create a C program  
using the code from the previous slide.


Using what you know about placeholders  
with `printf` and `scanf`,  
change it so it scans in  
and prints out a _char_.

</main>
-->

</section>

<section name="if">

<main is="slide" data-section-name="decisions" class="title">

<big><h1>making <strong>decisions</strong></h1></big>

different behaviour in different situations

</main>

<!-- -->

<main is="slide" data-section-name="decisions">

Driving, Take 1
===============

Write a program
which asks the user to enter their age.

If they are at least 16 years old,  
then, display "You can drive."

Then, whether or not they can drive,  
display "Have a nice day."

</main>
<main is="slide" data-section-name="decisions">

Driving, Take 1
===============

Write a program
which asks the user to enter their age.

__If__ they are at least 16 years old,  
__then__, display "You can drive."

Then, whether or not they can drive,  
display "Have a nice day."

</main>
<main is="slide" data-section-name="decisions">

Driving, Take 1: Step by Step
=============================

... Print "How old are you?"  
... Read in their age.  
... If their age is ≥ 16: print "You can drive".  
... Print "Have a nice day."

</main>
<main is="slide" data-section-name="decisions">

``` c
// Can a user drive?
// Andrew Bennett <andrew.bennett@unsw.edu.au>
// 2017-07-31

#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
    printf ("How old are you? ");
    int age = 0;
    if (age >= 16) {
        printf ("You can drive.\n");
    }

    printf ("Have a nice day.\n");

    return EXIT_SUCCESS;
}
```

</main>
<main is="slide" data-section-name="decisions">

Detour: Defining Constant Values
================================

Using the same value numerous times in a program  
becomes high maintenance if the value changes...  
and needs to be changed in many places.  
(You may miss one!)

Other developers may not know (or you may forget!)  
what this magical number means.

``` c
#define MIN_DRIVING_AGE 16
```

__note__  
there is no semicolon at the end of this line

</main>
<main is="slide" data-section-name="decisions">

``` c
// Can a user drive?
// Andrew Bennett <andrew.bennett@unsw.edu.au>
// 2017-07-31

#include <stdio.h>
#include <stdlib.h>

#define MIN_DRIVING_AGE 16

int main (int argc, char *argv[]) {
    printf ("How old are you? ");
    int age = 0;
    if (age >= MIN_DRIVING_AGE) {
        printf ("You can drive.\n");
    }

    printf ("Have a nice day.\n");

    return EXIT_SUCCESS;
}
```

</main>
<main is="slide" data-section-name="else">

Driving, Take 2
===============

Write a program
which asks the user to enter their age.

If they are at least 16 years old,  
then display "You can drive."  
Otherwise, display "You cannot drive."

Then, whether or not they can drive,  
display "Have a nice day."

</main>
<main is="slide" data-section-name="else">

Driving, Take 2
===============

Write a program
which asks the user to enter their age.

__If__ they are at least 16 years old,  
__then__ display "You can drive."  
__Otherwise__, display "You cannot drive."

Then, whether or not they can drive,  
display "Have a nice day."

</main>
<main is="slide" data-section-name="else">

Driving, Take 2: Step by Step
=============================

... Print "How old are you?"  
... Read in their age.  
... If their age is ≥ 16: print "You can drive".  
... Otherwise: print "You cannot drive".  
... Print "Have a nice day."

</main>
<main is="slide" data-section-name="decisions">

``` c
// Can a user drive?
// Andrew Bennett <andrew.bennett@unsw.edu.au>
// 2017-07-31

#include <stdio.h>
#include <stdlib.h>

#define MIN_DRIVING_AGE 16

int main (int argc, char *argv[]) {
    printf ("How old are you? ");
    int age = 0;
    if (age >= MIN_DRIVING_AGE) {
        printf ("You can drive.\n");
    } else {
        printf ("You cannot drive.\n");
    }

    printf ("Have a nice day.\n");

    return EXIT_SUCCESS;
}
```

</main>
<main is="slide" data-section-name="else">

More Conditions!
================

Sometimes, we want to consider  
more than two options for paths.

In the case of the driving scenario,  
we want to make sure
the age is ≥ 0 and ≤ 120...

</main>
<main is="slide" data-section-name="else">

Driving, Take 3
===============

``` c
    printf ("How old are you? ");
    int age = 0;
    if (age < 0) {
        printf ("Invalid input.\n");
    } else if (age < MIN_DRIVING_AGE) {
        printf ("You cannot drive.\n");
    } else if (age <= MAX_DRIVING_AGE) {
        printf ("You can drive.\n");
    } else {
        printf ("Invalid input.\n");
    }

    printf ("Have a nice day.\n");
```

</main>
<main is="slide" data-section-name="logic">

Conditions in C
===============

__less than__  
in maths, $<$; in C, <big>`<`</big>

__less than or equal to__  
in maths, $\le$; in C, <big>`<=`</big>

__greater than__  
in maths, $>$; in C, <big>`>`</big>

__greater than or equal to__  
in maths, $\ge$; in C, <big>`>=`</big>

__equal to__  
in maths, $=$; in C, <big>`==`</big>

__not equal to__  
in maths, $\ne$; in C, <big>`!=`</big>

</main>
<main is="slide" data-section-name="logic">

Nested Conditions
=================

``` c
    if (age >= MIN_DRIVING_AGE) {
        if (age <= MAX_DRIVING_AGE) {
            printf ("You can drive.\n");
        }
    }
```

</main>
<main is="slide" data-section-name="logic">

Logical Operators in C
======================

useful when we want to check  
multiple conditions in a single if statement.  
C uses __Boolean logic__:

__AND__  
in maths, $\land$; in C, <big>`&&`</big>  
_both expressions must be true_

__OR__  
in maths, $\lor$; in C, <big>`||`</big>  
_either or both expressions must be true_

__NOT__  
in maths, $\lnot$; in C, <big>`!`</big>  
_the expression must be false_

</main>
<main is="slide" data-section-name="logic">

Nested Conditions, Redux
========================

``` c
    if (age >= MIN_DRIVING_AGE) {
        if (age <= MAX_DRIVING_AGE) {
            printf ("You can drive.\n");
        }
    }
```

is the same as


``` c
    if (age >= MIN_DRIVING_AGE && age <= MAX_DRIVING_AGE) {
        printf ("You can drive.\n");
    }
```

</main>
<main is="slide" data-section-name="logic">

An Iffy Answer
==============

<pre>
if (<i>condition 1</i>) {
    // Do stuff
} else if (<i>condition 2</i>) {
    // Do something else
} else if (<i>condition 3</i>) {
    // Do something completely different
} else {
    // In all other cases, do this.
}
</pre>

</main>

<main is="slide" data-section-name="style">

Indentation
==============

```c
if (condition 1) {
// Do stuff
} else if (condition 2) {
// Do something else
} else if (condition 3) {
// Do something completely different
} else {
// In all other cases, do this.
}
```

</main>
 
<main is="slide" data-section-name="style">

Indentation
==============

``` c
if (condition 1) {
    // Do stuff
} else if (condition 2) {
    // Do something else
} else if (condition 3) {
    // Do something completely different
} else {
    // In all other cases, do this.
}
```

</main>
<main is="slide" data-section-name="style">

Indentation
==============

``` c
if (condition 1) {
if (condition 2) {
if (condition 3) {
// Do stuff
} else if (condition 4) {
// Do something else
}
} else if (condition 5) {
if (condition 6) {
// Do something completely different
}
} else {
// In all other cases, do this.
}
}

```
</main>
<main is="slide" data-section-name="style">

Indentation
==============

```c
if (condition 1) {
    if (condition 2) {
        if (condition 3) {
            // Do stuff
        } else if (condition 4) {
            // Do something else
        }
    } else if (condition 5) {
        if (condition 6) {
            // Do something completely different
        }
    } else {
        // In all other cases, do this.
    }
}
```

</main>
<main is="slide" data-section-name="????">

Application: Leap Years
==========

nearly every four years

keeping the calendar in line with the real world

</main>
<main is="slide" data-section-name="????">

Leap Years
==========

> <em>
> Every year that is exactly divisible by four is a leap year,  
> except for years that are exactly divisible by 100,  
> but these centurial years are leap years  
> if they are exactly divisible by 400.  
> For example, the years 1700, 1800, and 1900  
> were not leap years,  
> but the years 1600 and 2000 were.
> </em>

</main>

<main is="slide" data-section-name="????">

Leap Years
==========

Tutorial/lab exercise for this week

</main>


<main is="slide" data-section-name="">

Error handling
==============

Driving revisited:   
What if somebody types in an invalid age?

</main>
<main is="slide" data-section-name="">
























<!-- -->
</section>

<section name="end">

<main is="slide">

Feedback?
=========


<strong><big>
<a href="http://bit.do/comp1511-feedback-week1" style="color:#f9b200">
bit.do/comp1511-feedback-week1</a>
</big></strong>

<hr>

<img
src="http://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/week01/comp1511-week1-feedback-qr.png" />

</main>

</section>



<!--
<section name="variables">

<main is="slide" data-section-name="variables">

Variables and Types
===================

__Variables__ are used to store data...  
think "boxes"

Each variable has a __data type__...  
this is the size and structure of the "box"

For now, we are using three data types:

__char__ stores characters:  
`A`, `e`, `#`

__int__ stores whole numbers:  
`2`, `17`, `-5`

__float__ stores "floating-point" numbers:
`3.14159`, `2.71828`

</main>
<main is="slide" data-section-name="variables">

Variables
=========

__declare__  
the first time a variable is mentioned,  
we need to specify its type.

__initialise__  
before using a variable we need to assign it a value.

__assign__  
to give a variable a value.

``` c
int num; // Declare
num = 5; // Initialise (also Assign)
...
num = 27; // Assign
```

</main>
<main is="slide" data-section-name="variables">

Variables
=========

we can also declare and initialise in the same step:

``` c
int num = 5; // Declare and Initialise
...
num = 27; // Assign
```

</main>
<main is="slide" data-section-name="variables">

Variable Naming (and other identifiers)
=======================================

must be made up of letters, digits and underscores (`_`)  
the first character must be a letter  
are case sensitive (`num1` and `Num1` are different)

Keywords like  
`if`, `while`, `do`, `int`, `char`, `float`  
cannot be used as identifiers

</main>


</section>
-->


