---
short-title: COMP1511 18s1 lec17
title: "Lecture 17: Linked Lists"
...

define(CODE, {{<pre><code class="language-c">}})
define(NOCODE, {{</code></pre>}})

define(CSTART, {{<pre><code class="language-c">}})
define(CEND, {{</code></pre>}})

<style>
    img {
        max-width: 400px;
    }
</style>

<main is="slide" class="title">
<h1><big><strong>Welcome!</strong></big></h1>

<h1>COMP1511 18s1<br />
    <small>Programming Fundamentals</small></h1>
</main>
<main is="slide" class="title">
<h1>COMP1511 18s1<br />
  <strong>— Lecture 17 —</strong><br />
  <small>Linked Lists</small></h1>

Andrew Bennett  
<small><tt>&lt;andrew.bennett@unsw.edu.au&gt;</tt></small>

</main>
<!--
<main is="slide">

<p>&nbsp;</p>

<big><big><big><big><big>
<strong>
Before we begin...
</strong>
</big></big></big></big></big>

<p>&nbsp;</p>
<big>
__introduce__ yourself to the person sitting next to you  

<p>&nbsp;</p>

how are they going with __assignment 2__?
</big>
</main>
-->
<!--
<main is="slide">

Feedback
=======

upload __lecture code__  


upload/incorporate __diagrams__  
in lecture recordings

<i>more</i> __diagrams__

go through programs __as a whole__  
before running them

lecture __subtitles__

</main>

-->
<main is="slide">

Overview
=======

<big><strong>after this lecture, you should be able to...</strong></big>

have a better understanding of __linked lists__

write code to __create__ a linked list

write code to __traverse__ a linked list

solve simple problems using __linked lists__

<small><small><small>
(__note__: you shouldn't be able to do all of these immediately after
watching this lecture. however, this lecture should (hopefully!) give
you the foundations you need to develop these skills. remember:
programming is like learning any other language, it takes consistent and
regular practice.)
</small>
</small>
</small>

</main>
<main is="slide">

Admin
=====

<big>
<big><big><big><strong>Don't panic!</strong></big></big></big>
</big>

__assignment 2__  
(if you haven't started yet, start <strong><big><big>ASAP</big></big></strong>)  
<small>deadline extended to <strong>Sunday 13th May</strong></small>

__assignment 1__  
<small>tutor marking/feedback in progress</small>

__week 9 weekly test__ out now  

don't forget about __help sessions__!  
<small>see course website for details</small>

</main>
<main is="slide">

Help Sessions
=====

__Wednesday__  
6-8pm, J17 201

__Thursday__  
6-8pm, J17 201

__Friday__  
10am-12pm, Brass Lab (J17 Level 3)  
2pm-4pm, Brass Lab (J17 Level 3)  
4pm-6pm, Oboe Lab (J17 Level 3)  

<small>
note: Brass Lab = Bugle/Horn
</small>


</main>
<!--

<section name="linked-lists">

linked lists

"linked lists"

"but tomorrow definitely linked lists"

okay sure.

</section>
-->
<!--

I'm just going to very slowly

creation of list including insert (at front) and append (insert at end) including a last function

plus read-only operations: length + find node with particular value

then if time freeing a list

-->
<section name="review">
<main is="slide" class="title">

<h1>
a quick __recap__ of yesterday
</h1>

</main>

</section>

<section name="node struct">
<!--
<main is="slide" class="title">

<big><h1>introducing: <strong>something</strong></h1></big>

</main>
-->
<main is="slide">

The node struct
=====

CODE
struct node {
    int data;
    struct node *next;
};
NOCODE

</main>
<main is="slide">

Interacting with a node struct
=====

CODE
struct node {
    int data;
    struct node *next;
};

// "struct node hello" (no *)
// "hello" is an actual node in the function's memory
struct node hello;
hello.data = 10;
hello.next = NULL;

// in the function's memory
//        ______
// hello |  10  |
//       |------|
//       | NULL |
//       |______|
NOCODE

</main>
<main is="slide">

Making a new node
=====

CODE
// Allocates memory for a new node; returns its address
struct node *make_node(int value) {
    struct node *new = malloc(1 * sizeof(struct node));
    new->data = value;
    new->next = NULL;
    return new;
}

// "struct node * hello"
// "hello" is a pointer to a node,
// it just stores the _address_
// (of the memory we get from malloc)
struct node *hello = make_node(10);

// in the heap (malloced memory)
//        ______
// hello |  10  |
//       |------|
//       | NULL |
//       |______|
NOCODE

</main>
<main is="slide">

Freeing a node
=====

CODE
// In accordance with Newton's 3rd Law of Memory Allocation
// "For every malloc, there is an equal and opposite free"
void free_node(struct node *node) {
    free(node);
}

struct node *hello = make_node(10);
free_node(hello);
NOCODE

</main>
</section>

<section name="node pointers">

<main is="slide">

Node pointers vs allocated nodes
===

**reference** to a node  
<small>arrow</small>

CODE
struct node *curr ...
NOCODE


vs

making (**allocating**) a new node  
<small>circle</small>

CODE
... = malloc(1 * sizeof(struct node));
NOCODE

</main>
<main is="slide">

Node pointers vs allocated nodes
===

**reference** to a node  
<small>arrow</small>

CODE
struct node *curr ...
NOCODE


vs

making (**allocating**) a new node  
<small>circle</small>

CODE
... = malloc(1 * sizeof(struct node));
NOCODE

<img
src="https://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/diagrams/linked_lists/struct_node_star_cyan_alpha.png">


</main>
<main is="slide">

Node pointers vs allocated nodes
===
**reference** to a node <small>(arrow)</small> vs  
making (**allocating**) a new node <small>(circle)</small>

<img
src="https://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/diagrams/linked_lists/struct_node_star_curr_equals_malloc_cyab_alpha.png" style="max-width:700px">

</main>

</section>
<section name="list traversal">
<main is="slide" class="title">

__array__/__list__ "__traversal__"  
=================

<small>(going through every element)</small>

</main>

<main is="slide">

Traversing... an Array
======================

CODE
void fillArray (int array[ARRAY_SIZE], int value) {
    int i = 0;
    while (i < ARRAY_SIZE) {
        array[i] = value;  // set the value
        i++;               // move to next element
    }
}
NOCODE

</main>
<main is="slide">

Traversing... a Linked List
===========================

CODE
void fillList (struct node *list, int value) {
    struct node *curr = list;
    while (curr != NULL) {
        curr->data = value; // set the value
        curr = curr->next;   // move to next node
    }
}
NOCODE

</main>
</section>

<!-- 

the common list loop
read-only operations are all the same
eg list sum, list length, find in list?

add to start of list
add to end of list

-->

<section>
<main is="slide" class="title">
<h1>
and now for today's content...
</h1>
</main>

<section name="looping through a list">

<main is="slide">

The Standard List Loop
================

CODE
struct node *curr = list;

while (curr != NULL) {

    ?????

    curr = curr->next;
}
NOCODE

</main>
<main is="slide">

The Standard List Loop -- List Length
================

How can we calculate the length of a list?  
<small>i.e. how many nodes are in the list</small>

CODE
struct node *curr = list;

int num_nodes = 0;

while (curr != NULL) {

    num_nodes += 1;

    curr = curr->next;
}
NOCODE

</main>
<main is="slide">

The Standard List Loop -- List Sum
================

How can we sum all of the elements in a list?  
<small>i.e. add the values of all of the nodes together</small>

CODE
struct node *curr = list;

// int num_nodes = 0;
?????

while (curr != NULL) {

    // num_nodes += 1;
    ?????

    curr = curr->next;
}
NOCODE

</main>

</section>


<section name="changing a list">

<main is="slide">

Inserting Into a List
================

adding new nodes to our list....

insert at the __start__

insert at the __end__

insert in the __middle__

</main>
<section name="an aside">

<main is="slide">

An aside: When things go wrong
================

what if our list is __empty__?

what would this look like in code?

</main>
<main is="slide">

An aside: Function Comments
================

it's important to __document__ your functions:

what do they __assume__?

what does the __caller__ need to do?

</main>

</section>

<section name="putting it together">
<main is="slide">

Building Blocks
================

we can construct __complex__ list operations out of __simple__ functions

<!--
`insert_after`

`get_last`

`get_nth`
-->

</main>

</section>

<!--
<section name="list operations">

<main is="slide">

# What can we do with linked lists?

__insert into list__

remove from list

create a new node

print out list

</main>
<main is="slide">

# What can we do with linked lists?

insert into list

__remove from list__

create a new node

print out list

</main>
<main is="slide">

# What can we do with linked lists?

insert into list

remove from list

__create a new node__

print out list

</main>
<main is="slide">

# What can we do with linked lists?

insert into list

remove from list

create a new node

__print out list__

</main>
</section>
-->
<!--
<section name="using lists">

<main is="slide" class="title">

Working with Linked Lists
===

going through __every__ node  
vs  
stopping at a __certain node__

</main>

<main is="slide">

Going through every node
===

list length?

list sum?

printing a list?


</main>

<main is="slide">

Stopping at a certain node/position
===

add to end?

add to middle?

</main>

</section>
-->

<!--
<section name="section">

<main is="slide" class="title">

<big><h1>introducing: <strong>something</strong></h1></big>


</main>


<main is="slide">

title
=====

contents

</main>

<main is="slide">

title
=====

contents

</main>

</section>
-->

<!--
<main is="slide">

Feedback?
=========


<strong><big>
<a href="http://bit.do/comp1511-feedback-week4" style="color:#f9b200">
bit.do/comp1511-feedback-week4</a>
</big></strong>

<hr>

<img
src="http://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/week04/comp1511-week4-feedback-qr.png" />

alternate link: <a href="https://andrewb3.typeform.com/to/WrAhfV"
style="color:#f9b200">
https://andrewb3.typeform.com/to/WrAhfV
</a>
</main>

-->

<main is="slide">
end
</main>

</section>

<!-- code highlighted
<pre><code class="lang-clike">
if (condition 1) {
    if (condition 2) {
        if (condition 3) {
            // Do stuff
        } else if (condition 4) {
            // Do something else
        }
    } else if (condition 5) {
        if (condition 6) {
            // Do something completely different
        }
    } else {
        // In all other cases, do this.
    }
}
</code></pre>
-->


