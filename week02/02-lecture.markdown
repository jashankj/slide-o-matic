---
short-title: COMP1511 18s1 lec02
title: "Lecture 2: An Iffy Question"
...


<style>
pre {
    padding-top: 0em !important;
}
</style>

<main is="slide" class="title">
<h1><big><strong>Welcome!</strong></big></h1>

<h1>COMP1511 18s1<br />
    <small>Programming Fundamentals</small></h1>
</main>

<main is="slide" class="title">
<h1>COMP1511 18s1<br />
  <strong>— Lecture 2 —</strong><br />
  <small>An Iffy Question</small></h1>

Andrew Bennett  
<small><tt>&lt;andrew.bennett@unsw.edu.au&gt;</tt></small>

review: variables  
decisions and conditions  
constants with `#define`  

</main>

<main is="slide">

<p>&nbsp;</p>

<big><big><big><big><big>
<strong>
Before we begin...
</strong>
</big></big></big></big></big>

<p>&nbsp;</p>
<big>
__introduce__ yourself to the person sitting next to you  

<p>&nbsp;</p>


__why__ did they decide to study __computing__?
</big>

</main>

<main is="slide">

Feedback
=======

talk __louder__

talk __slower__

lecture/content __pace__  
(too fast, too slow)

lesson __summary__/__objectives__


</main>



<main is="slide">

Overview
=======

<big><strong>after this lecture, you should be able to...</strong></big>

create and use __variables__ with type `int` and `double`

use `#define`s to represent constants in your code

construct a __flow control diagram__ for a given situation

write code using __if statements__: `if`, `else if`, `else`

write code using __nested if statements__

<small>
(__note__: you shouldn't be able to do all of these immediately after
watching this lecture. however, this lecture should (hopefully!) give
you the foundations you need to develop these skills. remember:
programming is like learning any other language, it takes consistent and
regular practice.)
</small>
</main>


<main is="slide" data-section-name="admin">

Admin
=====

<big><strong>Don't panic!</strong></big>

lecture recordings are on WebCMS3

make sure you have __home computing__ set up

</main>

<section name="review">

<main is="slide">

In Review: Variables
====================

__declare__  
the first time a variable is mentioned,  
we need to specify its type.

__initialise__  
before using a variable we need to assign it a value.

__assign__  
to give a variable a value.

``` c
int num; // Declare
num = 5; // Initialise (also Assign)
...
num = 27; // Assign
```
</main>

<main is="slide" data-section-name="variables">

In Review: Printing Variables Out
======================

__No variables:__

``` c
printf ("Hello World\n");
```

__A single variable:__

``` c
int num = 5;
printf ("num is %d\n", num);
```

</main>
<main is="slide" data-section-name="variables">

In Review: Printing Variables Out
======================

__More than one variable:__

``` c
int num1 = 5;
int num2 = 17;
printf("num1 is %d and num2 is %d\n", num1, num2);
```

The order of arguments  
is the order they will appear:

``` c
int num1 = 5;
int num2 = 17;
printf ("num2 is %d and num1 is %d\n", num2, num1);
```

</main>

<main is="slide" data-section-name="scanf">

In Review: Numbers in: `scanf`
===================

``` c
int num = 0;
scanf ("%d", &num);
printf ("num = %d\n", num);
```

Note that the variable is still initialised.  
(Not necessary, but good practice.)

Note the & before the variable name.  
__Don't forget it!__

</main>



<main is="slide" data-section-name="orientation">

In Review: Compiling
=========

<big>
remember: we write C programs for __humans__ to read.
</big>

a C program must be translated into *machine code* to be run.

this process is known as *compilation*,  
and is performed by a *compiler*.

we will use a compiler named __dcc__ for COMP1511  
dcc is actually a custom wrapper around a compiler named clang.

another widely used compiler is called __gcc__.

</main>


<main is="slide">

The overall process
===================

<img
src="http://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/week01/compiling-diagram.png" style="width:800px">

</section>
<section name="if">

<main is="slide" data-section-name="decisions" class="title">

<big><h1>making <strong>decisions</strong></h1></big>

different behaviour in different situations

</main>

<!--   -->

<main is="slide" data-section-name="decisions">

In Review: Driving, Take 1
===============

Write a program
which asks the user to enter their age.

If they are at least 16 years old,  
then, display "You can drive."

Then, whether or not they can drive,  
display "Have a nice day."

</main>
<main is="slide" data-section-name="decisions">

In Review: Driving, Take 1
===============

Write a program
which asks the user to enter their age.

__If__ they are at least 16 years old,  
__then__, display "You can drive."

Then, whether or not they can drive,  
display "Have a nice day."

</main>
<main is="slide" data-section-name="decisions">

In Review: Driving, Take 1: Step by Step
=============================

... Print "How old are you?"  
... Read in their age.  
... If their age is ≥ 16: print "You can drive".  
... Print "Have a nice day."

</main>
<main is="slide" data-section-name="decisions">

<pre><code class="lang-clike">
// Can a user drive?
// Andrew Bennett <andrew.bennett@unsw.edu.au>
// 2018-03-06

#include &lt;stdio.h&gt;

int main (void) {
    printf ("How old are you? ");
    int age = 0;
    scanf("%d", &age);
    if (age >= 16) {
        printf ("You can drive.\n");
    }

    printf ("Have a nice day.\n");

    return 0;
}
</code></pre>

</main>

<!-- -->

<main is="slide" data-section-name="decisions">

Driving, Take 2
===============

Write a program
which asks the user to enter their age.

If they are at least 16 years old,  
then, display "You can drive."  
Otherwise, display "You can't drive."

Then, whether or not they can drive,  
display "Have a nice day."

</main>
<main is="slide" data-section-name="decisions">

Driving, Take 2
===============

Write a program
which asks the user to enter their age.

__If__ they are at least 16 years old,  
__then__, display "You can drive."  
__Otherwise__, display "You can't drive."

Then, whether or not they can drive,  
display "Have a nice day."

</main>
<main is="slide" data-section-name="decisions">

Driving, Take 2, Step by Step
=============================

... Print "How old are you?"  
... Read in their age.  
... If their age is ≥ 16: print "You can drive".  
... Otherwise, print "You can't drive".  
... Print "Have a nice day."

</main>

<main is="slide" data-section-name="decisions">

<pre><code class="lang-clike">
// Can a user drive?
// Andrew Bennett <andrew.bennett@unsw.edu.au>
// 2018-03-06

#include &lt;stdio.h&gt;

int main (void) {
    printf ("How old are you? ");
    int age = 0;
    scanf("%d", &age);
    if (age >= 16) {
        printf ("You can drive.\n");
    } else {
        printf ("You can't drive.\n");
    }
    printf ("Have a nice day.\n");

    return 0;
}
</code></pre>

</main>

<main is="slide">

Flow Control Diagrams
===================

<img src="http://www.cse.unsw.edu.au/~andrewb/slides/driving_basic.png"
style="max-width:800px">

</main>

<main is="slide" data-section-name="decisions">

Detour: Defining Constant Values
================================

Using the same value numerous times in a program  
becomes high maintenance if the value changes...  
and needs to be changed in many places.  
(You may miss one!)

Other developers may not know (or you may forget!)  
what this magical number means.

<pre><code class="lang-clike">
#define MIN_DRIVING_AGE 16
</code></pre>

__note__  
there is no semicolon at the end of this line

</main>

<main is="slide" data-section-name="decisions">

<pre><code class="lang-clike">
// Can a user drive?
// Andrew Bennett <andrew.bennett@unsw.edu.au>
// 2018-03-06

#include &lt;stdio.h&gt;

#define MIN_DRIVING_AGE 16

int main (void) {
    printf ("How old are you? ");
    int age = 0;
    scanf("%d", &age);
    if (age >= MIN_DRIVING_AGE) {
        printf ("You can drive.\n");
    } else {
        printf ("You can't drive.\n");
    }

    printf ("Have a nice day.\n");

    return 0;
}
</code></pre>

</main>

<main is="slide" data-section-name="else">

More Conditions!
================

Sometimes, we want to consider  
more than two options for paths.

In the case of the driving scenario,  
we want to make sure
the age is ≥ 0 and ≤ 120...

</main>
<main is="slide" data-section-name="else">

Driving, Take 3
===============

<pre><code class="lang-clike">
    printf ("How old are you? ");
    int age = 0;
    scanf("%d", &age);
    if (age < 0) {
        printf ("Invalid input.\n");
    } else if (age < MIN_DRIVING_AGE) {
        printf ("You can't drive.\n");
    } else if (age <= MAX_DRIVING_AGE) {
        printf ("You can drive.\n");
    } else {
        printf ("Invalid input.\n");
    }

    printf ("Have a nice day.\n");
</code></pre>

</main>
<main is="slide" data-section-name="logic">

Conditions in C
===============

__less than__  
in maths, $<$; in C, <big>`<`</big>

__less than or equal to__  
in maths, $\le$; in C, <big>`<=`</big>

__greater than__  
in maths, $>$; in C, <big>`>`</big>

__greater than or equal to__  
in maths, $\ge$; in C, <big>`>=`</big>

__equal to__  
in maths, $=$; in C, <big>`==`</big>

__not equal to__  
in maths, $\ne$; in C, <big>`!=`</big>

</main>

<main is="slide" data-section-name="logic">

Equals: Equality vs Assignment
===========================

__equality__: is this equal to?

e.g.
```
if (age == 18) {
    // then do something
}
```

__assignment__: make this be equal to

e.g.
```
// Set the variable 'age' to have the value 18
age = 18;
```

</main>




<main is="slide" data-section-name="logic">

Nested Conditions
=================

<pre><code class="lang-clike">
    if (age >= MIN_DRIVING_AGE) {
        if (age <= MAX_DRIVING_AGE) {
            printf ("You can drive.\n");
        }
    }
</code></pre>

</main>
<main is="slide" data-section-name="logic">

Logical Operators in C
======================

useful when we want to check  
multiple conditions in a single if statement.  
C uses __Boolean logic__:

__AND__  
in maths, $\land$; in C, <big>`&&`</big>  
_both expressions must be true_

__OR__  
in maths, $\lor$; in C, <big>`||`</big>  
_either or both expressions must be true_

__NOT__  
in maths, $\lnot$; in C, <big>`!`</big>  
_the expression must be false_

</main>
<main is="slide" data-section-name="logic">

Nested Conditions, Redux
========================

<pre><code class="lang-clike">
    if (age >= MIN_DRIVING_AGE) {
        if (age <= MAX_DRIVING_AGE) {
            printf ("You can drive.\n");
        }
    }
</code></pre>

is the same as


<pre><code class="lang-clike">
    if (age >= MIN_DRIVING_AGE && age <= MAX_DRIVING_AGE) {
        printf ("You can drive.\n");
    }
</code></pre>

</main>
<main is="slide" data-section-name="logic">

An Iffy Answer
==============

<pre>
if (<i>condition 1</i>) {
    // Do stuff
} else if (<i>condition 2</i>) {
    // Do something else
} else if (<i>condition 3</i>) {
    // Do something completely different
} else {
    // In all other cases, do this.
}
</pre>

</main>

<!-- indentation -->
 
<section name="style">

<main is="slide" class="title">

<big><h1>programming <strong>style</strong></h1></big>

what makes "good" code?  
why do we care?

</main>


<main is="slide" data-section-name="style">

Indentation
==============

<pre><code class="lang-clike">
if (condition 1) {
// Do stuff
} else if (condition 2) {
// Do something else
} else if (condition 3) {
// Do something completely different
} else {
// In all other cases, do this.
}
</code></pre>

</main>
 
<main is="slide" data-section-name="style">

Indentation
==============

<pre><code class="lang-clike">
if (condition 1) {
    // Do stuff
} else if (condition 2) {
    // Do something else
} else if (condition 3) {
    // Do something completely different
} else {
    // In all other cases, do this.
}
</code></pre>

</main>
<main is="slide" data-section-name="style">

Indentation
==============

<pre><code class="lang-clike">
if (condition 1) {
if (condition 2) {
if (condition 3) {
// Do stuff
} else if (condition 4) {
// Do something else
}
} else if (condition 5) {
if (condition 6) {
// Do something completely different
}
} else {
// In all other cases, do this.
}
}

</code></pre>
</main>
<main is="slide" data-section-name="style">

Indentation
==============

<pre><code class="lang-clike">
if (condition 1) {
    if (condition 2) {
        if (condition 3) {
            // Do stuff
        } else if (condition 4) {
            // Do something else
        }
    } else if (condition 5) {
        if (condition 6) {
            // Do something completely different
        }
    } else {
        // In all other cases, do this.
    }
}
</code></pre>
</main>
</section>

<!-- 

numbers have _types_
--------------------

so far we've seen `int`s: whole numbers

// if you think about it, they have to be stored by the computer somehow

maximum possible value

minimum possible value

what happens if we go over?


but there are more numbers than just whole numbers - in the 'real'
world, numbers can have decimal points too.

we need a different way to store these: `double`.

declaring

printf/scanf

-->

<section name="ints"> 
<main is="slide" class="title">

<big><h1>re-introducing <strong>int</strong></h1></big>

</main>

<main is="slide">

int
===

__maximum__ possible value?

__minimum__ possible value?

what happens if we go over/under?

</main>

</section>


<section name="doubles">
<main is="slide" class="title">

<big><h1>introducing <strong>double</strong></h1></big>


</main>

<main is="slide">

Working with doubles
====================

__declaring__  
`double height = 1.65; // metres`

__scanf__  
`scanf("%lf", &height);`

__printf__  
`printf("Your height is: %lf metres\n");`

</main>




</section>


<!--
<main is="slide" data-section-name="????">

Application: Leap Years
==========

nearly every four years

keeping the calendar in line with the real world

</main>
<main is="slide" data-section-name="????">

Leap Years
==========

> <em>
> Every year that is exactly divisible by four is a leap year,  
> except for years that are exactly divisible by 100,  
> but these centurial years are leap years  
> if they are exactly divisible by 400.  
> For example, the years 1700, 1800, and 1900  
> were not leap years,  
> but the years 1600 and 2000 were.
> </em>

</main>

<main is="slide" data-section-name="????">

Leap Years
==========

Tutorial/lab exercise for this week

</main>


<main is="slide" data-section-name="">

Error handling
==============

Driving revisited:   
What if somebody types in an invalid age?

</main>
-->


<!--
<section name="end">

<main is="slide">

Feedback?
=========


<strong><big>
<a href="http://bit.do/comp1511-feedback-week1" style="color:#f9b200">
bit.do/comp1511-feedback-week1</a>
</big></strong>

<hr>

<img
src="http://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/week01/comp1511-week1-feedback-qr.png" />

</main>

</section>
-->


<!--
<section name="variables">

<main is="slide" data-section-name="variables">

Variables and Types
===================

__Variables__ are used to store data...  
think "boxes"

Each variable has a __data type__...  
this is the size and structure of the "box"

For now, we are using three data types:

__char__ stores characters:  
`A`, `e`, `#`

__int__ stores whole numbers:  
`2`, `17`, `-5`

__float__ stores "floating-point" numbers:
`3.14159`, `2.71828`

</main>
<main is="slide" data-section-name="variables">

Variables
=========

__declare__  
the first time a variable is mentioned,  
we need to specify its type.

__initialise__  
before using a variable we need to assign it a value.

__assign__  
to give a variable a value.

``` c
int num; // Declare
num = 5; // Initialise (also Assign)
...
num = 27; // Assign
```

</main>
<main is="slide" data-section-name="variables">

Variables
=========

we can also declare and initialise in the same step:

``` c
int num = 5; // Declare and Initialise
...
num = 27; // Assign
```

</main>
<main is="slide" data-section-name="variables">

Variable Naming (and other identifiers)
=======================================

must be made up of letters, digits and underscores (`_`)  
the first character must be a letter  
are case sensitive (`num1` and `Num1` are different)

Keywords like  
`if`, `while`, `do`, `int`, `char`, `float`  
can't be used as identifiers

</main>


</section>
-->

<section name="end">
<main is="slide">
end
</main>
</section>

