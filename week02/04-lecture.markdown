---
short-title: COMP1511 18s1 lec04
title: "Lecture 3: Functions"
...


<style>
pre {
    padding-top: 0em !important;
}
</style>

<main is="slide" class="title">
<h1><big><strong>Welcome!</strong></big></h1>

<h1>COMP1511 18s1<br />
    <small>Programming Fundamentals</small></h1>
</main>

<main is="slide" class="title">
<h1>COMP1511 18s1<br />
  <strong>— Lecture 3 —</strong><br />
  <small>Functions</small></h1>

Andrew Bennett  
<small><tt>&lt;andrew.bennett@unsw.edu.au&gt;</tt></small>

even more if statements  
functions  
`while` loops?

</main>

<main is="slide">

<p>&nbsp;</p>

<big><big><big><big><big>
<strong>
Before we begin...
</strong>
</big></big></big></big></big>

<p>&nbsp;</p>
<big>
__introduce__ yourself to the person sitting next to you  

<p>&nbsp;</p>


__why__ did they decide to study __computing__?
</big>
</main>


<main is="slide">

Overview
=======

<big><strong>after this lecture, you should be able to...</strong></big>

represent more __complex__ situations with __if statements__

understand what a __function__ is

understand __why__ we use functions

write __simple functions__

<small><small><small>
(__note__: you shouldn't be able to do all of these immediately after
watching this lecture. however, this lecture should (hopefully!) give
you the foundations you need to develop these skills. remember:
programming is like learning any other language, it takes consistent and
regular practice.)
</small>
</small>
</small>
</main>


<main is="slide" data-section-name="admin">

Admin
=====

<big><big><strong>Don't panic!</strong></big></big>

lecture recordings are on WebCMS3

make sure you have __home computing__ set up

make sure you can send and receive __uni emails__

</main>

<section name="review">
<main is="slide">

Review
======

</main>
</section>

<section name="more-if-statements">
<main is="slide">

More If Statements
=================

<p>&nbsp;</p>
<p>&nbsp;</p>
<i><small>demo: license.c</small></i>
</main>
</section>



<section name="functions">
<main is="slide" class="title">

<big>
<big>and now for something __new__...</big>
</big>

</main>

<!-- who's done the copy/pasting thing, really annoying -->

<main is="slide" data-section-name="functions">

Wouldn't it be nice if...
=========================

<big>
... we didn't have to __copy and paste__ blocks of code?

... we could make parts of our code __reusable__?

... make our main function __smaller and simpler__?

... make our programs __nicer to read__?
</big>

</main>
<main is="slide" data-section-name="functions" class="title">

<big>introducing: <big>__functions__</big></big>

</main>

<!--
hopefully you've all heard of them in math, sine/cosine

more abstract example: you can think of a toaster as a function.

why?

you put something in: you put bread in, and get toast out.

magical black box (well, kitchen appliance), takes some input, does
something you can't see, gives input out.

takes a type of input: numbers for cos/sin, bread for a toaster.

same input = same output
toaster: it's not like one time you'll give it bread and you'll get cake
out

-->

<main is="slide" data-section-name="functions">

What is a Function?
===================

you've already seen functions outside programming:

<big>$\cos$, $\sin$, ...</big>

functions are like a black box.

</main>
<main is="slide" data-section-name="functions">

What is a Function?
===================

you've already seen functions *inside* programming!

<big>`printf`, `scanf`</big>

<big>`int main (int argc, char *argv[]) { ...`</big>

</main>
<main is="slide" data-section-name="functions">

What is a Function?
===================

<big>functions are way of achieving __abstraction__</big>

</main>

<!-- abstraction is basically the fundamental part of computer science
-->

<main is="slide" data-section-name="functions">

Abstraction
===========

"... creating _units_ which can be _reused_,  
and whose internal details  
are _hidden_ from outside inspection ..."

</main>

<!-- for those of you with programming experience, have you ever written
a program that was completely correct?
how do you know it's correct?
(autotest: how do you know the autotest is correct?)


mistakes/bugs can have a flow-on effect: testing each part individually
means easier to find bugs


shorter not always better
(if statement chain)

shorter if we can remove duplicated code
-->

<main is="slide" data-section-name="functions">

Abstraction via Functions
=========================

Functions allow us to:

separate out, or __encapsulate__  
a piece of code serving a single purpose

__test__ and __verify__  
a piece of code

__reuse__  
a piece of code

shorten our programs,  
making it easier to  
__modify__ and __debug__

</main>
<!-- this toaster is called addNumbers, returns toast of type int 

after this: do a demo. some basic playing around with functions stuff.

maybe like, start with addNumbers, then try doing some different things,
calling it from main, different types, etc?

-->
<main is="slide" data-section-name="functions">

Anatomy of a Function
=====================

__return type__  
__function name__  
__parameters__  
(inside parens, comma separated)  
__return statement__

<pre><code class="lang-clike">
int addNumbers (int num1, int num2) {
    int sum = num1 + num2;
    return sum;
}
</code></pre>

<small><small>let's try it!</small></small>
</main>
<main is="slide" data-section-name="functions">

Functions with No Parameters
============================

parameter list: __void__

<pre><code class="lang-clike">
int getRandomNumber (void) {
    // chosen by fair dice roll...
    // guaranteed to be random
    return 4;
}
</code></pre>

</main>
<main is="slide" data-section-name="functions">

Functions with No Return Value
==============================

return type: __void__  
no return statement necessary

<pre><code class="lang-clike">
void printAsterisks (void) {
    printf ("*****");
}
</code></pre>

</main>
<main is="slide" data-section-name="functions">

Function Prototypes
===================

every function has a __function prototype__:  
tells the compiler that  
the function exists,  
and the structure it has.

includes __key information__  
about the function.

<pre><code class="lang-clike">
int addNumbers (int num1, int num2);
int getRandomNumber (void);
void printAsterisks (void);
</code></pre>

</main>
<main is="slide" data-section-name="functions">

Noteworthy Features
===================

a function can have zero or more parameter(s)

a function can only return zero or one value(s)

`* * *`

a function stores a local copy of parameters passed to it

the original values of variables remain unaltered

parameters received by the function,  
and local variables created by the function,  
are all __discarded__ when the function returns

</main>
<main is="slide" data-section-name="functions">

Program Structure
=================

<big>
Header comment

`#include`d files

`#define`s

prototypes

`main` function

functions
</big>

</main>
</section>

<!-- week 3 lecture 2 from 17s2, probably don't go this far? -->

<section name="more-functions">
<main is="slide">
more functions?
</main>

<main is="slide" data-section-name="review">

Functions
=================

building blocks in our programs

self-contained, reusable pieces of code

abstraction

</main>
<main is="slide" data-section-name="review">

Anatomy of a Function
=============================

__return type__  
(`void` if no return value)  
__function name__  
__parameters__  
(inside parens, comma separated;  
`void` if no parameters)  
__statements__  
__return statement__

<pre><code class="lang-clike">
int addNumbers (int num1, int num2) {
    int sum = num1 + num2;
    return sum;
}
</code></pre>

</main>
<main is="slide" data-section-name="review">

Features of Functions
=============================

a function can have zero or more parameter(s)

a function can only return zero or one value(s)

`* * *`

a function stores a local copy of parameters passed to it

the original values of variables remain unaltered

parameters received by the function,  
and local variables created by the function,  
are all __discarded__ when the function returns

</main>
<main is="slide" data-section-name="review">

Functions as Building Blocks
============================

for example:  
a function that takes a number and multiplies it by 2

we can take our number, and put it into the function, and get it out doubled

<pre><code class="lang-clike">
int x = 5;
x = doubled (x);
</code></pre>

__key things__:  
input (parameters)  
output (return value)  
functions won't change values

</main>
<main is="slide" data-section-name="variables">

Touching the `void`
===================

most functions return a value...  
but `void` functions don't return anything

functions may have "side effects"  
like changing the state of the system,  
by printing things out

</main>
<main is="slide" data-section-name="variables">

Using Functions
===============

we've seen how to call a function:  
_printf_, _scanf_

but don't show the types,
just the name of it

</main>

</section>

<!---
<section name="loops">

<main is="slide">
loops?
</main>

<main is="slide" data-section-name="loops">

Remember `if` statements?
=========================

<pre><code class="lang-clike">
int main (int argc, char *argv[]) {
    printf ("Enter a number: ");

    int num;
    scanf ("%d", &num);

    if (num < 10) {
        printf ("Hello!\n");
    }
    
    return EXIT_SUCCESS;
}
</code></pre>

__if__ the condition is true,
__then__ do something,
__else__ do something else.

</main>
<main is="slide" data-section-name="loops">

What if we wanted to do something more than once?

<pre><code class="lang-clike">
int main (int argc, char *argv[]) {
    printf ("Enter a number: ");

    int num;
    scanf ("%d", &num);

    while (num < 10) {
        printf ("Hello!\n");
    }
    
    return EXIT_SUCCESS;
}
</code></pre>

</main>
<main is="slide" data-section-name="loops">

What if we wanted to do something more than once?


<pre><code class="lang-clike">
int main (int argc, char *argv[]) {
    printf ("Enter a number: ");

    int num;
    scanf ("%d", &num);

    while (num < 10) {
        printf ("Hello!\n");
        num++;
    }
    
    return EXIT_SUCCESS;
}
</code></pre>

</main>
<main is="slide" data-section-name="loops">

Anatomy of a Loop
=================

__initialisation__  
__condition__  
__statements__  
__update__

<pre><code class="lang-clike">
int i = 0;
while (i < 10) {
    printf ("Hello (number %d)\n", i);
    i = i + 1;
}
</code></pre>

</main>


</section>
-->

<section name="end">
<main is="slide">

Feedback?
=========


<strong><big>
<a href="http://bit.do/comp1511-feedback-week2" style="color:#f9b200">
bit.do/comp1511-feedback-week2</a>
</big></strong>

<hr>

<img
src="http://www.cse.unsw.edu.au/~andrewb/cs1511/18s1/week02/comp1511-week2-feedback-qr.png" />

alternate link: <a
href="https://andrewb3.typeform.com/to/uxfME1">https://andrewb3.typeform.com/to/uxfME1</a>
</main>

<main is="slide">
end
</main>

</section>

<!-- code highlighted
<pre><code class="lang-clike">
if (condition 1) {
    if (condition 2) {
        if (condition 3) {
            // Do stuff
        } else if (condition 4) {
            // Do something else
        }
    } else if (condition 5) {
        if (condition 6) {
            // Do something completely different
        }
    } else {
        // In all other cases, do this.
    }
}
</code></pre>
-->


